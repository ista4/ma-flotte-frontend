import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import lang from "../helpers/appLang";

const withGuest = (Component , role) => {

  
  const NewComponent =  (props) => {

    const navigate = useNavigate()

        useEffect(()=>{
          if(role === 'admin'){
            if(localStorage.token){
              return navigate(lang+'/admin' , {replace: true})
              
            }
          }
          if(role === 'SpAdmin'){
            if(localStorage.sp_admin_token){
              return navigate(lang+'/sp-admin' , {replace: true})
              
            }
          }
          if(role === 'user'){
            if(localStorage.user_token){
              return navigate(lang+'/' , {replace: true})
              
            }
          }
        
        }, [navigate])
    
    return <Component {...props} />
  }
  
  return NewComponent
  
};



export default withGuest;
