import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import lang from "../helpers/appLang";
import { getAdmin } from "../store/reducer/admin/AdminSlice";
import { getSpAdmin } from "../store/reducer/sp-admin/SpAdminSlice";
import { getUser } from "../store/reducer/user/UserSclice";

const withAuth = (Component,role) => {

  const NewComponent =  (props) => {

    const dispatch = useDispatch()
    const navigate = useNavigate()
    
        useEffect(()=>{
          if(role === 'admin'){
            if(!localStorage.token){

              return navigate(lang+'/admin/auth' , {replace: true})
            }
            if(localStorage.token){

              dispatch(getAdmin()).unwrap()
              .then((res) =>{
                if(res.token === localStorage.token){
                  
                  return navigate(lang+'/admin' , {replace: true})
                }
              })
              .catch((err) =>  console.log(err))
            }
          }
                  
          if (role === "SpAdmin") {
            if (!localStorage.sp_admin_token) {
              return navigate(lang + "/sp-admin/auth", { replace: true });
            }

            if (localStorage.sp_admin_token) {
              dispatch(getSpAdmin())
                .unwrap()
                .then((res) => {
                  if (res.token === localStorage.sp_admin_token) {
                    return navigate(lang + "/sp-admin", { replace: true });
                  }
                })
                .catch((err) => console.log(err));
            }
          }

          if(role === 'user'){
            if(!localStorage.user_token){

              return navigate(lang+'/auth' , {replace: true})
            }
            if(localStorage.user_token){

              dispatch(getUser()).unwrap()
              .then((res) =>{
                if(res.token === localStorage.user_token){

                  return navigate(lang+'/' , {replace: true})
                }
              })
              .catch((err) =>  console.log(err))
            }
          }

        }, [navigate,dispatch])

    return <Component {...props} />
  }
  return NewComponent
};

export default withAuth;
