import {  useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import lang from "../helpers/appLang";

const withGuard = (Component , role) => {

  const NewComponent =  (props) => {

    const navigate = useNavigate()
    
    const user  =  useSelector(state => {
      if(role === "admin") {
        return state.admin
      }
      if(role === "SpAdmin") {
        return state.SpAdmin
      }
      if(role === "user") {
        return state.user
      }
    })

    if(role === "admin"){
      
      if(!localStorage.token){
          console.log(localStorage.token);
          return navigate(lang+'/admin/auth/')
      }
      if(user.admin.email_verified_at === null){
        return navigate(lang+'/admin/auth/complet-register')
      }
    }
    else if(role === "SpAdmin"){
      if(!localStorage.sp_admin_token || localStorage.sp_admin_token === undefined){
          console.log(localStorage.sp_admin_token);
          return navigate(lang+'/sp-admin/auth/' , {replace :true})
      }
    }
    else if(role === "user"){
      if(!localStorage.user_token){
          localStorage.removeItem(localStorage.user_token)
          return navigate(lang+'/auth')
      }
      if(user.user.email_verified_at === null){
        return navigate(lang+'/auth/complet-register')
     }
    }

    return <Component {...props} />
  }
  
  return NewComponent
};



export default withGuard;
