import { Avatar, Box } from '@mui/material';
import { DataGrid } from '@mui/x-data-grid';
import moment from 'moment';
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import DashBoardHeader from '../../../components/DashBoardHeader/DashBoardHeader';
import Density from '../../../components/EditTable/Density';
import withGuard from '../../../hoc/withGuard'
import { getUsers } from '../../../store/reducer/sp-admin/users/GetUsersSlice';
import styles from "./styles.module.css";

const AdminUsers = () => {
  const {users, loading} = useSelector(state => state.GetUsers)
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);
  return (
    <Box className={styles.content} component="main">
        <DashBoardHeader> Users List</DashBoardHeader>

        
      <div
      
      style={{
        height: "100%",
        width: "100%",
        background: "#fff",
        marginTop: "16px",
      }}
    >
      <DataGrid
        rows={users}
        columns={columns}
        loading={loading}
        slots={{ 
          toolbar: Density,
         }}
        sx={{ fontWeight: "bold" }}
      />
    </div>
    </Box>



  )
}

const columns = [
  {
    field: "avatar",
    headerName: "Avatar",
    width: 100,

    editable: false,
    sortable: false,
    filtrable: false,
    renderCell: (params) => (
      <Avatar
        sx={{ width: 46, height: 46 }}
        alt={params.row.nom + " " + params.row.prenom}
        title={params.row.nom + " " + params.row.prenom}
        src={params.row.image}
      />
    ),
  },
  { field: "nom", headerName: "Nom", width: 140 },
  { field: "prenom", headerName: "Prenom", width: 140 },
  {
    field: "tel",
    headerName: "Tel",
    width: 160,
  },
  {
    field: "devices_connected",
    headerName: "appareils connectes",
    type: "string",
    width: 200,
    editable: false,
  },
];

export default withGuard(AdminUsers, 'admin')