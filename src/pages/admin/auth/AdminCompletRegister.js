import React, { useEffect, useState } from 'react'
import { LoadingButton } from '@mui/lab'
import { Alert,TextField ,ToggleButtonGroup, ToggleButton, CircularProgress, LinearProgress,} from '@mui/material'
import { Container } from '@mui/system'
import { useDispatch, useSelector } from 'react-redux'
import { getAdmin } from '../../../store/reducer/admin/AdminSlice'
import { adminCompletRegister } from '../../../store/reducer/admin/auth/AdminCompletRegisterSlice'
import { useNavigate } from 'react-router-dom'
import lang from '../../../helpers/appLang'
import withVerified from '../../../hoc/withVerified'

const AdminCompletRegister = (props) => {

  const [email,setEmail] = useState()
  const [tel,setTel] = useState()
  const [sexe,setSexe] = useState('')

  const [msgError,setMsgError] = useState({
    email : false,
    sexe : false,
    tel : false,
    error : false,
    msg:""
  })

  const {loading :isLoading} = useSelector((state) => state.AdminCompletRegister)
   


  const {loading, admin } = useSelector((state) => state.admin)



  const dispatch = useDispatch()
  const navigate = useNavigate()

  
  useEffect(()=>{
    dispatch(getAdmin()).unwrap()
       .then((res) => res.email_verified_at ?  navigate(lang+'/admin'):'')
       .catch((err) =>  console.log(err))
  }, [dispatch, navigate])


  const handleChange = (event, newAlignment) => {
    
    setSexe(newAlignment);
  };

  const handelSubmit = (e) => {
    e.preventDefault()

    const data = {email, tel, sexe}

    if(!data.email){
     setMsgError((prev) =>( {...prev,email:true}))
    }else{
     setMsgError((prev) =>( {...prev,email:false}))
    }
    if(!data.tel){
     setMsgError((prev) =>( {...prev,tel:true}))
    }else{
     setMsgError((prev) =>( {...prev,tel:false}))
    }

    if(!data.sexe){
      setMsgError((prev) =>( {...prev,sexe:true}))
     }else{
      setMsgError((prev) =>( {...prev,sexe:false}))
     }

    if(data.email && data.tel && data.sexe){
      
      
      dispatch(adminCompletRegister(data)).unwrap()
      .then((originalPromiseResult) => {
        console.log(originalPromiseResult);
        navigate(`${lang}/admin/auth/verification`)
      })
      .catch((rejectedValueOrSerializedError) => {
        
       setMsgError((prev) =>( {...prev,error:true , msg:rejectedValueOrSerializedError}))
      })
    }
 }
  return (
   loading ? <LinearProgress color='success'  sx={{ height: "4px"}}/> :
   ( <div className='parent'> 
   <Container sx={{width:'100%', height:'100%',display:'flex'}} maxWidth={'xl'} component="div" className='container'>
  <div className='left'>

    <div >

      <h1 >Bienvenue  <span style={{textTransform:'capitalize'}}>  {loading ?  <CircularProgress size={30}/> : admin.nom } </span> </h1>
      <p >veuillez compléter l'enregistrement !</p>
    </div>

  </div>
  <div className='right'>
 
    <div className='form-container'>
       {
         msgError.error ? 
         (<Alert severity="error" sx={{width:"100%",marginBottom:"12px"}}>{msgError.msg}</Alert>) :
          msgError.email || msgError.tel || msgError.sexe ? 
         (<Alert severity="error" sx={{width:"100%",marginBottom:"12px"}}>Tous les champs sont obligatoires !</Alert>) : ""
       }
      <form onSubmit={handelSubmit}>
        <TextField id="outlined-basic" 
        label="Email"
        type={'email'} 
        error = {msgError.email}
        onChange={(e) => setEmail(e.target.value)}
        variant="outlined" 
        sx={{width:'100%'}}
        />
        <TextField id="outlined-basic" 
        label="Numéro De Tél" 
        error = {msgError.tel}
        variant="outlined" 
        onChange={(e) => setTel(e.target.value)}
        sx={{width:'100%'}}
        />
        
       <div className='gender' sx={{width:'100%'}}>
       <label>sexe</label>
       <ToggleButtonGroup
     value={sexe}
     exclusive
     onChange={handleChange}
     className = {msgError.sexe? 'error' : ''}
     aria-label="sexe"
       >
       
         <ToggleButton value="H" >Homme</ToggleButton>
         <ToggleButton value="F">Femme</ToggleButton>
         
       </ToggleButtonGroup>
       </div>

        <span className='line'></span>
       <LoadingButton type='submit'  variant='contained' loading={isLoading} sx={{ width:'100%'}}>S'identifier</LoadingButton>

      </form>
    </div>


  </div>
  </Container>
</div>)

  )
}

export default withVerified(AdminCompletRegister,'admin')