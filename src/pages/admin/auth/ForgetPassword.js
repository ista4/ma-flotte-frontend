import { LoadingButton } from '@mui/lab'
import { Alert , TextField} from '@mui/material'
import { Container } from '@mui/system'
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import lang from '../../../helpers/appLang'
import withGuest from '../../../hoc/withGuest'

import { forgetPassword } from '../../../store/reducer/admin/auth/AdminForgetPasswordSlice'


const ForgetPassword = () => {

  const [email, setEmail] = useState('')

  const dispatch = useDispatch()
  const navigate = useNavigate()
  const {loading} = useSelector((state) => state.AdminForgetPassword)

  const [msgError,setMsgError] = useState({
    email : false,
    msg:"",
  })

  const handelSubmit = (e) => {
    e.preventDefault()
  
    if(!email.length || email.length <6 ){
        setMsgError((prev) =>( {...prev,email:true, msg:'email invalide'}))

    }else{
      dispatch(forgetPassword(email)).unwrap()
      .then((originalPromiseResult) => {
        if(originalPromiseResult.success){
          navigate(`${lang}/admin/auth/reset-password`)
         }
         
      })
      .catch((rejectedValueOrSerializedError) => {
 
       setMsgError((prev) =>( {...prev,error:true , msg:rejectedValueOrSerializedError}))
      })
    }
  }

  return (
    <div className='parent'> 
       <Container sx={{width:'100%', height:'100%',display:'flex'}} maxWidth={'xl'} component="div" className='container'>
     
      <div className='right'>
      <h4 className='otp-msg'>Pour réinitialiser le mot de passe entrez votre email</h4>
        <div className='form-container'>
        
        {
          msgError.error ? 
          (<Alert severity="error" sx={{width:"100%",marginBottom:"12px"}}>{msgError.msg}</Alert>) :''
        }
      
          <form method='POST' onSubmit={handelSubmit}>
          
          <TextField id="outlined-basic" 
            label="Email"
            type={'email'} 
            variant="outlined" 
            onChange={(e)=>setEmail(e.target.value)}
            error={msgError.email}
            sx={{width:'100%'}}
         />
            <LoadingButton loading={loading} type='submit'  variant='contained'  sx={{ width:'100%'}}>vérifier</LoadingButton>
          </form>  
        </div>
      </div>
      </Container>
    </div>
  
  )
}

export default withGuest(ForgetPassword,'admin') 