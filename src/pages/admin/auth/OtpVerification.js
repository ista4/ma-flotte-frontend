
import { LoadingButton } from '@mui/lab'
import { Alert } from '@mui/material'
import { Container } from '@mui/system'
import  { useState } from 'react'
import { MuiOtpInput } from 'mui-one-time-password-input'


import '../../auth.css'
import { adminOtp } from '../../../store/reducer/admin/auth/AdminOtpVerificationSlice'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import lang from '../../../helpers/appLang'
import withVerified from '../../../hoc/withVerified'


const OtpVerification = () => {
 
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const {loading} = useSelector((state) => state.adminOtp)

    const [otp, setOtp] = useState('')

    const [msgError,setMsgError] = useState({
      otp : false,
      msg:""
    })
    const handleChange = (newValue) => {
        setOtp(newValue)
    }


    const handelSubmit = (e)=> {

      e.preventDefault()

      if(otp.length !== 6){
        setMsgError((prev) =>( {...prev,otp:true, msg:'code invalide'}))
      }else{
        dispatch(adminOtp(otp)).unwrap()
        .then((originalPromiseResult) => {
          navigate(`${lang}/admin`)
        })
        .catch((rejectedValueOrSerializedError) => {
          console.log(rejectedValueOrSerializedError);
         setMsgError((prev) =>( {...prev,error:true , msg:'code invalide'}))
        })
  
      }
      
    }

   

    const handelComplete = (e) => {
     
      if(e.length === 6){

        dispatch(adminOtp(e)).unwrap()
        .then((originalPromiseResult) => {
          console.log(originalPromiseResult);
          navigate(lang+'/admin')
        })
        .catch((rejectedValueOrSerializedError) => {
          console.log(rejectedValueOrSerializedError);
          
         setMsgError((prev) =>( {...prev,otp:true , msg:'code invalide'}))
        })
      }

    }
      
    const validateChar = (value, index) => {

        return !isNaN(value) 
      }


  return (

    <div className='parent'> 
       <Container sx={{width:'100%', height:'100%',display:'flex'}} maxWidth={'xl'} component="div" className='container'>
     
      <div className='right'>
      <h4 className='otp-msg'>Code envoyé à votre e-mail. Entrez le code pour vérifier.</h4>
        <div className='form-container'>
        
        {
          msgError.otp ? <Alert severity="error" sx={{width:"100%",marginBottom:"12px"}}>{msgError.msg}</Alert> : ''
        }
        
        
          <form onSubmit={handelSubmit}>
          
           <MuiOtpInput  onError={() => setMsgError((prev) =>( {...prev,otp:true, msg:'code invalide'}))}
            onComplete={handelComplete}  length={6} value={otp} 
            onChange={handleChange}  validateChar={validateChar}/>
          
            <LoadingButton type='submit' loading={loading} variant='contained'  sx={{ width:'100%'}}>vérifier</LoadingButton>

       
          </form>  
        </div>


      </div>
      </Container>
    </div>
  

  )
}

export default withVerified(OtpVerification, 'admin')