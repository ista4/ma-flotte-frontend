import { Avatar, Box, Button, IconButton, Typography} from '@mui/material'
import React, { useState } from 'react'

import withGuard from '../../../hoc/withGuard'
import EditIcon from '@mui/icons-material/Edit';
import styles from './styles.module.css'
import { useDispatch, useSelector } from 'react-redux';
import { PhotoCamera } from '@mui/icons-material';

import { enqueueSnackbar } from 'notistack';

import { openResponsiveModal } from '../../../store/reducer/ResponsiveModalSlice';
import { updateAdmin } from '../../../store/reducer/sp-admin/admins/UpdateAdminSlice';
import { getAdmin } from '../../../store/reducer/admin/AdminSlice';




const AdminProfile = () => {
  const [image, setImage] = useState(null)
  const {admin} = useSelector(state => state.admin)

  const dispatch = useDispatch()

  const handelImageChange = (img) => {
    setImage(img)

    if(img){

      dispatch(updateAdmin({id : admin.id, field : 'image', value : img})).unwrap()
      .then(res => {
        dispatch(getAdmin())
        enqueueSnackbar('image est modifié avec succés', {variant : 'success'})
      }).catch(err => {
        console.log(err);
        enqueueSnackbar('Accept just JPG, PNG, GIF BMP ou WebP .', {variant : 'error'})
      })

    }
  }

 
  return (
    <Box className={styles.content}  component="main">

      <Box className={styles.profile_image_container}>

        <Box className={styles.cover_image}>
          <span></span>
        </Box>

        <Box className={styles.profile_image}>
          <Avatar className={styles.img} 
          src={image ? URL.createObjectURL(image) : admin.image}

          sx={{ width: 200, height: 200 }} alt='profile image'/>

          <IconButton color="success" aria-label="upload picture" 
            className={styles.upload_btn}
            component="label">
            <input hidden accept="image/*" type="file" 
            onChange={(e) => handelImageChange(e.target.files[0])}
             />
            <PhotoCamera />
          </IconButton>
          
        </Box>

      </Box>

      <Box className={styles.profile_info}>
            <Box className={styles.actions}>
              <Button className={styles.btn}
                onClick={() => dispatch(openResponsiveModal({
                  componentName : 'UpdateProfile',
                  title : 'Modifier le profil',
                  childrenProps : {role : 'admin'},
                }))}
               variant="outlined" color="primary" size="small" startIcon={<EditIcon />}>
                Modifier
              </Button>
            </Box>

            <Box className={styles.info}>
              <Typography className={styles.title} variant="h5" fontWeight={700} my={5} component="h2">
              Informations :
              </Typography>

              <Box className={styles.info_items}>

                
                <Box className={styles.info_item}>
                <span className={styles.label}>Nom et Prénom</span>
                <span className={styles.value}>{admin.nom} {admin.prenom}</span>
                </Box>

                <Box className={styles.info_item}>
                <span className={styles.label}>CIN</span>
                <span className={styles.value} >{admin.cin}</span>
                </Box>

                <Box className={styles.info_item}>
                <span className={styles.label}>Sexe</span>
                <span className={styles.value}>{admin.sexe}</span>
                </Box>

                <span className={styles.separetor}></span>
                

                <Box className={styles.info_item}>
                <span className={styles.label}>Email</span>
                <span className={styles.value}>{admin.email} </span>
                </Box>

                <Box className={styles.info_item}>
                <span className={styles.label}>Telephone</span>
                <span className={styles.value}>{admin.tel}</span>
                </Box>
                
                <span className={styles.separetor}></span>

                
                <Box className={styles.info_item}>
                <span className={styles.label}>Rôle</span>
                <span className={styles.value}>{admin.role}</span>
                </Box>
        
              </Box>

            </Box>
      </Box>

    </Box>
  )
}


export default withGuard(AdminProfile,'admin')