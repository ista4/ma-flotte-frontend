import {  Box, Chip, IconButton } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import DashBoardHeader from "../../../components/DashBoardHeader/DashBoardHeader";
import withGuard from "../../../hoc/withGuard";

import styles from "./styles.module.css";
import { getMissions } from "../../../store/reducer/mission/MissionSlice";
import {Print } from "@mui/icons-material";
import { printMission } from "../../../store/reducer/mission/PrintMissionSlice";
import downloadFile from "../../../helpers/fileDownload";

import { enqueueSnackbar } from "notistack";
import { openFullModal } from "../../../store/reducer/FullModalSlice";
import { openResponsiveModal } from "../../../store/reducer/ResponsiveModalSlice";

const Orders = () => {
  const dispatch = useDispatch();
  const { loading, missions } = useSelector((s) => s.Mission);
  console.log(missions);
  useEffect(() => {
    dispatch(getMissions());
  }, [dispatch]);

  const columns = [
    {
      field: "user",
      headerName: "Utilisateur",
      width: 150,
      renderCell: ({ row }) => (
        <Chip label={row.user.nom + " " + row.user.prenom} 
          clickable
          title={row.user.nom + " " + row.user.prenom}
          onClick={() => {
            dispatch(openResponsiveModal({
              componentName: "UserCart",
              childrenProps: row.user,
              
            }))}}
        />

      )
    },
    {
      field: "vehicule",
      headerName: "Vehicule",
      width: 150,
      renderCell: ({ row }) => (
        <Chip label={row.vehicule.nom }
          clickable
          title={row.vehicule.nom  + " | " + row.vehicule.immatriculation}
          onClick={() => {
            dispatch(openFullModal({
              title: "Vehicule",
              vehicule : row.vehicule,
              role: 'SpAdmin',
            }))}}
        />
      )
    },
    {
      field: "completed_at",
      headerName: "Complet a",
      width: 200,
      type: "dateTime",
      valueGetter: ({ value }) => {
        return value ? new Date(value) : "";
      },
    },
    {
      field: "date_depart",
      headerName: "Date Depart",
      width: 200,
      type: "dateTime",
      valueGetter: ({ value }) => {
        return value ? new Date(value) : "";
      },
    },
    {
      field: "date_retour",
      headerName: "Date Retour",
      width: 200,
      type: "dateTime",
      valueGetter: ({ value }) => {
        return value ? new Date(value) : "";
      },
    },
    { field: "lieu", headerName: "Liue", width: 120 },
    { field: "nbr_personne", headerName: "N° persoones", width: 120 },
    {
      field: "is_completed",
      headerName: "Terminé",
      type: "string",
      width: 120,
      renderCell: ({ row }) => (
        <Chip
          variant="outlined"
          label={row.is_completed  ? "Terminé" : "En cours"}
          color={row.is_completed  ? "success" : "warning"}
        />
      ),
    },
    // actions : print , delete
  
    {
      field: "actions",
      headerName: "Actions",
      width: 120,
      renderCell: ({ row }) => (
        <Box display={"flex"} gap={2} >
          <IconButton color="primary" onClick={() => handlePrint(row.id)} aria-label="print order" component="div">
            <Print color="primary" />
          </IconButton>
        </Box>
      ),
    },
  ];


  const handlePrint = (id) => {
    dispatch(printMission(id)).unwrap().then((res) =>{
      console.log(res);

      if(res.success){
        downloadFile(res.pdf, "mission.pdf", "application/pdf");
        enqueueSnackbar("Mission imprimée avec succès", { variant: "success" });

      }
    })
    .catch((err) => enqueueSnackbar("Une erreur est survenue", { variant: "error" }));

  };
  return (
    <Box className={styles.content} component="main">
      <DashBoardHeader>Orders</DashBoardHeader>
      <div
        style={{
          height: "100%",
          width: "100%",
          background: "#fff",
          marginTop: "16px",
        }}
      >
        <DataGrid
          rows={missions}
          columns={columns}
          loading={loading}
          slots={{ toolbar: GridToolbar }}
          sx={{ fontWeight: "bold" }}
        />
      </div>
    </Box>
  );
};



export default withGuard(Orders, "admin");
