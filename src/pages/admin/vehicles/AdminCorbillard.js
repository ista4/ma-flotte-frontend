import { Box } from '@mui/material'
import React from 'react'
import DashBoardHeader from '../../../components/DashBoardHeader/DashBoardHeader'
import withGuard from '../../../hoc/withGuard'

import styles from './styles.module.css'

const AdminCorbillard = () => {
  return (
    <Box className={styles.content}  component="main">
       <DashBoardHeader>
           Admin Corbillard
       </DashBoardHeader>
    </Box>
  )
}

export default withGuard(AdminCorbillard, 'admin')