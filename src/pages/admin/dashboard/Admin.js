import {
  Avatar,
  Box,
  Chip,
  CircularProgress,
  Divider,
  Typography,
} from "@mui/material";
import React, { useEffect } from "react";
import withGuard from "../../../hoc/withGuard";
import DashBoardHeader from "../../../components/DashBoardHeader/DashBoardHeader";
import styles from "./styles.module.css";
import SoftBox from "../../../components/SoftBox/SoftBox";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import LocalGasStationIcon from "@mui/icons-material/LocalGasStation";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css";
import "swiper/css/navigation";
import { Navigation } from "swiper";
import { Group } from "@mui/icons-material";
import CommuteIcon from "@mui/icons-material/Commute";
import { DataGrid } from "@mui/x-data-grid";

import FuelChart from "../../../components/AddFuel/FuelChart";
import { useDispatch, useSelector } from "react-redux";
import { getDashboardData } from "../../../store/reducer/DashBoardSlice";
import { openFullModal } from "../../../store/reducer/FullModalSlice";
import PieChartMission from "../../sp-admin/dashboard/PieChartMission";

const Admin = () => {
  const dispatch = useDispatch();
  const {
    loading,
    admin_number,
    user_number,
    vehucule_number,
    carburant_number,
    last_fuels,
    //TODO: add last_missions
    missions_encours,
    missions_terminees,
  } = useSelector((s) => s.DashBoard);

  useEffect(() => {
    dispatch(getDashboardData())
      .unwrap()
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [dispatch]);

  return (
    <Box className={styles.content} component="main">
      <DashBoardHeader>Tableau de bord</DashBoardHeader>
      <Swiper
        spaceBetween={16}
        slidesPerView={"auto"}
        modules={[Navigation]}
        navigation
        breakpoints={{
          639: {
            slidesPerView: 2,
          },
          865: {
            slidesPerView: 2,
          },
          1000: {
            slidesPerView: 3,
          },
          1500: {
            slidesPerView: 4,
          },
          1700: {
            slidesPerView: 4,
          },
        }}
        className={styles.mySwiper}
      >
        <SwiperSlide className={styles.swiper_slide}>
          <SoftBox
            icon={<Group fontSize="inherit" />}
            title="N° administrateurs"
            body={loading ? ".." : admin_number}
          />
        </SwiperSlide>
        <SwiperSlide className={styles.swiper_slide}>
          <SoftBox
            icon={<ManageAccountsIcon fontSize="inherit" />}
            title="N° utilisateurs"
            body={loading ? ".." : user_number}
          />
        </SwiperSlide>
        <SwiperSlide className={styles.swiper_slide}>
          <SoftBox
            icon={<CommuteIcon fontSize="inherit" />}
            title="N° vehicules"
            body={loading ? ".." : vehucule_number}
          />
        </SwiperSlide>
        <SwiperSlide className={styles.swiper_slide}>
          <SoftBox
            icon={<LocalGasStationIcon fontSize="inherit" />}
            title="N° remplissages"
            body={loading ? ".." : carburant_number}
          />
        </SwiperSlide>
      </Swiper>

      <Divider />

      <Divider
        sx={{
          marginTop: "2.2rem",
        }}
      >
        <Chip label="Statistiques" />
      </Divider>

      <Box className={styles.charts}>
        {loading ? (
          <Box
            sx={{
              width: "100%",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "370px",
            }}
          >
            <CircularProgress color="inherit" size={48} />
          </Box>
        ) : (
          <>
            <Box className={styles.chart_one}>
              <Typography variant="h6" gutterBottom component="div">
                Statistiques des missions
              </Typography>
              <PieChartMission
                data={[
                  {
                    name: "En Cours",
                    value: missions_encours.length,
                  },
                  {
                    name: "Terminees",
                    value: missions_terminees,
                  },
                ]}
              />
            </Box>
            <Box className={styles.chart_two}>
              <Typography variant="h6" gutterBottom component="div">
                Statistiques de consommation
              </Typography>
              <FuelChart loading={loading} />
            </Box>
          </>
        )}
      </Box>

      <Divider
        sx={{
          marginTop: "2.2rem",
        }}
      >
        <Chip label="Dernières Activités" />
      </Divider>

      <Box className={styles.tables}>
        <Box className={styles.table_one}>
          <Typography variant="h6" gutterBottom component="div">
            Derniers remplissages
          </Typography>

          <DataGrid
            className={styles.table}
            rows={last_fuels}
            columns={[
              { field: "par", headerName: "Par", width: 120,
              valueGetter: (params) => params.row.carbure_par.nom + " " + params.row.carbure_par.prenom,
              },
              { field: "vehicule", headerName: "Vehicule", width: 120,
              valueGetter: (params) => params.row.vehicule.immatriculation,
              },
              { field: "quantite", headerName: "Quantite", width: 120 },
              { field: "cout", headerName: "Cout", width: 120 },
              { field: "created_at", headerName: "Date", width: 120 },
            ]}
            hideFooter
            sx={{ fontWeight: "bold" }}
          />
        </Box>
        <Box className={styles.table_two}>
          <Typography variant="h6" gutterBottom component="div">
            Mission en cours
          </Typography>
          <DataGrid
            className={styles.table}
            rows={missions_encours}
            columns={[
              {
                field: "image",
                headerName: "Image",
                width: 60,
                renderCell: (params) => (
                  <Avatar
                    key={params.row.user.id}
                    alt={params.row.user.cin}
                    src={params.row.user.image}
                    sx={{ width: "40px", height: "40px" }}
                  />
                ),
              },
              {
                headerName: "Utilisateur",
                field: "user",
                width: 120,
                renderCell: (params) =>
                  params.row.user.nom + " " + params.row.user.prenom,
              },
              {
                field: "vehicule",
                headerName: "Vehicule",
                width: 120,
                renderCell: (params) => {
                  return (
                    <Chip
                      label={params.row.vehicule.immatriculation}
                      clickable
                      onClick={() => {
                        dispatch(
                          openFullModal({
                            vehicule: params.row.vehicule,
                            role: "user",
                          })
                        );
                      }}
                    />
                  );
                },
              },
              { field: "lieu", headerName: "Lieu", width: 130 },
              { field: "date_depart", headerName: "Date depart", width: 130 },
            ]}
            hideFooter
            sx={{ fontWeight: "bold" }}
          />
        </Box>
      </Box>
    </Box>
  );
}


export default withGuard(Admin, 'admin')