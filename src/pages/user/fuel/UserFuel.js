import { Box, Button} from '@mui/material'
import React, { useEffect } from 'react'
import DashBoardHeader from '../../../components/DashBoardHeader/DashBoardHeader'
import withGuard from '../../../hoc/withGuard'

import styles from './styles.module.css'
import EditTable from '../../../components/EditTable/EditTable'
import { useDispatch, useSelector } from 'react-redux'
import { getUser } from '../../../store/reducer/user/UserSclice'
import moment from 'moment/moment'
import { openFullModal } from '../../../store/reducer/FullModalSlice'
import { openResponsiveModal } from '../../../store/reducer/ResponsiveModalSlice'

const UserFuel = () => {
  const dispatch = useDispatch()
  const {user, loading} = useSelector(state => state.user)


  const columns = [
    { field: "quantite", headerName: "Quantite/L", width: 130 },
    { field: "prix", headerName: "Prix/L", width: 130 },
    { field: "cout", headerName: "Cout", width: 130 },
    { field: "created_at", headerName: "Date", width: 200,
     type :'dateTime' ,valueGetter: (params) => new Date(params.row.created_at),  },
    { field: "vehicule", headerName: "Vehicule", width: 200 ,
    valueGetter: (params) => params.row.vehicule.nom + " | " + params.row.vehicule.immatriculation,},
    { field: "station_service", headerName: "Station", width: 200 ,
      renderCell: ({ row }) => (
        <Button
          sx={{ fontSize: "12px" , height:'80%'}}
          variant="contained"
          fullWidth
          onClick={() => handelStationClick(row.station_service)}
          color="success"
        >
          {row.station_service.nom }
        </Button>
      ), },
  ];

  const handelStationClick = (data) => {

   
    dispatch(openResponsiveModal({
      componentName: 'GasStation',
      childrenProps: data,
      title: data.nom 
    }))
    console.log(data);
  }


  return (
    <Box className={styles.content}  component="main">
       <DashBoardHeader>
        Carburant User
       </DashBoardHeader>
       
       <Box sx={{
          width: '100%',
          height: '100%',
          padding: '12px',
       }}
        >
        <EditTable
              style={{ height: 700, width: '100%' }}
              pageSize={5}
              pageSizeOptions={[5, 10, 25]}
              columns={columns}
              rows={ user.carburants ?? []}
              storeName="user"
              loading={loading}
        />

        </Box>
    </Box>
  )
}

export default withGuard(UserFuel,'user')