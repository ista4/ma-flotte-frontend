import { Box, Chip, IconButton, Tooltip } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";

import React from "react";
import {  useDispatch, useSelector } from "react-redux";
import DashBoardHeader from "../../../components/DashBoardHeader/DashBoardHeader";
import withGuard from "../../../hoc/withGuard";

import styles from "./styles.module.css";
import TaskAltIcon from '@mui/icons-material/TaskAlt';
import { completeMission } from "../../../store/reducer/mission/CompleteMissionSlice";
import { closeResponsiveModal } from "../../../store/reducer/ResponsiveModalSlice";
import { enqueueSnackbar } from "notistack";
import { getUser } from "../../../store/reducer/user/UserSclice";
const UserOrders = () => {

  const columns = [

    { field: "code_order", headerName: "Code", width: 140 },
    { field: "completed_at", headerName: "Complet a", width: 200,type: 'dateTime', valueGetter : ({value}) => { return  value ?  new Date(value) :'' } },
    { field: "date_depart", headerName: "Date Depart", width: 200 ,type: 'dateTime', valueGetter : ({value}) => { return  value ?  new Date(value) :'' } },
    { field: "date_retour", headerName: "Date Retour", width: 200 ,type: 'dateTime', valueGetter : ({value}) => { return  value ?  new Date(value) :'' }  },
    { field: "lieu", headerName: "Liue", width: 120 },
    { field: "nbr_personne", headerName: "N° persoones", width: 120 },
    {
      field :'complete_mission',
      headerName: "Terminé ?",
      width: 90,
      renderCell: ({ row }) => (
        <Tooltip title={!Boolean(row.is_completed) ? "Terminé Cette Mission" : ''}>
        <IconButton
          disabled={Boolean(row.is_completed)}
          onClick={() => {
            handelMissionStop(row.id);
          }}
        >
          <TaskAltIcon />
        </IconButton>
      </Tooltip>
      ),
    },
    {
      field: "is_completed", headerName: "Statut", width: 120,
      renderCell: ({ row }) => (
        <Chip label={row.is_completed ? "Terminé" : "En cours"} color={row.is_completed ? "success" : "warning"} />
      ),
    },
  ];
  
  
  const { loading, user } = useSelector((s) => s.user);
  const dispatch = useDispatch();
  
  const handelMissionStop = (missionID) => {
    console.log(missionID);
 
    dispatch(completeMission(missionID)).unwrap()
    .then((res) => {
      console.log(res);
      enqueueSnackbar("Mission Terminer avec succés", { variant: "success" })
      dispatch(getUser());
    })
    .catch((err) => {
      console.log(err);
       enqueueSnackbar("Erreur lors de la terminaison de la mission", { variant: "error" })
    });
  };

  return (
    <Box className={styles.content} component="main">
      <DashBoardHeader>Orders</DashBoardHeader>

      <div
      
        style={{
          height: "100%",
          width: "100%",
          background: "#fff",
          marginTop: "16px",
        }}
      >
        <DataGrid
          rows={user.missions ?? []}
          columns={columns}
          loading={loading}
          slots={{ toolbar: GridToolbar }}
          sx={{ fontWeight: "bold" }}
        />
      </div>
    </Box>
  );
};


export default withGuard(UserOrders, "user");
