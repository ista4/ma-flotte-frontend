import { Box } from '@mui/material'
import React from 'react'
import DashBoardHeader from '../../../components/DashBoardHeader/DashBoardHeader'
import withGuard from '../../../hoc/withGuard'

import styles from './styles.module.css'

const UserVehicles = () => {
  return (
    <Box className={styles.content}  component="main">
       <DashBoardHeader>
        User Vehicles  
       </DashBoardHeader>
    </Box>
  )
}

export default withGuard(UserVehicles, 'user')