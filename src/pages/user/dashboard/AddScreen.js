import { Box } from "@mui/material";
import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import { QrCodeScanner } from "@mui/icons-material";
import KeyboardIcon from '@mui/icons-material/Keyboard';
import { useDispatch } from "react-redux";
import { openResponsiveModal } from "../../../store/reducer/ResponsiveModalSlice";
import LocalGasStationIcon from '@mui/icons-material/LocalGasStation';
import RouteIcon from '@mui/icons-material/Route';

// Css classes 
import styles from './styles.module.css'
const AddScreen = () => {

    const dispatch = useDispatch()
  return (
    <Box
      sx={{
       maxWidth: "600px",
       height: "100%",
        display: "flex",
        flexDirection: "column",
        gap: "20px",
        alignItems: "center",
        justifyContent: "center",
        margin: "auto",
      }}
    >
      <Card className={styles.card} onClick={() => dispatch(openResponsiveModal({componentName:"Mission", title:'Order Mission'}))}>
        <CardActionArea>
          <CardContent>
            <Typography
              gutterBottom
              variant="h5"
              className={styles.card_title}
              fontWeight="500"
              component="div"
            >
              Start mission <RouteIcon fontSize="large" />
            </Typography>
            <Typography variant="body2" color="text.secondary">
             Clicker ici pour commencer une nouvelle mission 
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>


      <Card className={styles.card}
       onClick={() => dispatch(openResponsiveModal({componentName:"AddFuel", title : 'Ajouter Carburant'}))}>
        <CardActionArea>
          <CardContent>
            <Typography
              gutterBottom
              variant="h5"
              className={styles.card_title}
              fontWeight="500"
              component="div"
            >
              Ajouter Carburant <LocalGasStationIcon fontSize="large" />
            </Typography>
            <Typography variant="body2" color="text.secondary">
                pour ajouter le carburant manuellement ou par le QrCode de vehicule
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Box>
  );
};

export default AddScreen;
