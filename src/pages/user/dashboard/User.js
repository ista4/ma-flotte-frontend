import {Box, Chip, Divider, SpeedDial} from '@mui/material'
import AddIcon from '@mui/icons-material/Add'
import React from 'react'
import DashBoardHeader from '../../../components/DashBoardHeader/DashBoardHeader'
import withGuard from '../../../hoc/withGuard'
import { useDispatch, useSelector } from 'react-redux'
import { openResponsiveModal } from '../../../store/reducer/ResponsiveModalSlice'
import styles from './styles.module.css'


const User = () => {
  const dispatch = useDispatch()
  const {user, loading} = useSelector(s => s.user)

  const missions = user?.missions 
  const missionsTermines = user.missions?.filter(m => m.is_completed) 
  const missionsEnCours = user.missions?.filter(m => !m.is_completed ) 

  // carburants Satatistics
  const carburants = user?.carburants  
  const coutTotal = carburants?.reduce((acc, carburant) => acc + Number(carburant.cout), 0) 
  const quantiteTotal = carburants?.reduce((acc, carburant) => acc + Number(carburant.quantite), 0)  
  const vehiculeCarburer = [...new Set(carburants?.map(carburant => carburant.vehicule.nom)) ]

  return (
    <Box className={styles.content} component="main">
      <DashBoardHeader>Tableau De Bord</DashBoardHeader>

      <SpeedDial
        ariaLabel="Ajouter carburant"
        onClick={() =>
          dispatch(openResponsiveModal({ componentName: "AddScreen" }))
        }
        sx={{ position: "absolute", bottom: 16, right: 16 }}
        icon={<AddIcon />}
      ></SpeedDial>

      {
        loading ? ''  : (
          <>
          <Divider
        sx={{
          marginY: "20px",
        }}
      >
        <Chip label="Missions" />
      </Divider>
      <Box className={styles.dashboard_content} component="main">
        <Box className={styles.box_info} component="div">
          <Box className={styles.box_info_title} component="h3">
            {missions?.length}
          </Box>
          <Box className={styles.box_info_subtitle} component="p">
            Total Missions
          </Box>
        </Box>

        <Box className={styles.box_info} component="div">
          <Box className={styles.box_info_title} component="h3">
            {missionsTermines?.length}
          </Box>
          <Box className={styles.box_info_subtitle} component="p">
            Mission terminées
          </Box>
        </Box>

        <Box className={styles.box_info} component="div">
          <Box className={styles.box_info_title} component="h3">
            {missionsEnCours?.length}
          </Box>
          <Box className={styles.box_info_subtitle} component="p">
            Mission en cours
          </Box>
        </Box>

      </Box>
      <Divider
       sx={{
          marginY: "20px",
        }}
      >
            <Chip label="Carburants" />
      </Divider>
      <Box className={styles.dashboard_content} component="main">
        <Box className={styles.box_info_two} component="div">
          <Box className={styles.box_info_title} component="h3">
            {coutTotal }
          </Box>
          <Box className={styles.box_info_subtitle} component="p">
            Cout Total
          </Box>
        </Box>

        <Box className={styles.box_info_two} component="div"> 
          <Box className={styles.box_info_title} component="h3">
            {quantiteTotal}
          </Box>
          <Box className={styles.box_info_subtitle} component="p">
            Quantité Total
          </Box>
        </Box>

        <Box className={styles.box_info_two} component="div">
          <Box className={styles.box_info_title} component="h3">
            {vehiculeCarburer?.length}
          </Box>
          <Box className={styles.box_info_subtitle} component="p">
            Véhicule Carburer
          </Box>
        </Box>
     </Box>

          </>
        )
      }
   </Box>
  );
}


export default withGuard(User, 'user')