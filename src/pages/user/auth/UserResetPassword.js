import { useState } from "react";
import { LoadingButton } from "@mui/lab";
import { Alert, FormControl, IconButton, InputAdornment, InputLabel, OutlinedInput, TextField } from "@mui/material";
import { Container } from "@mui/system";
import { MuiOtpInput } from "mui-one-time-password-input";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import lang from "../../../helpers/appLang";
import { resetPassword } from "../../../store/reducer/user/auth/UserResetPasswordSlice";

import { Visibility, VisibilityOff } from "@mui/icons-material";
import withGuest from "../../../hoc/withGuest";

const UserResetPassword = () => {

  const [showPassword, setShowPassword] = useState(false);
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const dispatch = useDispatch()
  const navigate = useNavigate()

  const [otp, setOtp] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirmation, setPasswordConfirmation] = useState("");

  const [msgError,setMsgError] = useState({
    password : false,
    passwordConfirmation : false,
    passwordIsConfirmation : false,
    otp : false,
    error:false,
    msg:""
  })

  const {loading} = useSelector((state) => state.UserResetPassword)

  const handleChange = (newValue) => {
    setOtp(newValue);
  };

  const validateChar = (value, index) => {
    return !isNaN(value);
  };

  const handelSubmit = (e)=> {
    e.preventDefault()

    if(otp.length !== 6){
      setMsgError((prev) =>( {...prev,otp:true, msg:'code invalide'}))
    }else if(!password){
      setMsgError((prev) =>( {...prev,password:true, msg:'mot de pass invalide'}))
    }else if(!passwordConfirmation){
      setMsgError((prev) =>( {...prev,passwordConfirmation:true, msg:'Confirmation mot de passe invalide'}))
    }else if(passwordConfirmation !== password){
      setMsgError((prev) =>( {...prev,passwordIsConfirmation:true,msg:'Le mot de passe ne correspond pas'}))
    }else{
      dispatch(resetPassword({otp, password, passwordConfirmation})).unwrap()
      .then((res) => {
        navigate(`${lang}/auth`)
      })
      .catch((rejectedValueOrSerializedError) => {
       setMsgError((prev) =>( {...prev,error:true , msg:rejectedValueOrSerializedError}))
      })

    }
  }


  return (
    <div className="parent">
      <Container
        sx={{ width: "100%", height: "100%", display: "flex" }}
        maxWidth={"xl"}
        component="div"
        className="container"
      >
        <div className="right">
          <div className="form-container">

          {
          msgError.error ? 
          (<Alert severity="error" sx={{width:"100%",marginBottom:"12px"}}>{msgError.msg}</Alert>) :
           msgError.otp || msgError.password || msgError.passwordConfirmation || msgError.passwordIsConfirmation ? 
          (<Alert severity="error" sx={{width:"100%",marginBottom:"12px"}}>{msgError.msg}</Alert>) : ""
          }
       
            <form method="POST" onSubmit={handelSubmit}>
              <label style={{ color: "#1c1c1c", textAlign: "center" }}>
                Code envoyé à votre e-mail. Entrez le code pour réinitialiser.
              </label>
              <MuiOtpInput
                itemType="number"
                length={6}
                value={otp}
                onChange={handleChange}
                validateChar={validateChar}
              />
              <span className="line"></span>
              <FormControl sx={{ width: '100%' }} variant="outlined" className="mui-formcontrolle-password">

              <InputLabel htmlFor="outlined-adornment-password" sx={{ background: '#fff' }}>Password</InputLabel>
              <OutlinedInput
                id="outlined-adornment-password"
                error={msgError.password}
                onChange={(e) => setPassword(e.target.value)}
                variant="outlined"
                type={showPassword ? 'text' : 'password'}
                endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  size={'small'}
                >
                  {showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
                sx={{ width: "100%" }}
              />
              </FormControl>
              
              <TextField
                id="outlined-basic"
                label="Confirmation mot de passe"
                type={"password"}
                error={msgError.passwordConfirmation}
                onChange={(e) => setPasswordConfirmation(e.target.value)}
                variant="outlined"
                sx={{ width: "100%" }}
              />

              
              <LoadingButton
                type="submit"
                loading={loading}
                variant="contained"
                sx={{ width: "100%" }}
              >
                réinitialiser
              </LoadingButton>
            </form>
          </div>
        </div>
      </Container>
    </div>
  );
};

export default withGuest(UserResetPassword, 'user');
