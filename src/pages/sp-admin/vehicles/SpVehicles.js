import {Box, Button, Card, CardContent, Grid, Skeleton} from '@mui/material'
import React, { useCallback } from 'react'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CardBox from '../../../components/CardBox/CardBox'
import DashBoardHeader from '../../../components/DashBoardHeader/DashBoardHeader'
import SearchInput from '../../../components/SearchInput/SearchInput'
import withGuard from '../../../hoc/withGuard'
import { openFullModal } from '../../../store/reducer/FullModalSlice'
import { openResponsiveModal } from '../../../store/reducer/ResponsiveModalSlice'
import { getVehicules } from '../../../store/reducer/vehicle/VehicleSlice'

import styles from './styles.module.css'
import { getVehiculesType } from '../../../store/reducer/vehicle/VehicleTypeSlice'
import { QrCode } from '@mui/icons-material'

const SpVehicles = () => {
  const dispatch = useDispatch()
  const {loading , vehicules, fitredVehicule} = useSelector(state => state.Vehicle)


  const getVehiculesTypeIfChanged = useCallback(() => {
        dispatch(getVehiculesType())
}, [dispatch]);


  useEffect(()=> {
    dispatch(getVehicules())
    getVehiculesTypeIfChanged()
  } , [dispatch, getVehiculesTypeIfChanged])

  return (
    <Box className={styles.content} component="main">
      <DashBoardHeader>
        <Box
          sx={{
            width: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <SearchInput />
        </Box>
      </DashBoardHeader>

      <Box className={styles.actions}>
        <Button
          sx={{ width: "300px" }}
          onClick={() => dispatch(openResponsiveModal({componentName: "AddVehicle", title:"AddVehicle"}))}
          variant="contained"
        >
          Ajouter véhicule
        </Button>
        <Button
          sx={{ width: "300px" }}
          onClick={() => dispatch(openResponsiveModal({componentName: "AddVehicleType", title:"Ajouter nouveau type"}))}
          variant="contained"
        >
          Ajouter nouveau type
        </Button>

        <Button
          sx={{ width: "300px" }}
          onClick={() => dispatch(openResponsiveModal({componentName: "PrintVehicleQrCode", title:"Imprimer QrCodes"}))}
          variant="contained"
          endIcon={<QrCode />}
        >
          QrCodes 
        </Button>
      </Box>

      <Box 
      >
        {/* Cards */}

        <Grid
          container
          p={'16px'}
          justifyContent={"center"}
          alignItems={"center"}
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 1, sm: 8, md: 12 , xl: 12  }}
        >
          {
            loading ? (
              Array.from(new Array(8)).map((item, index) => (
                <Grid item={true} 
                  justifyItems={"center"}
                  alignItems={"center"}
                  xs={1} sm={4} md={4} xl={3} key={index}>
              
              <Card sx={{ maxWidth: 345, m: 2 }}>
      
      {loading ? (
        <Skeleton sx={{ height: 190 }} animation="wave" variant="rectangular" />
      ) : ""}

      <CardContent>
        {loading ? (
          <React.Fragment>
            <Skeleton animation="wave" height={16} width='80%' style={{ marginBottom: 6 }} />
            <Skeleton animation="wave" variant='rectangular' height={60}  />
          </React.Fragment>
        ) : ""}
      </CardContent>
    </Card>
            </Grid>
              )
            ))
             :
            (fitredVehicule.length ? fitredVehicule :vehicules).map((vehicule, index) => (
            <Grid item={true} 
            onClick={() => dispatch(openFullModal({ vehicule: vehicule, role: "SpAdmin" }))}
                  justifyItems={"center"}
                alignItems={"center"}
             xs={1} sm={4} md={4} xl={3} key={index}>
              <CardBox 
                image={vehicule.image}
                qrCode={vehicule.qr_code}
                loading={loading}
                nom={vehicule.nom}
                kilometrage={vehicule.kilometrage}
                matricule={vehicule.immatriculation}
              />
            </Grid>
          ))}
        </Grid>
      </Box>
    </Box>
  );
}

export default withGuard(SpVehicles, 'SpAdmin')