import { Box, Button, Divider, SpeedDial } from "@mui/material";
import React, { useEffect} from "react";
import withGuard from "../../../hoc/withGuard";
import DashBoardHeader from "../../../components/DashBoardHeader/DashBoardHeader";
import styles from "./styles.module.css";
import PointOfSaleIcon from '@mui/icons-material/PointOfSale';
import SoftBox from "../../../components/SoftBox/SoftBox";
import LocalGasStationIcon from "@mui/icons-material/LocalGasStation";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css";
import "swiper/css/navigation";
import { Navigation } from "swiper";
import EditTable from "../../../components/EditTable/EditTable";
import { useDispatch, useSelector } from "react-redux";
import { getFuel } from "../../../store/reducer/Fuel/FuelSlice";
import { openFullModal } from "../../../store/reducer/FullModalSlice";
import moment, { now } from "moment/moment";
import { openResponsiveModal } from "../../../store/reducer/ResponsiveModalSlice";
import FuelChart from "../../../components/AddFuel/FuelChart";


const SpFuel = () => {
  const dispatch = useDispatch();

  const {fuels} = useSelector(state => state.Fuel)
  
  const columns = [
    { field: "prix", headerName: "Prix/L", width: 80 , type: 'number'},
    { field: "quantite", headerName: "Quantite/L", width: 100, type: 'number' },
    { field: "cout", headerName: "Cout/dhs", width: 100 , type: 'number'},
    { field: "kilometrage", headerName: "Kilometrage", width: 120, type: 'number' },
  
    {
      field: "type",
      headerName: "Type",
      width: 70,
    },
  
    {
      field: "created_at",
      headerName: "Date",
      type: "dateTime",
      valueGetter : ({value}) => new Date(value),
      width: 170,
    },
    {
      field: "stationService",
      headerName: "Station",
      width: 140,
      sortable: false,
      renderCell: ({ row }) => (
        <Button
          sx={{ fontSize: "12px" , height:'80%'}}
          variant="contained"
          fullWidth
          onClick={() => handelStationClick(row.stationService)}
          color="success"
        >
          {row.stationService.nom }
        </Button>
      ),
    },
    {
      field: "vehicule",
      headerName: "Vehicule",
      sortable: false,
      width: 140,
      renderCell: ({ row }) => (
        <Button
          sx={{ fontSize: "12px" , height:'80%'}}
          variant="contained"
          fullWidth
          onClick={() => handelVehiculeClick(row.vehicule)}
          color="success"
        >
          {row.vehicule.nom  }
          <br/>
          {row.vehicule.immatriculation}
        </Button>
      ),
    },
    {
      field: "carbure_par",
      headerName: "Par",
      sortable: false,
      width: 140,
      renderCell: ({ row }) => (
        <Button
          sx={{ fontSize: "12px" , height:'80%'}}
          variant="contained"
          fullWidth
          onClick={() => handelParClick(row.carbure_par)}
          color="success"
        >
          {row.carbure_par.nom}
          <br/>
          { row.carbure_par.prenom}
        </Button>
      ),
    },
  ];

  useEffect(() => {
    dispatch(getFuel());
  }, [dispatch]);


  const handelStationClick = (data) => {

    dispatch(openResponsiveModal({
      componentName: 'GasStation',
      childrenProps: data,
      title: data.nom 
    }))

    console.log(data);
  }

  const handelVehiculeClick = (data) => {
    dispatch(openFullModal({ vehicule: data, role: "SpAdmin" }))
    
    console.log(data);
  }

  const handelParClick = (data) => {
    dispatch(openResponsiveModal({
      componentName: 'UserCart',
      childrenProps: data,
      title: ''
    }))
    console.log(data);
  }

  const getPleins = () => {
    const pleins  = {
      cout :null,
      j: [],
      m: [],
      a: []
    }

    fuels.filter((e) =>{
         if(
            moment(e.created_at).format('DD') === moment(now()).format('DD')
         ){
          pleins.j.push(e)
         }

         if(
          moment(e.created_at).format('MM') === moment(now()).format('MM')
        ){
          pleins.m.push(e)
          }

          if(
            moment(e.created_at).format('YYYY') === moment(now()).format('YYYY')
          ){
            pleins.a.push(e)
            }
            return ""
    })

    pleins.cout = fuels.reduce((a, b) => a + Number(b.cout), 0)
    return pleins
  }
  return (
    <Box className={styles.content} component="main">
    
    <SpeedDial
        ariaLabel="Ajouter carburant"
        onClick={() => dispatch( openResponsiveModal({componentName: 'AddFuel' ,title: 'Ajouter Carburant'}) )}
        
        sx={{ position: 'absolute', bottom: 16, right: 16}}
        icon={<LocalGasStationIcon />}
      >

      </SpeedDial>
      <DashBoardHeader>Carburant</DashBoardHeader>
      <Swiper
        spaceBetween={16}
        slidesPerView={"auto"}
        modules={[Navigation]}
        navigation
        breakpoints={{
          639: {
            slidesPerView: 2,
          },
          865: {
            slidesPerView: 2,
          },
          1000: {
            slidesPerView: 3,
          },
          1500: {
            slidesPerView: 4,
          },
          1700: {
            slidesPerView: 4,
          },
        }}
        className={styles.mySwiper}
      >
        <SwiperSlide className={styles.swiper_slide}>
          <SoftBox
            icon={<PointOfSaleIcon fontSize="inherit" />}
            title="coût total - dhs"
            body={getPleins().cout}
          />
        </SwiperSlide>
        <SwiperSlide className={styles.swiper_slide}>
          <SoftBox
            icon={<LocalGasStationIcon fontSize="inherit" />}
            title="N° Remplissages ( j )"
            body={getPleins().j.length}
          />
        </SwiperSlide>

        <SwiperSlide className={styles.swiper_slide}>
          <SoftBox
            icon={<LocalGasStationIcon fontSize="inherit" />}
            title="N° Remplissages ( mois )"
            body={getPleins().m.length}
          />
        </SwiperSlide>
        <SwiperSlide className={styles.swiper_slide}>
          <SoftBox
            icon={<LocalGasStationIcon fontSize="inherit" />}
            title="N° Remplissages ( année ) "
            body={getPleins().a.length}
          />
        </SwiperSlide>
      </Swiper>

      <Divider />

      <Box className={styles.chart}>
      <FuelChart/>
      </Box>

      <Divider />

      <EditTable
        pageSize={5}
        pageSizeOptions={[5, 10, 25]}
        columns={columns}
        rows={fuels}
        storeName="Fuel"
      />
    </Box>
  );
};



export default withGuard(SpFuel, "SpAdmin");
