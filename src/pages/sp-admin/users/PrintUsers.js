import { Button } from '@mui/material'
import { GridCloseIcon } from '@mui/x-data-grid'
import React, { useState } from 'react'
import SelectMultiple from '../../../components/SelectMultiple/SelectMultiple'
import CheckIcon from '@mui/icons-material/Check';
import PrintIcon from '@mui/icons-material/Print';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import { useDispatch, useSelector } from 'react-redux';

import DownloadIcon from '@mui/icons-material/Download';

import { LoadingButton } from '@mui/lab';
import { enqueueSnackbar } from 'notistack';

import downloadFile from '../../../helpers/fileDownload';
import { printUsers } from '../../../store/reducer/sp-admin/users/PrintUsersSlice';

const PrintUsers = () => {

  const dispatch = useDispatch()
  const {users} = useSelector(state => state.GetUsers)

  const {loading, pdf} = useSelector(state => state.PrintUsers)

  let ids = []

  const handelUsersPrint = ()=>{
    if(ids.length > 0){

      dispatch(printUsers(ids)).unwrap()
      .then((res) => {

        downloadFile(res.pdf , "MaFlotte_Users.pdf")

        enqueueSnackbar('utilisateurs imprimer avec succès' ,{ variant : 'success'})
      })
      .catch((err) => {
        enqueueSnackbar('impossible de imprimer les utilisateurs' ,{ variant : 'error'})
      });
    }

  } 

  const getData = (data) =>{
     ids = data.map((user) => user.id)
  }
  
  const [open , setOpen] = useState(false)

  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <>
      <Button
        variant="contained"
        sx={{ width: "100%", gap: "16px" }}
        onClick={handleClickOpen}
      >
        <PrintIcon /> Imprimer
      </Button>

      <Dialog
        fullScreen={fullScreen}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">Imprimer Comptes</DialogTitle>

        <DialogContent>
          <DialogContentText pb={"8px"}>
            selectioner les admins
          </DialogContentText>

          <SelectMultiple
            label="Users"
            data={users}
            getData={getData}
            viewField={["nom", "prenom", "cin"]}
          />

          {pdf ? (
            <Button
              href={pdf}
              onClick={() =>setOpen(false)}
              target="_blank"
              color='info'
              variant='contained'
              sx={{ mt: "10px" ,  gap:'16px'}}
              download='Comptes.pdf'
              endIcon={<DownloadIcon/>}
              rel="noreferrer"
              
            >

              Telecharger 
            </Button>
          ) : (
            ""
          )}
        </DialogContent>

        <DialogActions sx={{ md: { paddingBottom: "100px" } }}>
          <Button disabled={loading} sx={{ gap: "12px" }} onClick={handleClose}>
            <GridCloseIcon /> Annuler
          </Button>

          <LoadingButton
            loading={loading}
            href={""}
            sx={{ gap: "12px" }}
            onClick={handelUsersPrint}
          >
            <CheckIcon /> imprimer
          </LoadingButton>
        </DialogActions>
      </Dialog>
    </>
  );
}

export default PrintUsers