import { Avatar, Box, Divider, FormControl, TextField } from "@mui/material";
import React, { useEffect, useState } from "react";
import withGuard from "../../../hoc/withGuard";
import DashBoardHeader from "../../../components/DashBoardHeader/DashBoardHeader";
import styles from "./styles.module.css";
import SoftBox from "../../../components/SoftBox/SoftBox";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import PersonAddAlt1Icon from '@mui/icons-material/PersonAddAlt1';
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css";
import "swiper/css/navigation";
import { Navigation } from "swiper";

import { LoadingButton } from "@mui/lab";
import EditTable from "../../../components/EditTable/EditTable";
import moment from "moment/moment";
import { useDispatch, useSelector } from "react-redux";
import { enqueueSnackbar } from "notistack";
import { addUserToState, getUsers } from "../../../store/reducer/sp-admin/users/GetUsersSlice";
import { addUser } from "../../../store/reducer/sp-admin/users/AddUserSlice";
import DeleteUsers from "./DeleteUsers";
import PrintUsers from "./PrintUsers";
import UpdateUser from "./UpdateUser";


const SpUsers = () => {
  const dispatch = useDispatch()

  const {loading, users} = useSelector((s) => s.GetUsers )

  const {loading:AddIsLoading} = useSelector((s) => s.AddUser)

  const [nom,setNom] = useState('')
  const [prenom,setPrenom] = useState('')
  const [identifiant,setIdentifiant] = useState('')
  const [role,setRole] = useState('')

  const [msgError,setMsgError] = useState({
    nom : false,
    prenom : false,
    identifiant : false,
    role : false,
    error : false,
    msg:""
  })


  useEffect(()=>{
    dispatch(getUsers())
  }, [dispatch])


  const handelSubmit = (e) => {
    e.preventDefault()

    const data = {nom , prenom , role, identifiant}

    if(!data.nom){
     setMsgError((prev) =>( {...prev,nom:true}))
    }else{
     setMsgError((prev) =>( {...prev,nom:false}))
    }

    if(!data.prenom){
     setMsgError((prev) =>( {...prev,prenom:true}))
    }else{
     setMsgError((prev) =>( {...prev,prenom:false}))
    }
    if(!data.role){
      setMsgError((prev) =>( {...prev,role:true}))
     }else{
      setMsgError((prev) =>( {...prev,role:false}))
     }
    if(!data.identifiant){
      setMsgError((prev) =>( {...prev,identifiant:true}))
     }else{
      setMsgError((prev) =>( {...prev,identifiant:false}))
     }

     
    if(data.nom && data.prenom && data.role && data.identifiant){
     

      dispatch(addUser(data)).unwrap()
       .then((res) => {
        console.log(res.user);
        dispatch(addUserToState(res.user))
        setNom('')
        setPrenom('')
        setRole('')
        setIdentifiant('')
        enqueueSnackbar(data.nom +' | Ajouter avec succès', {variant :'success'})
       })
       .catch((err) =>   {
        console.log(err);
        enqueueSnackbar(err, {variant :'error'})
       })
      
    }

 }
  return (
    <Box className={styles.content} component="main">
      <DashBoardHeader>Utilisateurs</DashBoardHeader>
      <Swiper
        spaceBetween={16}
        slidesPerView={"auto"}
        modules={[Navigation]}
        navigation
        breakpoints={{
          639: {
            slidesPerView: 2,
          },
          865: {
            slidesPerView: 2,
          },
          1000: {
            slidesPerView: 3,
          },
          1500: {
            slidesPerView: 4,
          },
          1700: {
            slidesPerView: 4,
          },
        }}
        className={styles.mySwiper}
      >
        <SwiperSlide className={styles.swiper_slide}>
          <SoftBox
            icon={<ManageAccountsIcon fontSize="inherit" />}
            title="N° d'Utilisateurs"
            link={"/sp-admin/users-list"}
            body={loading ? '..' :users.length}
          />
        </SwiperSlide>
        <SwiperSlide className={styles.swiper_slide}>
          <SoftBox
            icon={<ManageAccountsIcon fontSize="inherit" />}
            title="Utilisateurs Active"
            body={loading ? '..' :'...'}
          />
        </SwiperSlide>
        <SwiperSlide className={styles.swiper_slide}>
          <SoftBox
            icon={<ManageAccountsIcon fontSize="inherit" />}
            title="N° d'Utilisateurs"
            body={"0"}
          />
        </SwiperSlide>
        <SwiperSlide className={styles.swiper_slide}>
          <SoftBox
            icon={<ManageAccountsIcon fontSize="inherit" />}
            title="N° d'administrateurs"
            body={"20"}
          />
        </SwiperSlide>
      </Swiper>


      <Divider />

      <Box className={styles.parent}>

        <Box className={styles.form_content}>
          <form method="POST" onSubmit={handelSubmit}>

            <FormControl className={styles.formcontrol}>
              <TextField
                id="nom"
                error={msgError.nom}
                label="nom"
                onChange={(e) => setNom(e.target.value)}
                value={nom}
                autoComplete="on"
                variant="outlined"
                sx={{ width: "100%" }}
              />

              <TextField
                id="prenom"
                error={msgError.prenom}
                label="prenom"
                autoComplete="on"
                variant="outlined"
                onChange={(e) => setPrenom(e.target.value)}
                value={prenom}
                sx={{ width: "100%" }}
              />
            </FormControl>

            <FormControl className={styles.formcontrol}>
              <TextField
                id="Identifiant"
                error={msgError.identifiant}
                label="Identifiant"
                autoComplete="on"
                value={identifiant}
                onChange={(e) => setIdentifiant(e.target.value)}
                variant="outlined"
                sx={{ width: "100%" }}
              />

              <TextField
                id="rôle"
                label="rôle"
                error={msgError.role}
                onChange={(e) => setRole(e.target.value)}
                autoComplete="on"
                variant="outlined"
                value={role}
                sx={{ width: "100%" }}
              />
            </FormControl>

            <LoadingButton
              type="submit"
              loading={AddIsLoading}
              variant="contained"
              sx={{ width: "100%", gap: "16px" }}
            >
              <PersonAddAlt1Icon /> Ajouter
            </LoadingButton>
          </form>
        </Box>
        <Box className={styles.actions_content}>

    

          <PrintUsers/>
          <UpdateUser/>
          <DeleteUsers/>
         


        </Box>
      </Box>
      <Divider />

      <EditTable
      pageSize={5}
      pageSizeOptions={[5 , 10 , 25]}
      columns={columns}
      rows={users}
      role="SpAdmin"
      storeName = 'UpdateUser'
      updated = 'user'
      loading={loading}
      />
    </Box>
  );
}

const columns = [
  { field: 'avatar', headerName: 'Avatar', width: 100,

    editable: false,
    sortable : false,
    filtrable: false,
    renderCell : (params) => <Avatar sx={{ width: 48, height: 48 }} 
    alt={params.row.nom + ' '+ params.row.prenom }
    title={params.row.nom + ' '+ params.row.prenom }
     src={params.row.image} />
  },
  { field: 'nom', headerName: 'Nom', width: 130, editable: true },
  { field: 'prenom', headerName: 'Prenom', width: 130, editable: true },
  { field: 'cin', headerName: 'Identifiant', width: 120, editable: true },
  { field: 'role', headerName: 'Rôle', width: 130, editable: true },
  { field: 'sexe', headerName: 'Sexe', type: 'singleSelect',
  valueOptions: ['H', 'F'], width: 70, editable: true },
  {
    field: 'tel',
    headerName: 'Tel',
    width: 160,
    editable: true,
  },
  {
    field: 'created_at',

    headerName: 'date Créée',

    width: 180,
    editable: false,
    renderCell : (params) => {return (moment(params.row.created_at).format('YYYY-MM-DD hh:mm')) },
  },
  {
    field: 'devices_connected',
    headerName: 'appareils connectés',
    type: 'string',
    width: 200,
    editable: false,
  },
];

export default withGuard(SpUsers, 'SpAdmin')