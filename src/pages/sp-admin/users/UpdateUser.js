import { Autocomplete, Box, Button, TextField } from '@mui/material'
import { GridCloseIcon } from '@mui/x-data-grid'
import React, { useState } from 'react'
import CheckIcon from '@mui/icons-material/Check';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import EditIcon from '@mui/icons-material/Edit';
import { useDispatch, useSelector } from 'react-redux';
import { LoadingButton } from '@mui/lab';
import { enqueueSnackbar } from 'notistack';

import { updateUsersState } from '../../../store/reducer/sp-admin/users/GetUsersSlice';
import { updateUser } from '../../../store/reducer/sp-admin/users/UpdateUserSlice';

const UpdateUser = () => {

  const dispatch = useDispatch()
  const {users} = useSelector(state => state.GetUsers)
  const {loading} = useSelector(state => state.UpdateUser)


  const [userId, setUserId] = useState('')
  const [field, setField] = useState('')
  const [data, setData] = useState('')



  const handelAdminUpdate = ()=>{
    
    if(field  && data && userId)
            
        
    
        console.log(userId);
        dispatch(updateUser({id :userId , field , value : data})).unwrap()
        .then((res) => {

          dispatch(updateUsersState(res.user))
          setOpen(false)
          enqueueSnackbar(field +' mis à jour avec succès', {variant :'success'});
          setData('')
          setUserId('')
          setField('')
        })
        .catch((err) => {
          console.log(err);
           enqueueSnackbar(err, {variant :'error'});

        });
    } 


  


  const [open , setOpen] = useState(false)

  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (

    <>
    <Button variant="contained"  sx={{ width: "100%", gap:'16px' }}onClick={handleClickOpen}>
      <EditIcon  />   Modifier
    </Button>

    <Dialog
      fullScreen={fullScreen}
      open={open}
      onClose={handleClose}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogTitle id="responsive-dialog-title"   >
        Modifier
      </DialogTitle>

      <DialogContent >
        <DialogContentText pb={'8px'}>
            selectioner user et le champe 
        </DialogContentText>

                <Box sx={{width:'100%',display:'flex',flexDirection:'column', gap:'16px'}}>
                <Autocomplete
                    sx={{ width: "100%", minWidth: "300px" }}
                    options={users}
                    id="users"
                    onChange={(e, val) => {

                            setUserId(val?.id ? val.id : '');
                            
                    }}
                   
                    getOptionLabel={(item) =>
                    (item['nom'] ?  item['nom'] :" ") + " " + 
                    (item['prenom'] ?  item['prenom'] :" ") + " " + 
                    (item['cin'] ? " | " + item['cin'] :" ")
                    }
               
                    renderInput={(params) => <TextField {...params} label="selectioner user" />}
                />

            
                <Autocomplete
                    sx={{ width: "100%", minWidth: "300px" }}
                    options={['nom', 'prenom' , 'role', 'tel' , 'email', 'sexe' ]}
                    id="fields"
                    value={field}
                    onChange={(e, val) => {
                        setField(val); 
                    }}
                    getOptionLabel={(item) =>  item}
               
                    renderInput={(params) => <TextField {...params} label="selectioner le champe" />}
                />

                {userId && field ? 
                (<TextField id="data" value={data} autoFocus={true} placeholder={users.find(ad => ad.id === userId)[field]} onChange={(event) => setData(event.target.value)} label="entrer la valeur" variant="outlined" />) 
                : ''}
                </Box>

      </DialogContent>

      <DialogActions sx={{ md : {paddingBottom : '100px'}}}>

      <Button disabled={loading} sx={{ gap: "12px" }} color="success" variant='contained' onClick={handleClose}>
        <GridCloseIcon /> Annuler
      </Button>

      <LoadingButton loading={loading} sx={{ gap: "12px" }} color='warning' variant='contained' onClick={handelAdminUpdate}>
        <CheckIcon /> Modifier
      </LoadingButton>
      </DialogActions>
    </Dialog>
  </>
  );
}

export default UpdateUser