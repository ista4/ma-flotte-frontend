import { Button } from '@mui/material'
import { GridCloseIcon } from '@mui/x-data-grid'
import React, { useState } from 'react'
import SelectMultiple from '../../../components/SelectMultiple/SelectMultiple'
import CheckIcon from '@mui/icons-material/Check';

import { LoadingButton } from '@mui/lab';

import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import PersonRemoveIcon from '@mui/icons-material/PersonRemove';
import { useDispatch, useSelector } from 'react-redux';


import { deleteUsers  } from '../../../store/reducer/sp-admin/users/DeleteUsersSlice';
import { removeUsers } from '../../../store/reducer/sp-admin/users/GetUsersSlice';
import { enqueueSnackbar } from 'notistack';

const DeleteUsers = () => {

  const dispatch = useDispatch()
  const {users} = useSelector(state => state.GetUsers)
  const {loading} = useSelector(state => state.DeleteUsers)
  let ids = []

  const handelUsersDelete = ()=>{
    if(ids.length > 0){

      dispatch(deleteUsers(ids)).unwrap()
      .then((res) => {
        setOpen(false);
        enqueueSnackbar('utilisateurs supprimés avec succès' ,{ variant : 'success'})
        dispatch(removeUsers(ids))
      })
      .catch((err) => {
        enqueueSnackbar('impossible de supprimer les utilisateurs' ,{ variant : 'error' })
      });
    }

  } 
  const getData = (data) =>{
     ids = data.map((user) => user.id)
  }
  
  const [open , setOpen] = useState(false)

  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (

    <>
    <Button variant="contained" color={'error'} sx={{ width: "100%", gap:'16px' }}onClick={handleClickOpen}>
      <PersonRemoveIcon />   Suprimmer
    </Button>

    <Dialog
      fullScreen={fullScreen}
      open={open}
      onClose={handleClose}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogTitle id="responsive-dialog-title"  color="error" >
        Suprimmer
      </DialogTitle>

      <DialogContent >
        <DialogContentText pb={'8px'}>
            selectioner les admins 
        </DialogContentText>

        <SelectMultiple
        label="Users"
        data={users}
        getData={getData}
        viewField={["nom", "prenom" , "cin"]}
      />

      </DialogContent>

      <DialogActions sx={{ md : {paddingBottom : '100px'}}}>

      <Button disabled={loading} sx={{ gap: "12px" }} color="success" variant='contained' onClick={handleClose}>
        <GridCloseIcon /> Annuler
      </Button>

      <LoadingButton loading={loading} sx={{ gap: "12px" }} color="error" variant='contained' onClick={handelUsersDelete}>
        <CheckIcon /> Supprimer
      </LoadingButton>
      </DialogActions>
    </Dialog>
  </>
  );
}

export default DeleteUsers