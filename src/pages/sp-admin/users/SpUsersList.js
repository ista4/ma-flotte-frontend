import { Avatar, Box } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import moment from "moment";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import DashBoardHeader from "../../../components/DashBoardHeader/DashBoardHeader";
import withGuard from "../../../hoc/withGuard";

import { getUsers } from "../../../store/reducer/sp-admin/users/GetUsersSlice";

import styles from "./styles.module.css";

const SpUsersList = () => {
  const dispatch = useDispatch();
  const { loading, users } = useSelector((s) => s.GetUsers);

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  return (
    <Box className={styles.content} component="main">
      <DashBoardHeader>Sp Users List</DashBoardHeader>

      <div
      
        style={{
          height: "100%",
          width: "100%",
          background: "#fff",
          marginTop: "16px",
        }}
      >
        <DataGrid
          rows={users}
          columns={columns}
          loading={loading}
          slots={{ toolbar: GridToolbar }}
          sx={{ fontWeight: "bold" }}
        />
      </div>
    </Box>
  );
};

const columns = [
  {
    field: "avatar",
    headerName: "Avatar",
    width: 100,

    editable: false,
    sortable: false,
    filtrable: false,
    renderCell: (params) => (
      <Avatar
        sx={{ width: 48, height: 48 }}
        alt={params.row.nom + " " + params.row.prenom}
        title={params.row.nom + " " + params.row.prenom}
        src={params.row.image}
      />
    ),
  },
  { field: "nom", headerName: "Nom", width: 140 },
  { field: "prenom", headerName: "Prenom", width: 140 },
  { field: "cin", headerName: "Identifiant", width: 120 },
  { field: "password", headerName: "Mot de Pass", width: 100 },
  { field: "role", headerName: "Role", width: 130 },
  { field: "sexe", headerName: "Sexe", width: 70 },
  {
    field: "email",
    headerName: "Email",
    width: 190,
  },
  {
    field: "tel",
    headerName: "Tel",
    width: 160,
  },
  {
    field: "devices_connected",
    headerName: "appareils connectes",
    type: "string",
    width: 200,
    editable: false,
  },
  {
    field: 'email_verified_at',
    headerName: 'Verifier a',
    type:'dateTime',
    width: 190,
    editable: false,
    valueGetter : ({value}) => { return  value ?  new Date(value) :'' },
  },
  {
    field: "created_at",
    type: "string",
    headerName: "date Creee",
    width: 180,
    editable: false,
    renderCell: (params) => {
      return moment(params.row.created_at).format("YYYY-MM-DD hh:mm");
    },
  },
 
];

export default withGuard(SpUsersList, "SpAdmin");
