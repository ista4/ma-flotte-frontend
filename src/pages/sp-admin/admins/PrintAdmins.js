import { Button } from '@mui/material'
import { GridCloseIcon } from '@mui/x-data-grid'
import React, { useState } from 'react'
import SelectMultiple from '../../../components/SelectMultiple/SelectMultiple'
import CheckIcon from '@mui/icons-material/Check';
import PrintIcon from '@mui/icons-material/Print';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import { useDispatch, useSelector } from 'react-redux';

import DownloadIcon from '@mui/icons-material/Download';

import { LoadingButton } from '@mui/lab';
import { enqueueSnackbar } from 'notistack';
import { printAdmins } from '../../../store/reducer/sp-admin/admins/PrintAdminsSlice';
import downloadFile from '../../../helpers/fileDownload';

const PrintAdmins = () => {

  const dispatch = useDispatch()
  const {admins} = useSelector(state => state.GetAdmins)

  const {loading , pdf} = useSelector(state => state.PrintAdmins)

  let ids = []

  const handelAdminPrint = ()=>{
    if(ids.length > 0){

      dispatch(printAdmins(ids)).unwrap()
      .then((res) => {

        

        downloadFile(res.pdf , "download.pdf")

        enqueueSnackbar('administrateurs imprimer avec succès' ,{ variant : 'success'})
      })
      .catch((err) => {
        enqueueSnackbar('impossible de imprimer les administrateurs' ,{ variant : 'error'})
      });
    }

  } 
  const getData = (data) =>{
     ids = data.map((admin) => admin.id)
  }
  
  const [open , setOpen] = useState(false)

  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <>
      <Button
        variant="contained"
        sx={{ width: "100%", gap: "16px" }}
        onClick={handleClickOpen}
      >
        <PrintIcon /> Imprimer
      </Button>

      <Dialog
        fullScreen={fullScreen}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">Imprimer Comptes</DialogTitle>

        <DialogContent>
          <DialogContentText pb={"8px"}>
            selectioner les admins
          </DialogContentText>

          <SelectMultiple
            label="Admins"
            data={admins}
            getData={getData}
            viewField={["nom", "prenom", "cin"]}
          />

          {pdf ? (
            <Button
              href={pdf}
              onClick={() =>setOpen(false)}
              target="_blank"
              color='info'
              variant='contained'
              sx={{ mt: "10px" ,  gap:'16px'}}
              download='Comptes.pdf'
              endIcon={<DownloadIcon/>}
              rel="noreferrer"
              
            >

              Telecharger 
            </Button>
          ) : (
            ""
          )}
        </DialogContent>

        <DialogActions sx={{ md: { paddingBottom: "100px" } }}>
          <Button disabled={loading} sx={{ gap: "12px" }} onClick={handleClose}>
            <GridCloseIcon /> Annuler
          </Button>

          <LoadingButton
            loading={loading}
            href={""}
            sx={{ gap: "12px" }}
            onClick={handelAdminPrint}
          >
            <CheckIcon /> imprimer
          </LoadingButton>
        </DialogActions>
      </Dialog>
    </>
  );
}

export default PrintAdmins