import { Autocomplete, Box, Button, TextField } from '@mui/material'
import { GridCloseIcon } from '@mui/x-data-grid'
import React, { useState } from 'react'
import SelectMultiple from '../../../components/SelectMultiple/SelectMultiple'
import CheckIcon from '@mui/icons-material/Check';

import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import EditIcon from '@mui/icons-material/Edit';
import { useDispatch, useSelector } from 'react-redux';


import { LoadingButton } from '@mui/lab';
import { enqueueSnackbar } from 'notistack';
import { updateAdmin } from '../../../store/reducer/sp-admin/admins/UpdateAdminSlice';
import { updateAdminsState } from '../../../store/reducer/sp-admin/admins/GetAdminsSlice';

const UpdateAdmin = () => {

  const dispatch = useDispatch()
  const {admins} = useSelector(state => state.GetAdmins)
  const {loading} = useSelector(state => state.UpdateAdmin)


  const [adminId, setAdminId] = useState('')
  const [field, setField] = useState('')
  const [data, setData] = useState('')


  const handelAdminUpdate = ()=>{
    
    if(field  && data && adminId)
            
        dispatch(updateAdmin({id :adminId , field , value : data})).unwrap()
        .then((res) => {

          dispatch(updateAdminsState(res.admin))
          setOpen(false)
          enqueueSnackbar(field +' mis à jour avec succès', {variant :'success'});
          setData('')
          setAdminId('')
          setField('')
        })
        .catch((err) => {
          console.log(err);
           enqueueSnackbar(err, {variant :'error'});

        });
    } 


  


  const [open , setOpen] = useState(false)

  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (

    <>
    <Button variant="contained"  sx={{ width: "100%", gap:'16px' }}onClick={handleClickOpen}>
      <EditIcon  />   Modifier
    </Button>

    <Dialog
      fullScreen={fullScreen}
      open={open}
      onClose={handleClose}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogTitle id="responsive-dialog-title"   >
        Modifier
      </DialogTitle>

      <DialogContent >
        <DialogContentText pb={'8px'}>
            selectioner l'admin et le champe 
        </DialogContentText>

                <Box sx={{width:'100%',display:'flex',flexDirection:'column', gap:'16px'}}>
                <Autocomplete
                    sx={{ width: "100%", minWidth: "300px" }}
                    options={admins}

                    id="admins"
                    onChange={(e, val) => {
                            setAdminId(val?.id ? val.id : '');
                    }}
                   
                    getOptionLabel={(item) =>
                    (item['nom'] ?  item['nom'] :" ") + " " + 
                    (item['prenom'] ?  item['prenom'] :" ") + " " + 
                    (item['cin'] ? " | " + item['cin'] :" ")
                    }
               
                    renderInput={(params) => <TextField {...params} label="selectioner l'admin" />}
                />

            
                <Autocomplete
                    sx={{ width: "100%", minWidth: "300px" }}
                    options={['nom', 'prenom' , 'role', 'tel' , 'email', 'sexe' ,]}
                    value={field}
                    id="fields"
                    onChange={(e, val) => {
                        setField(val);
                            
                    }}
                    getOptionLabel={(item) =>  item}
               
                    renderInput={(params) => <TextField {...params} label="selectioner le champe" />}
                />

                {adminId && field ? 
                (<TextField id="data" value={data} autoFocus={true} placeholder={admins.find(ad => ad.id === adminId)[field]} onChange={(event) => setData(event.target.value)} label="entrer la valeur" variant="outlined" />) 
                : ''}
                </Box>

      </DialogContent>

      <DialogActions sx={{ md : {paddingBottom : '100px'}}}>

      <Button disabled={loading} sx={{ gap: "12px" }} color="success" variant='contained' onClick={handleClose}>
        <GridCloseIcon /> Annuler
      </Button>

      <LoadingButton loading={loading} sx={{ gap: "12px" }} color='warning' variant='contained' onClick={handelAdminUpdate}>
        <CheckIcon /> Modifier
      </LoadingButton>
      </DialogActions>
    </Dialog>
  </>
  );
}

export default UpdateAdmin