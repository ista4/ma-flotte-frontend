import React, { useEffect } from 'react'
import withGuard from '../../../hoc/withGuard'
import DashBoardHeader from '../../../components/DashBoardHeader/DashBoardHeader'
import styles from './styles.module.css'
import { Avatar, Box } from '@mui/material'

import { useDemoData } from '@mui/x-data-grid-generator';
import { DataGrid, GridToolbar } from '@mui/x-data-grid';
import moment from 'moment'
import { useDispatch, useSelector } from 'react-redux'
import { getAdmins } from '../../../store/reducer/sp-admin/admins/GetAdminsSlice'


const SpAdminsList = () => {

  const dispatch = useDispatch()
  const {loading, admins} = useSelector((s) => s.GetAdmins )

  useEffect(()=>{
    dispatch(getAdmins())
  }, [dispatch])



  return (
    <Box className={styles.content}  component="main">
    <DashBoardHeader>
          Admins   Liste
    </DashBoardHeader>

    <div style={{ height:'100%', width: '100%', background:'#fff', marginTop:'16px' }}>

      <DataGrid  rows={admins} columns={columns} loading={loading} slots={{ toolbar: GridToolbar }}
        sx={{fontWeight:'bold',}}
       />
    </div>
 </Box>
  )
}

const columns = [
  { field: 'avatar', headerName: 'Avatar', width: 100,

    editable: false,
    sortable : false,
    filtrable: false,
    renderCell : (params) => <Avatar sx={{ width: 48, height: 48 }} 
    alt={params.row.nom + ' '+ params.row.prenom }
    title={params.row.nom + ' '+ params.row.prenom }
     src={params.row.image} />
  },
  { field: 'nom', headerName: 'Nom', width: 140, },
  { field: 'prenom', headerName: 'Prenom', width: 140},
  { field: 'cin', headerName: 'Identifiant', width: 120},
  { field: 'password', headerName: 'Mot de Pass', width: 100},
  { field: 'role', headerName: 'Role', width: 130},
  { field: 'sexe', headerName: 'Sexe', width: 70},
  {
    field: 'email',
    headerName: 'Email',

    width: 190,
   
  },
  {
    field: 'tel',
    headerName: 'Tel',
    width: 160,
   
  },
  {
    field: 'email_verified_at',
    headerName: 'Verifier a',
    type:'dateTime',
    width: 190,
    editable: false,
    valueGetter : ({value}) => { return  value ?  new Date(value) :'' },
  },

  {
    field: 'devices_connected',
    headerName: 'appareils connectes',
    type: 'string',
    width: 200,
    editable: false,
  },
  {
    field: 'created_at',
    type: 'string',
    headerName: 'date Creee',
    width: 180,
    editable: false,
    renderCell : (params) => {return (moment(params.row.created_at).format('YYYY-MM-DD hh:mm')) },
  },
];

export default withGuard(SpAdminsList, 'SpAdmin')




