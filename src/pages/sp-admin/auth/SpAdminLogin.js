
import { LoadingButton } from '@mui/lab'
import { Alert,TextField } from '@mui/material'
import { Container } from '@mui/system'
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, useNavigate } from 'react-router-dom'
import lang from '../../../helpers/appLang'
import withAuth from '../../../hoc/withAuth'
import { spAdminLogin } from '../../../store/reducer/sp-admin/auth/SpAdminLoginSlice'

import '../../auth.css'
const SpAdminLogin = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const {loading} = useSelector((state) => state.SpAdminLogin)
  const [identifiant,setIdentifiant] = useState()

  const [password,setPassword] = useState()
  const [msgError,setMsgError] = useState({
    identifiant : false,
    password : false,
    error : false,
    msg:""
  })

  const handelSubmit = (e) => {
     e.preventDefault()

     const data = {identifiant, password}

     if(!data.identifiant){
      setMsgError((prev) =>( {...prev,identifiant:true}))
     }else{
      setMsgError((prev) =>( {...prev,identifiant:false}))
     }
     if(!data.password){
      setMsgError((prev) =>( {...prev,password:true}))
     }else{
      setMsgError((prev) =>( {...prev,password:false}))
     }


     if(data.identifiant && data.password){
       dispatch(spAdminLogin(data)).unwrap()
       .then((originalPromiseResult) => {
         if(originalPromiseResult.success){
            navigate(lang+'/sp-admin', {replace:true})
          }
          
       })
       .catch((rejectedValueOrSerializedError) => {
        setMsgError((prev) =>( {...prev,error:true , msg:"les informations d'identification invalides !"}))
       })
     }
  }

  return (

    <div className='parent'> 
       <Container sx={{width:'100%', height:'100%',display:'flex'}} maxWidth={'xl'} component="div" className='container'>
      <div className='left'>

        <div>
          <h1>Ma Flotte </h1>
          <p>Ma flotte, la meilleure offre de pneus verts et <span style={{color:'#62B250'}}>jusqu'à 10% d'économies</span> sur les coûts de carburant.</p>
        </div>

      </div>
      <div className='right'>
     
        <div className='form-container'>
        {
          msgError.error ? 
          (<Alert severity="error" sx={{width:"100%",marginBottom:"12px"}}>{msgError.msg}</Alert>) :
           msgError.identifiant || msgError.password ? (<Alert severity="error" sx={{width:"100%",marginBottom:"12px"}}>Tous les champs sont obligatoires !</Alert>) : ""
        }

          <form method='POST' onSubmit={handelSubmit}>

            <TextField id="Identifiant" 
            label="Identifiant" 
            autoComplete='on'
            error={msgError.identifiant}
            onChange={(e) => setIdentifiant(e.target.value)}
            variant="outlined" 
            sx={{width:'100%'}}
            />

            <TextField id="Mot De Passe" 
            label="Mot De Passe"
            type={'password'}
            autoComplete='off'
            error={msgError.password}
            onChange={(e) => setPassword(e.target.value)} 
            variant="outlined" 
            sx={{width:'100%'}}
            />

            <LoadingButton type='submit' loading={loading} variant='contained'  sx={{ width:'100%'}} >Se connecter</LoadingButton>

          </form>

        </div>

      </div>
      </Container>
    </div>
  

  )
}

export default withAuth(SpAdminLogin , 'SpAdmin')