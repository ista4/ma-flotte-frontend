
import Box from "@mui/material/Box";
import { Outlet } from "react-router-dom";
import AppSideBar from "../AppSideBar/AppSideBar";
import AppHeader from "../AppHeader/AppHeader";
import { Dialog } from "@mui/material";
import ResponsiveModal from "../../components/ResponsiveModal/ResponsiveModal";

function SuperAdminLayout(props) {

  return (
<>
    <div style={{
        width:'100%',
        height:'56px',
      }}>
    <AppHeader  role={'SpAdmin'}/>
    </div>  
    
    <Box sx={{ display: "flex" , width:'100%' , height:'100%'}}>
      <AppSideBar role={'SpAdmin'} />
      <Box
        component="main"
        sx={{
          width: '100%',
          height: '100%',
          overflow:'auto',
        }}
      >
      {/* content */}
      
        <Outlet/>
      </Box>
    </Box>

 
</>
  );
}

export default SuperAdminLayout;