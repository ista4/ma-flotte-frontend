import * as React from "react";

import Box from "@mui/material/Box";


import { Outlet } from "react-router-dom";

import AppSideBar from "../AppSideBar/AppSideBar";
import AppHeader from "../AppHeader/AppHeader";



function Index(props) {

  return (
<>

    <div style={{
        width:'100%',
        height:'56px',
      }}>
    <AppHeader  role={'admin'}/>
    </div>  
    
    <Box sx={{ display: "flex" , width:'100%' , height:'100%'}}>
      <AppSideBar role={'admin'}/>
      <Box
        component="main"
        sx={{
          width: '100%',
          height: '100%',
          overflow:'auto',
        }}
      >

      {/* content */}
        <Outlet/>
      </Box>
    </Box>

 
</>
  );
}

export default Index;
