import {
  Collapse,
  Divider,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Toolbar,
} from "@mui/material";

import React from "react";
import styles from "./style.module.css";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import AirportShuttleIcon from '@mui/icons-material/AirportShuttle';
import CommuteIcon from '@mui/icons-material/Commute';
import ListAltIcon from '@mui/icons-material/ListAlt';
import DirectionsCarFilledIcon from "@mui/icons-material/DirectionsCarFilled";
import LocalGasStationIcon from "@mui/icons-material/LocalGasStation";
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts';
import DashboardIcon from "@mui/icons-material/Dashboard";
import GroupIcon from '@mui/icons-material/Group';
import styled from "styled-components";
import { ExpandLess, ExpandMore, StarBorder } from "@mui/icons-material";
import GradingIcon from '@mui/icons-material/Grading';
import { useDispatch, useSelector } from "react-redux";
import { drawerToggle } from "../../store/reducer/SideBarSlice";
import { NavLink } from "react-router-dom";

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",

  // necessary for content to be below app bar
  ...theme?.mixins?.toolbar,
  justifyContent: "flex-end",
}));

const AppSideBar = ({ children, window ,role}) => {
  const container =
    window !== undefined ? () => window().document.body : undefined;
  const dispatch = useDispatch();


  const { mobileOpen } = useSelector((state) => state.sidebar);

  const [vehicules, setVehicules] = React.useState(false);
  const [users, setUsers] = React.useState(false);
  const [admins, setAdmins] = React.useState(false);

  const [selectedIndex, setSelectedIndex] = React.useState(0);

  const handleClick = (event, index , item) => {

    if(item === 'vehicules'){
      setVehicules(!vehicules);
    }

    if(item === 'users'){
      setUsers(!users);
    }

    if(item === 'admins'){
      setAdmins(!admins);
    }

    setSelectedIndex(index);
  };

  const handleListItemClick = (event, index, isCollapse) => {
    if(isCollapse){
      dispatch(drawerToggle());
    }
    setSelectedIndex(index);
  };

  const handleDrawerToggle = () => {
    dispatch(drawerToggle());
  };

  const drawer = (
    <div>
      <Toolbar edge="start" onClick={handleDrawerToggle} sx={{ mr: 2 }}>
        <DrawerHeader onClick={handleDrawerToggle}>
          <IconButton
            onClick={handleDrawerToggle}
            className={styles.DrawerHeader}
          >
            <ChevronLeftIcon color="success" fontSize="large" />
          </IconButton>
        </DrawerHeader>
      </Toolbar>

      <Divider />
      <List>
        <ListItem disablePadding>
          <NavLink
            to={""}
            style={{ textDecoration: "none", color: "inherit", width: "100%" }}
          >
            <ListItemButton
              selected={selectedIndex === 0}
              onClick={(event) => handleListItemClick(event, 0, true)}
            >
              <ListItemIcon>
                <DashboardIcon />
              </ListItemIcon>
              <ListItemText primary={"Tableau de bord"} />
            </ListItemButton>
          </NavLink>
        </ListItem>

        <ListItem disablePadding>
          <NavLink
            to={"carburant"}
            style={{ textDecoration: "none", color: "inherit", width: "100%" }}
          >
            <ListItemButton
              selected={selectedIndex === 1}
              onClick={(event) => handleListItemClick(event, 1 , true)}
            >
              <ListItemIcon>
                <LocalGasStationIcon />
              </ListItemIcon>
              <ListItemText primary={"Carburant"} />
            </ListItemButton>
          </NavLink>
        </ListItem>
{
    (
    
    <ListItem disablePadding>
          <NavLink
            to={"orders"}
            style={{ textDecoration: "none", color: "inherit", width: "100%" }}
          >
            <ListItemButton
              selected={selectedIndex === 8}
              onClick={(event) => handleListItemClick(event, 8 , true)}
            >
              <ListItemIcon>
                <GradingIcon/>
              </ListItemIcon>
              <ListItemText primary={"Ordres"} />
            </ListItemButton>
          </NavLink>
        </ListItem>
    )
}
      </List>

      <Divider />

      <List>
        <ListItemButton
          selected={selectedIndex === 2}
          onClick={(event) => handleClick(event, 2, "vehicules")}
        >
          <ListItemIcon>
            <DirectionsCarFilledIcon />
          </ListItemIcon>
          <ListItemText primary="Véhicules" />
          {vehicules ? <ExpandLess /> : <ExpandMore />}
        </ListItemButton>
        <Collapse in={vehicules} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
          <NavLink
                to={"vehicles"}
                style={{
                  textDecoration: "none",
                  color: "inherit",
                  width: "100%",
                }}
              >
            <ListItemButton
              sx={{ pl: 4 }}
              selected={selectedIndex === -1}
              onClick={(event) => handleListItemClick(event, -1 , true)}>
              <ListItemIcon>
                <CommuteIcon />
              </ListItemIcon>
              <ListItemText primary="Vehicules" />
            </ListItemButton>
            </NavLink>

            <NavLink
                to={"corbillard"}
                style={{
                  textDecoration: "none",
                  color: "inherit",
                  width: "100%",
                }}
              >
            <ListItemButton 
              sx={{ pl: 4 }}
              selected={selectedIndex === -2}
              onClick={(event) => handleListItemClick(event, -2 , true)}
            >
              <ListItemIcon>
                <AirportShuttleIcon />
              </ListItemIcon>
              <ListItemText primary="Corbillard" />
            </ListItemButton>
            </NavLink>
          </List>
        </Collapse>

        {role === "SpAdmin" ? (
          <>
            <ListItemButton
              selected={selectedIndex === 3}
              onClick={(event) => handleClick(event, 3, "users")}
            >
              <ListItemIcon>
                <GroupIcon />
              </ListItemIcon>
              <ListItemText primary="Utilisateurs" />
              {users ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
            <Collapse in={users} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                <NavLink
                  to={"users"}
                  style={{
                    textDecoration: "none",
                    color: "inherit",
                    width: "100%",
                  }}
                >
                  <ListItemButton
                    sx={{ pl: 4 }}
                    selected={selectedIndex === -3}
                    onClick={(event) => handleListItemClick(event, -3 , true)}
                  >
                    <ListItemIcon>
                      <GroupIcon />
                    </ListItemIcon>
                    <ListItemText primary="Utilisateurs" />
                  </ListItemButton>
                </NavLink>
                <NavLink
                to={"users-list"}
                style={{
                  textDecoration: "none",
                  color: "inherit",
                  width: "100%",
                }}
              >
                <ListItemButton sx={{ pl: 4 }}
                    selected={selectedIndex === -7}
                    onClick={(event) => handleListItemClick(event, -7 , true)}
                >
                  <ListItemIcon>
                    <ListAltIcon />
                  </ListItemIcon>
                  <ListItemText primary="Liste d'utilisateurs" />
                </ListItemButton>
                </NavLink>
              </List>
            </Collapse>

            <ListItemButton
              selected={selectedIndex === 4}
              onClick={(event) => handleClick(event, 4, "admins")}
              
            >
              <ListItemIcon sx={{margin:'-3px'}}   >
                <ManageAccountsIcon  />
              </ListItemIcon>
              <ListItemText  primary="Administrateurs" />
              {admins ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
            <Collapse in={admins} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                <NavLink
                  to={"admins"}
                  style={{
                    textDecoration: "none",
                    color: "inherit",
                    width: "100%",
                  }}
                >
                  <ListItemButton
                    sx={{ pl: 4 }}
                    selected={selectedIndex === -4}
                    onClick={(event) => handleListItemClick(event, -4 , true)}
                  >
                    <ListItemIcon>
                      <ManageAccountsIcon />
                    </ListItemIcon>
                    <ListItemText primary="Admins" />
                  </ListItemButton>
                </NavLink>
                <NavLink
                  to={"admins-list"}
                  style={{
                    textDecoration: "none",
                    color: "inherit",
                    width: "100%",
                  }}
                >
                <ListItemButton sx={{ pl: 4 }}
                selected={selectedIndex === -5}
                    onClick={(event) => handleListItemClick(event, -5 , true)}>
                  <ListItemIcon>
                    <ListAltIcon />
                  </ListItemIcon>
                  <ListItemText primary="Liste Admins" />
                </ListItemButton>
                </NavLink>
              </List>
            </Collapse>
          </>
        ) : (
          role === 'admin' ? 
          (<List>
            <ListItem disablePadding>
              <NavLink
                to={"users"}
                style={{
                  textDecoration: "none",
                  color: "inherit",
                  width: "100%",
                }}
              >
                <ListItemButton
                  selected={selectedIndex === 3}
                  onClick={(event) => handleListItemClick(event, 3, true)}
                >
                  <ListItemIcon>
                    <GroupIcon />
                  </ListItemIcon>
                  <ListItemText primary={"Utilisateurs"} />
                </ListItemButton>
              </NavLink>
            </ListItem>
          </List>) : ''
        )}


      </List>
    </div>
  );

  return (
    <div className={styles.parent}>
      {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
      <Drawer
        container={container}
        variant="temporary"
        open={mobileOpen}
        onClose={handleDrawerToggle}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}
        sx={{
          display: { xs: "block", lg: "none" },
          "& .MuiDrawer-paper": { boxSizing: "border-box", width: "240px" },
        }}
      >
        {drawer}
      </Drawer>

      <Drawer
        variant="persistent"
        sx={{
          display: { xs: "none", lg: "block" },
          "& .MuiDrawer-paper": { boxSizing: "border-box", width: "240px" },
          position: "sticky",
        }}
        open
      >
        {drawer}
      </Drawer>
    </div>
  );
};

export default AppSideBar;
