import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import { useDispatch, useSelector } from 'react-redux';
import { drawerToggle } from '../../store/reducer/SideBarSlice';
import { getAdmin } from '../../store/reducer/admin/AdminSlice';
import { Button, Dialog, DialogActions, DialogTitle, Divider, Slide } from '@mui/material';
import LogoutIcon from '@mui/icons-material/Logout';
import { Link, useNavigate } from 'react-router-dom';
import { adminLogout } from '../../store/reducer/admin/auth/AdminLogoutSlice';
import lang from '../../helpers/appLang';
import { LoadingButton } from '@mui/lab';
import { getSpAdmin } from '../../store/reducer/sp-admin/SpAdminSlice';
import { getUser } from '../../store/reducer/user/UserSclice';
import { spAdminLogout } from '../../store/reducer/sp-admin/auth/SpAdminLogoutSlice';
import { userLogout } from '../../store/reducer/user/auth/UserLogoutSlice';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function AppHeader({role}) {

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const [anchorElUser, setAnchorElUser] = React.useState(null);
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const [user, setUser] = React.useState({})

  const {loading} = useSelector(st => {
    if(role === 'admin'){
      return st.AdminLogout
    }
    if(role === 'SpAdmin'){
      return st.SpAdminLogout
    }
    if(role === 'user'){
      return st.UserLogout
    }
  })

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  React.useEffect(() => {
    if(role === 'admin') {
      dispatch(getAdmin()).unwrap()
    .then((res) => setUser(res))
    .catch((err) => console.log(err));
    }

    if(role === 'SpAdmin') {
      dispatch(getSpAdmin()).unwrap()
    .then((res) => setUser(res))
    .catch((err) => console.log(err));
    }

    if(role === 'user') {
      dispatch(getUser()).unwrap()
    .then((res) => {
      setUser(res)
      console.log(res);
    })
    .catch((err) => console.log(err));
    }

  }, [dispatch,role])


  const handleCloseUserMenu = (e) => { 
    setAnchorElUser(null)
  };

  const handleLogOut = () => {
      if (role === "admin") {
        dispatch(adminLogout(user.token))
          .unwrap()
          .then((res) => {
            navigate(lang + "/admin/auth", { replace: true });
            setAnchorElUser(null);
            handleClose();
          })
          .catch((err) => console.log(err));
      }
 
      if (role === "SpAdmin") {
        dispatch(spAdminLogout(user.token))
          .unwrap()
          .then((res) => {
            navigate(lang + "/sp-admin/auth", { replace: true });
            setAnchorElUser(null);
            handleClose();
          })
          .catch((err) => console.log(err));
      }
 
      if (role === "user") {
        dispatch(userLogout(user.token))
          .unwrap()
          .then((res) => {
            navigate(lang + "/auth", { replace: true });
            setAnchorElUser(null);
            
            handleClose();
          })
          .catch((err) => console.log(err));
      }
  };

  return (
    <AppBar position="fixed" sx={{ background: "#62B250" }}>
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Typography
            variant="h6"
            noWrap
            component="a"
            sx={{
              mr: 2,
              display: { xs: "none", lg: "flex" },
              fontWeight: 700,
              letterSpacing: ".3rem",
              color: "inherit",
              textDecoration: "none",
            }}
          >
            Ma Flotte
          </Typography>

          <Box sx={{ flexGrow: 1, display: { xs: "flex", lg: "none" } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={() => dispatch(drawerToggle())}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
          </Box>

          <Typography
            variant="h5"
            noWrap
            component="a"
            sx={{
              display: { xs: "flex", lg: "none" },
              flexGrow: 1,
              alignItems: "center",
              justifyContent: "center",
              fontWeight: 700,
              letterSpacing: ".3rem",
              textAlign: "center",
              color: "inherit",
              textDecoration: "none",
            }}
          >
            Ma Flotte
          </Typography>

          <Box sx={{ flexGrow: 1 }}>
            {/* 
                Links 
                    -> 
                    -> 
             */}
          </Box>

          <Box sx={{ flexGrow: 0 }}>
            <Tooltip title={user.nom + " " + user.prenom}>
              <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                <Avatar alt={user.nom + " " + user.prenom} src={user.image ?? '' } />
              </IconButton>
            </Tooltip>
            <Menu
              sx={{ mt: "45px" }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
              <Link
                to="profil"
                style={{ textDecoration: "none", color: "inherit" }}
              >
                <MenuItem onClick={handleCloseUserMenu} sx={{ gap: "8px" }}>
                  <Avatar
                    alt={user.nom + " " + user.prenom}
                    sx={{ width: 24, height: 24 }}
                  />
                  <Typography textAlign="center">Profil</Typography>
                </MenuItem>
                <Divider />
              </Link>

              <MenuItem onClick={handleClickOpen} sx={{ gap: "8px" }}>
                <LogoutIcon color="error" fontSize="small" />
                <Typography color="#d32f2f" textAlign="center">
                  Déconnecter
                </Typography>
              </MenuItem>
            </Menu>

            <Dialog
              open={open}
              TransitionComponent={Transition}
              keepMounted
              onClose={handleClose}
              aria-describedby="alert-dialog-slide-description"
            >
              <DialogTitle color={"#62B250"}  >
                {"Voulez-vous vous déconnecter?"}
              </DialogTitle>

              <DialogActions>
                <Button
                  disabled={loading}
                  onClick={handleClose}
                  color={"success"}
                >
                  Annuler
                </Button>
                <LoadingButton
                  loading={loading}
                  onClick={handleLogOut}
                  color={"error"}
                >
                  Déconnexion
                </LoadingButton>
              </DialogActions>
            </Dialog>
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
}
export default AppHeader;