

export const clientLang = window.clientInformation.language.split('-')[0]


export const appLang = ()=>{

    const lang = document.location.pathname.split('/')[1]

    if(lang === "ar"){
        return "ar"
    }
    if(lang === "fr"){
        return "fr"
    }
    if(lang === "en"){
        return "en"
    }

    return clientLang
}


const lang = appLang() === clientLang ? '' : '/'+appLang()

export default lang