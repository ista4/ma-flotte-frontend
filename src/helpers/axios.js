import axios from "axios";
import { appLang } from "./appLang";


// Api configuration
const baseURL = process.env.REACT_APP_API_URL
const apiKEY =  ""

// Api headers
const axiosInstance  =axios.create({
    baseURL:baseURL+appLang(),
    headers : {
        "Accept": "application/json",
        "X-API-Key": apiKEY ,
        "Access-Control-Allow-Origin": "*"
    }
})



export default axiosInstance