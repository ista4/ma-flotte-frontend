import React, { Suspense } from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import "./index.css";
import "leaflet/dist/leaflet.css";
import 'leaflet/dist/leaflet.js'



//redux
import { Provider } from "react-redux";
import store from "./store/index.js";
// localization
import lang from "./helpers/appLang";
// mui
import { LinearProgress } from "@mui/material";
//layouts
import AuthLayout from "./layout/AuthLayout";
import SuperAdminLayout from "./layout/sp-admin/SuperAdminLayout";
import UserLayout from "./layout/user/UserLayout";
import Index from "./layout/admin/Index";

// pages
import AdminLogin from "./pages/admin/auth/AdminLogin";
import SuperAdmin from "./pages/sp-admin/dashboard/SuperAdmin";
import SpFuel from "./pages/sp-admin/fuel/SpFuel";
import SpAdminProfile from "./pages/sp-admin/profile/SpAdminProfile";
import SpUsers from "./pages/sp-admin/users/SpUsers";
import SpAdmins from "./pages/sp-admin/admins/SpAdmins";
import SpAdminLogin from "./pages/sp-admin/auth/SpAdminLogin";
import AdminUsers from "./pages/admin/users/AdminUsers";
import SpUsersList from "./pages/sp-admin/users/SpUsersList";
import SpVehicles from "./pages/sp-admin/vehicles/SpVehicles";
import SpCorbillard from "./pages/sp-admin/vehicles/SpCorbillard";
import AdminVehicles from "./pages/admin/vehicles/AdminVehicles";
import AdminCorbillard from "./pages/admin/vehicles/AdminCorbillard";
import SpAdminsList from "./pages/sp-admin/admins/SpAdminsList";


import UserFuel from "./pages/user/fuel/UserFuel";
import UserProfile from "./pages/user/profile/UserProfile";
import UserVehicles from "./pages/user/vehicles/UserVehicles";
import UserCorbillard from "./pages/user/vehicles/UserCorbillard";
import UserLogin from "./pages/user/auth/UserLogin";
import UserCompletRegister from "./pages/user/auth/UserCompletRegister";
import UserOtpVerification from "./pages/user/auth/UserOtpVerification";
import UserForgetPassword from "./pages/user/auth/UserForgetPassword";
import UserResetPassword from "./pages/user/auth/UserResetPassword";
import { SnackbarProvider } from "notistack";
import ResponsiveModal from "./components/ResponsiveModal/ResponsiveModal";
import FullModal from "./components/FullModal/FullModal";
import SpOrders from "./pages/sp-admin/orders/SpOrders";
import UserOrders from "./pages/user/orders/UserOrders";
import User from "./pages/user/dashboard/User";
import Orders from "./pages/admin/orders/Orders";


const Admin = React.lazy(() => import("./pages/admin/dashboard/Admin"));
const AdminProfile = React.lazy(() => import("./pages/admin/profile/AdminProfile"));
const Fuel = React.lazy(() => import("./pages/admin/fuel/Fuel"));
const ForgetPassword = React.lazy(() => import("./pages/admin/auth/ForgetPassword"));
const AdminCompletRegister = React.lazy(() => import("./pages/admin/auth/AdminCompletRegister"));
const OtpVerification = React.lazy(() => import("./pages/admin/auth/OtpVerification"));
const ResetPassword = React.lazy(() => import("./pages/admin/auth/ResetPassword"));

const router = createBrowserRouter([
  
  // ******* User Routes  ********************************
  {
    path: lang + "/",
    element: (
      <SnackbarProvider maxSnack={3}>
        <UserLayout />
      </SnackbarProvider>
    ),

    children: [
      {
        index: true,
        path: "",
        element: (
            <User />
        ),
      },
      {
        
        path: "carburant",
        element: (
            <UserFuel />
        ),
      },
      {
        
        path: "orders",
        element: (
            <UserOrders />
        ),
      },
      {
        
        path: "profil",
        element: (
            <UserProfile />
        ),
      },
      {
        
        path: "vehicles",
        element: (
            <UserVehicles/>
        ),
      },
      {
        
        path: "corbillard",
        element: (
            <UserCorbillard/>
        ),
      },
    ],
  },
  //   User Auth
  {
    path: lang + "/auth",
    element: (
        <AuthLayout />
    ),
    children: [
      {
        index: true,
        path: "",
        element: (
            <UserLogin />
        ),
      },
      {
        path: "login",
        element: (
            <UserLogin />
        ),
      },
      {
        path: "complet-register",
        element: (
            <UserCompletRegister />
        ),
      },
      {
        path: "verification",
        element: (
            <UserOtpVerification />
        ),
      },
      {
        path: "forget-password",
        element: (
            <UserForgetPassword />
        ),
      },

      {
        path: "reset-password",
        element: (
            <UserResetPassword />
        ),
      },
    ],
  },
  // ******* End User Routes  ********************************



  // ******* Admin routes ********************************
  {
    path: lang + "/admin",
    element: (
      <SnackbarProvider maxSnack={3}>
        <Index />
        </SnackbarProvider>
    ),

    children: [
      {
        index: true,
        path: "",
        element: (
          <Suspense   fallback={<LinearProgress color="success" sx={{ height: "4px" }} />}>

              <Admin />
          </Suspense>
        ),
      },
      {
        
        path: "carburant",
        element: (
          <Suspense  fallback={<LinearProgress color="success" sx={{ height: "4px" }} />}>
            <Fuel />
          </Suspense>
        ),
      },
      {
        
        path: "orders",
        element: (
      
            <Orders />
       
        ),
      },
      {
        
        path: "users",
        element: (
          <Suspense  fallback={<LinearProgress color="success" sx={{ height: "4px" }} />}>
            <AdminUsers />
          </Suspense>
        ),
      },
      {
        
        path: "profil",
        element: (
          <Suspense  fallback={<LinearProgress color="success" sx={{ height: "4px" }} />}>
            <AdminProfile />
          </Suspense>
        ),
      },
      {
        
        path: "vehicles",
        element: (
            <AdminVehicles/>
        ),
      },
      {
        
        path: "corbillard",
        element: (
            <AdminCorbillard/>
        ),
      },
    ],
  },
  //Admin Auth Routes
  {
    path: lang + "/admin/auth",
    element: (
        <AuthLayout />
    ),
    children: [
      {
        index: true,
        path: "",
        element: (
          <Suspense   fallback={<LinearProgress color="success" sx={{ height: "4px" }} />}>
            <AdminLogin />
          </Suspense>
        ),
      },
      {
        path: "login",
        element: (
          <Suspense   fallback={<LinearProgress color="success" sx={{ height: "4px" }} />}>
            <AdminLogin />
          </Suspense>
        ),
      },
      {
        path: "complet-register",
        element: (
          <Suspense
            fallback={<LinearProgress color="success" sx={{ height: "4px" }} />}
          >
            <AdminCompletRegister />
          </Suspense>
        ),
      },
      {
        path: "verification",
        element: (
          <Suspense
            fallback={<LinearProgress color="success" sx={{ height: "4px" }} />}
          >
            <OtpVerification />
          </Suspense>
        ),
      },
      {
        path: "forget-password",
        element: (
          <Suspense
            fallback={<LinearProgress color="success" sx={{ height: "4px" }} />}
          >
            <ForgetPassword />
          </Suspense>
        ),
      },

      {
        path: "reset-password",
        element: (
          <Suspense
            fallback={<LinearProgress color="success" sx={{ height: "4px" }} />}
          >
            <ResetPassword />
          </Suspense>
        ),
      },
    ],
  },
  // *******End Admin routes ********************************



  // ******* Super Admin routes ********************************
  {
    path: lang + "/sp-admin",
    element: (
      <SnackbarProvider maxSnack={3}>
        <SuperAdminLayout/>
        </SnackbarProvider>
    ),
    children: [
      {
        index: true,
        path: "",
        element: (
            <SuperAdmin />
        ),
      },
      {
        
        path: "carburant",
        element: (<SpFuel/>),
      },
      {
        
        path: "profil",
        element: (
            <SpAdminProfile/>
        ),
      },
      {
        
        path: "users",
        element: (
            <SpUsers/>
        ),
      },
      {
        
        path: "users-list",
        element: (
            <SpUsersList/>
        ),
      },
      {
        
        path: "orders",
        element: (
            <SpOrders/>
        ),
      },
      {
        
        path: "admins",
        element: (
            <SpAdmins/>
        ),
      },
      {
        
        path: "admins-list",
        element: (
            <SpAdminsList/>
        ),
      },
      {
        
        path: "vehicles",
        element: (
            <SpVehicles/>
        ),
      },
      {
        
        path: "corbillard",
        element: (
            <SpCorbillard/>
        ),
      },
    ],
  },
    // Super Admin Auth Routes
  {
    path: lang + "/sp-admin/auth",
    element: (
      <Suspense   fallback={<LinearProgress color="success" sx={{ height: "4px" }} />}>
        <AuthLayout />
      </Suspense>
    ),
    children: [
      {
        index: true,
        path: "",
        element: (
          <Suspense   fallback={<LinearProgress color="success" sx={{ height: "4px" }} />}>
            <SpAdminLogin />
          </Suspense>
        ),
      },
      {
        path: "login",
        element: (
          <Suspense   fallback={<LinearProgress color="success" sx={{ height: "4px" }} />}>
            <SpAdminLogin />
          </Suspense>
        ),
      }
    ],
  },
  // *******End Admin routes ********************************

]);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <ResponsiveModal/>
    <FullModal/>
    <RouterProvider router={router} />
  </Provider>
);
