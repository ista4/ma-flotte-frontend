import { configureStore } from "@reduxjs/toolkit";
// Admin
import AdminSlice from "./reducer/admin/AdminSlice";
import AdminCompletRegisterSlice from "./reducer/admin/auth/AdminCompletRegisterSlice";
import AdminForgetPasswordSlice from "./reducer/admin/auth/AdminForgetPasswordSlice";
import AdminLoginSlice from "./reducer/admin/auth/AdminLoginSlice";
import AdminLogoutSlice from "./reducer/admin/auth/AdminLogoutSlice";
import AdminOtpVerificationSlice from "./reducer/admin/auth/AdminOtpVerificationSlice";
import AdminResetPasswordSlice from "./reducer/admin/auth/AdminResetPasswordSlice";
// UI
import FullModalSlice from "./reducer/FullModalSlice";
import MenuListSlice from "./reducer/MenuListSlice";
import ResponsiveModalSlice from "./reducer/ResponsiveModalSlice";
import SideBarSlice from "./reducer/SideBarSlice";
// Super Admin - Admins
import AddAdminSlice from "./reducer/sp-admin/admins/AddAdminSlice";
import DeleteAdminsSlice from "./reducer/sp-admin/admins/DeleteAdminsSlice";
import GetAdminsSlice from "./reducer/sp-admin/admins/GetAdminsSlice";
import PrintAdminsSlice from "./reducer/sp-admin/admins/PrintAdminsSlice";
import UpdateAdminSlice from "./reducer/sp-admin/admins/UpdateAdminSlice";
// Super Admin - Auth
import SpAdminLoginSlice from "./reducer/sp-admin/auth/SpAdminLoginSlice";
import SpAdminLogoutSlice from "./reducer/sp-admin/auth/SpAdminLogoutSlice";
import SpAdminSlice from "./reducer/sp-admin/SpAdminSlice";
// Super Admin - Users
import AddUserSlice from "./reducer/sp-admin/users/AddUserSlice";
import DeleteUsersSlice from "./reducer/sp-admin/users/DeleteUsersSlice";
import GetUsersSlice from "./reducer/sp-admin/users/GetUsersSlice";
import PrintUsersSlice from "./reducer/sp-admin/users/PrintUsersSlice";
import UpdateUserSlice from "./reducer/sp-admin/users/UpdateUserSlice";
// User
import UserCompletRegisterSlice from "./reducer/user/auth/UserCompletRegisterSlice";
import UserForgetPasswordSlice from "./reducer/user/auth/UserForgetPasswordSlice";
import UserLoginSlice from "./reducer/user/auth/UserLoginSlice";
import UserLogoutSlice from "./reducer/user/auth/UserLogoutSlice";
import UserOtpVerificationSlice from "./reducer/user/auth/UserOtpVerificationSlice";
import UserResetPasswordSlice from "./reducer/user/auth/UserResetPasswordSlice";
import UserSclice from "./reducer/user/UserSclice";
// Vehicle
import AddVehicleSlice from "./reducer/vehicle/AddVehicleSlice";
import VehicleSlice from "./reducer/vehicle/VehicleSlice";
import VehicleTypeSlice from "./reducer/vehicle/VehicleTypeSlice";
import UpdateVehicleSlice from "./reducer/vehicle/UpdateVehicleSlice";
// Fuel
import FuelSlice from "./reducer/Fuel/FuelSlice";
import AddFuelSlice from "./reducer/Fuel/AddFuelSlice";
import FuelStatistiquesSlice from "./reducer/Fuel/FuelStatistiquesSlice";
import PrintVehiculeQrCodeSlice from "./reducer/vehicle/PrintVehiculeQrCodeSlice";
// Mission
import AddMissionSlice from "./reducer/mission/AddMissionSlice";
import MissionSlice from "./reducer/mission/MissionSlice";
import UpdateOrderLocationSlice from "./reducer/mission/UpdateOrderLocationSlice";
import DeleteMissionSlice from "./reducer/mission/DeleteMissionSlice";
import PrintMissionSlice from "./reducer/mission/PrintMissionSlice";
import CompleteMissionSlice from "./reducer/mission/CompleteMissionSlice";

// Dashboard
import DashBoardSlice from "./reducer/DashBoardSlice";

const store = configureStore({
    
  reducer: {
    // UI
    sidebar: SideBarSlice,
    respoiveModal: ResponsiveModalSlice,
    fullModal: FullModalSlice,
    menuList: MenuListSlice,
    // Dashboard
    DashBoard: DashBoardSlice,
    // Mission
    AddMission: AddMissionSlice,
    Mission: MissionSlice,
    UpdateOrderLocation: UpdateOrderLocationSlice,
    DeleteMission: DeleteMissionSlice,
    PrintMission: PrintMissionSlice,
    CompleteMission: CompleteMissionSlice,
    // Vehicle
    Vehicle: VehicleSlice,
    AddVehicle: AddVehicleSlice,
    VehicleType: VehicleTypeSlice,
    UpdateVehicle: UpdateVehicleSlice,
    PrintVehiculeQrCode: PrintVehiculeQrCodeSlice,
    // Carburant
    Fuel: FuelSlice,
    AddFuel: AddFuelSlice,
    FuelStatistiques: FuelStatistiquesSlice,
    // User
    user: UserSclice,
    UserLogin: UserLoginSlice,
    UserLogout: UserLogoutSlice,
    UserCompletRegister: UserCompletRegisterSlice,
    userOtp: UserOtpVerificationSlice,
    UserForgetPassword: UserForgetPasswordSlice,
    UserResetPassword: UserResetPasswordSlice,
    AddUser: AddUserSlice,
    GetUsers: GetUsersSlice,
    UpdateUser: UpdateUserSlice,
    DeleteUsers: DeleteUsersSlice,
    PrintUsers: PrintUsersSlice,
    // Admin
    admin: AdminSlice,
    AdminLogin: AdminLoginSlice,
    AdminLogout: AdminLogoutSlice,
    AdminCompletRegister: AdminCompletRegisterSlice,
    adminOtp: AdminOtpVerificationSlice,
    AdminForgetPassword: AdminForgetPasswordSlice,
    AdminResetPassword: AdminResetPasswordSlice,
    AddAdmin: AddAdminSlice,
    GetAdmins: GetAdminsSlice,
    UpdateAdmin: UpdateAdminSlice,
    DeleteAdmins: DeleteAdminsSlice,
    PrintAdmins: PrintAdminsSlice,
    // Super Admin
    SpAdmin: SpAdminSlice,
    SpAdminLogin: SpAdminLoginSlice,
    SpAdminLogout: SpAdminLogoutSlice,
  },
});

export default store;
