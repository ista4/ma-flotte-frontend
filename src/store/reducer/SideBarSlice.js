import {createSlice } from "@reduxjs/toolkit"


const initialState = {
    mobileOpen: false,
}

const SidebarSlice = createSlice({
    name: 'sidebar',
    initialState ,
    reducers : {
        drawerToggle(state,payload){
            state.mobileOpen = !state.mobileOpen;;
        },
    }

})

export const {drawerToggle} =  SidebarSlice.actions

export default SidebarSlice.reducer