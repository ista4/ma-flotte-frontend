
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"
import { getUser } from "../UserSclice"

export const userOtp = createAsyncThunk('userOtp' , async (otp , thunkApi) => {
    
    const {rejectWithValue, dispatch } = thunkApi
    
    
    const admin = await dispatch(getUser())

    try {
        const request  = await axiosInstance.put(`/otpVerification`, {
            identifiant : admin.payload.cin,
            otp : otp
        },{
            headers : {
                Authorization : 'Bearer ' + localStorage.user_token
            }
        })
        
        const res = await request.data

        return res
    } catch (error) {
        return rejectWithValue(error)
    }
})


const initialState = {
    loading : false,
    success: false,
    rejected : false,
}

const UserOtpVerificationSlice = createSlice({
    name: 'userOTP',
    initialState ,
    extraReducers : (builder) => {
        builder
        // otp verification
        .addCase( userOtp.pending , (state , action) => {
            state.loading = true;
            state.success = false;
            
        })
        .addCase( userOtp.fulfilled , (state , action) => {
            state.loading = false;
           
            if(action.payload.success){
                state.success = true;
                state.rejected = false;  
            }
            

            console.log(action.payload);
        })
        .addCase( userOtp.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;
            console.log(action.payload);
        })

    }

})


export default UserOtpVerificationSlice.reducer