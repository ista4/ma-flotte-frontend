
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"





export const userLogin = createAsyncThunk('userLogin' , async (data , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
    
    try {
        const request  = await axiosInstance.post(`/login`, {
            identifiant : data.identifiant,
            password : data.password
        },{
            headers : {
                Authorization : 'Bearer ' + localStorage.user_token
            }
        })
        
        const res = await request.data

        return res
    } catch (error) {
        return rejectWithValue(error.message)
    }
})


const initialState = {
    token: '',
    loading : false,
    success: false,
    rejected : false,
}

const UserLoginSlice = createSlice({
    name: 'userLogin',
    initialState ,
    extraReducers : (builder) => {
        builder
        // login
        .addCase( userLogin.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( userLogin.fulfilled , (state , action) => {
            state.loading = false;
           
            if(action.payload.success){
                state.success = true;
                state.token = action.payload.token;
                state.rejected = false;
                localStorage.setItem('user_token', state.token);
            }
            

            console.log(action.payload);
        })
        .addCase( userLogin.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;
        })

    }

})


export default UserLoginSlice.reducer