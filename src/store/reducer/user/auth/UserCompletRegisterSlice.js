import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"


export const userCompletRegister = createAsyncThunk('userCompletRegister' , async (data , thunkApi) => {
    
    const {rejectWithValue} = thunkApi

    
    try {
        const request  = await axiosInstance.post(`/completRegister`, {
            email : data.email,
            tel : data.tel,
            sexe : data.sexe
        },{
            headers : {
                Authorization : 'Bearer ' + localStorage.user_token
                
            }
        })
        
        const res = await request.data

        return res
    } catch (error) {
        return rejectWithValue(error.response.data.message)
    }
})


const initialState = {
    loading : false,
    success: false,
    rejected : false,
}

const UserCompletRegisterSlice = createSlice({
    name: 'userCompletRegister',
    initialState ,
    extraReducers : (builder) => {
        builder
        // login
        .addCase( userCompletRegister.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( userCompletRegister.fulfilled , (state , action) => {
            state.loading = false;
            if(action.payload.success){
                state.success = true;
                state.rejected = false;
            }
            
        })
        .addCase( userCompletRegister.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;
         
        })

    }

})


export default UserCompletRegisterSlice.reducer