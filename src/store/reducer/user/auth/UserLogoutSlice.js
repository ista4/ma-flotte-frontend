
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"





export const userLogout = createAsyncThunk('userLogout' , async (data , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
    
    try {
        const request  = await axiosInstance.delete(`/logout`,{
            headers : {
                Authorization : 'Bearer ' + localStorage.user_token
            }
        })
        
        const res = await request.data

        return res
    } catch (error) {
        return rejectWithValue(error)
    }
})


const initialState = {
    loading : false,
    success: false,
    rejected : false,
}

const UserLogoutSlice = createSlice({
    name: 'userLogout',
    initialState ,
    extraReducers : (builder) => {
        builder
        // login
        .addCase( userLogout.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( userLogout.fulfilled , (state , action) => {
            state.loading = false;
           
            if(action.payload.success){
                state.success = true;   
                localStorage.removeItem('user_token');
                state.rejected = false;
                state.loading = false;
            }
        })
        .addCase( userLogout.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;

        })

    }

})


export default UserLogoutSlice.reducer