import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../helpers/axios"





export const getUser = createAsyncThunk('getUser' , async (_ , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
    
    try {
        const request  = await axiosInstance.get(`/user`,{
            headers : {
                Authorization : 'Bearer ' + localStorage.user_token
            }
        })
        
        const res = await request.data

        return res
    } catch (error) {
        return rejectWithValue(error.message)
    }
})


const initialState = {
    user: {},
    loading : false,
    success: false,
    rejected : false,
}

const UserSlice = createSlice({
    name: 'user',
    initialState ,
    extraReducers : (builder) => {
        builder
        // login
        .addCase( getUser.pending , (state , action) => {
            state.loading = true;
            state.success = false;
            
        })
        .addCase( getUser.fulfilled , (state , action) => {
            state.loading = false;
            state.user = action.payload;
        })
        .addCase( getUser.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;

        })

    }

})


export default UserSlice.reducer