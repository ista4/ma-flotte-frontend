import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../helpers/axios"

export const getDashboardData = createAsyncThunk('dashboard/getDashboardData' , async (_ , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
  
    try {
        const request  = await axiosInstance.get(`/dashboard`,{
            headers : {
                Authorization : 'Bearer ' + (
                    localStorage.sp_admin_token ||
                    localStorage.token)
            }
        })
        
        const res = await request.data
        return res
    } catch (error) {
        return rejectWithValue(error)
    }
})


const initialState = {
    admin_number        : 0 ,
    user_number         : 0 ,
    vehucule_number     : 0 ,
    carburant_number    : 0 ,
    last_fuels          : [] ,
    missions_encours    : [] ,
    missions_terminees  : [] ,

    loading : false,
    success: false,
    rejected : false,
}

const DashBoardSlice = createSlice({
    name: 'dashboard',
    initialState ,
    reducers : {
        updateVehicleFromFuelState : (state , action) => {
             state.fuels.map((fuel) => {

                if(fuel.vehicule.id === action.payload.id){
                    fuel.vehicule = action.payload
                }
                return ''
            })
        }   ,
        addFuelToState : (state , action) => {
            state.fuels.push(action.payload)
        } ,
    },
    extraReducers : (builder) => {
        builder
        // Get dashboard data
        .addCase( getDashboardData.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( getDashboardData.fulfilled , (state , action) => {
            state.loading = false;
            if(action.payload){
                state.success = true;

                state.admin_number = action.payload.admin_number
                state.user_number = action.payload.user_number
                state.vehucule_number = action.payload.vehucule_number
                state.carburant_number = action.payload.carburant_number
                state.last_fuels = action.payload.last_fuels
                state.missions_encours = action.payload.missions_encours
                state.missions_terminees = action.payload.missions_terminees
                
            }
        })
        .addCase( getDashboardData.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;

        })

    }

})

export default DashBoardSlice.reducer