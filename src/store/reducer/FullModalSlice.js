import {createSlice } from "@reduxjs/toolkit"


const initialState = {
    open: false,
    vehicule : {},
    componentName: '',
    title : '',
    childrenProps: {},
    role: "",

}

const FullModalSlice = createSlice({
    name: 'fullModal',
    initialState ,
    reducers : {
        openFullModal : (state, action) => {
            state.open = true;
            state.vehicule = action.payload.vehicule;
            state.componentName = action.payload.componentName;
            state.title = action.payload.title;
            state.childrenProps = action.payload.childrenProps;
            state.role = action.payload.role;
        },
        closeFullModal : (state, action) => {
            state.open = false;
            state.vehicule = {};
            state.componentName = '';
            state.title = '';
            state.childrenProps = {};
            state.role = "";
        },
        updateVehiculeFullMoadal : (state, action) => {
            state.vehicule = action.payload.vehicule;
        },
    }

})

export const {openFullModal, closeFullModal, updateVehiculeFullMoadal} =  FullModalSlice.actions

export default FullModalSlice.reducer