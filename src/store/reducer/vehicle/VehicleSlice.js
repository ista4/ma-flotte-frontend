import { createAsyncThunk, createSlice, current } from "@reduxjs/toolkit";
import axiosInstance from "../../../helpers/axios";

export const getVehicules = createAsyncThunk(
  "getVehicules",
  async (_, thunkApi) => {
    const { rejectWithValue } = thunkApi;
    try {
      const request = await axiosInstance.get(`/vehicules`, {
        headers: {
          Authorization:
            "Bearer " + (localStorage.user_token ||
              localStorage.sp_admin_token ||
              localStorage.token),
        },
      });

      const res = await request.data;

      return res;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

// get vehicule by id or matricule
export const getVehiculeById = createAsyncThunk(
  "getVehiculeById",
  async (id, thunkApi) => {
    const { rejectWithValue } = thunkApi;
    try {
      const request = await axiosInstance.get(`/vehicules/${id}`, {
        headers: {
          Authorization: "Bearer " + (localStorage.token || localStorage.user_token || localStorage.sp_admin_token), 
        },
      });

      const res = await request.data;

      return res;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);


const initialState = {
  vehicules: [],
  fitredVehicule: [],
  vehicule: {},
  loading: false,
  success: false,
  rejected: false,
};

const VehicleSlice = createSlice({
  name: "vehicules",
  initialState,
  reducers: {
    addVehicleToState: (state, action) => {
      state.vehicules.push(action.payload);
    },
    deleteVehicleFromState: (state, action) => {
      state.vehicules = state.vehicules.filter(
        (vehicule) => vehicule.id !== action.payload
      )
    },
    updateVehicleFromState: (state, action) => {
      const index = state.vehicules.findIndex(
        (vehicule) => vehicule.id === action.payload.id
      );
      if (index !== -1) {
        state.vehicules[index] = action.payload;
      }
    },
    searchVehicleFromState: (state, action) => {
      const fitredVehiculeType = state.vehicules.filter((vehicule) =>
       
        vehicule.type.id === action.payload.type ||
        (
          vehicule.nom?.toLowerCase().includes(action.payload.nom?.toLowerCase()) || 
          vehicule.immatriculation?.toLowerCase().includes(action.payload.matricule?.toLowerCase())
        ) 

      );
      state.fitredVehicule = fitredVehiculeType;
    },},
  extraReducers: (builder) => {
    builder
      // get
      .addCase(getVehicules.pending, (state, action) => {
        state.loading = true;
        state.success = false;
      })
      .addCase(getVehicules.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload) {
          state.success = true;
          state.vehicules = action.payload;
        }
      })
      .addCase(getVehicules.rejected, (state, action) => {
        state.loading = false;
        state.success = false;
        state.rejected = true;
      })
      // get vehicule by id or matricule
      .addCase(getVehiculeById.pending, (state, action) => {
        state.loading = true;
        state.success = false;
      })
      .addCase(getVehiculeById.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload.success) {
          state.success = true;
          state.vehicule = action.payload.vehicule;
        }
      })
      .addCase(getVehiculeById.rejected, (state, action) => {
        state.loading = false;
        state.success = false;
        state.rejected = true;
      }
      );

  },
});

export const {
  addVehicleToState,
  searchVehicleFromState,
  deleteVehicleFromState,
  updateVehicleFromState,
} = VehicleSlice.actions;

export default VehicleSlice.reducer;
