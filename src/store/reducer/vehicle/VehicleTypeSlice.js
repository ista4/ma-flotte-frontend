import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosInstance from "../../../helpers/axios";

export const addVehiculesType = createAsyncThunk(
  "vehiculesType/addVehiculesType",
  async (type, thunkApi) => {
    const { rejectWithValue } = thunkApi;

    try {
      const request = await axiosInstance.post(
        `/vehiculesTypes`,
        {
          type: type,
        },
        {
          headers: {
            Authorization: "Bearer " + localStorage.sp_admin_token,
          },
        }
      );

      const res = await request.data;

      return res;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const getVehiculesType = createAsyncThunk(
  "vehiculesType/getVehiculesType",
  async (_, thunkApi) => {
    const { rejectWithValue } = thunkApi;
    try {
      const request = await axiosInstance.get(`/vehiculesTypes`, {
        headers: {
          Authorization:
            "Bearer " + (localStorage.sp_admin_token ||
              localStorage.token ||
              localStorage.user_token),
        },
      });

      const res = await request.data;

      return res;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const deleteVehiculesType = createAsyncThunk(
  "vehiculesType/deleteVehiculesType",
  async (id, thunkApi) => {
    const { rejectWithValue } = thunkApi;

    try {
      const request = await axiosInstance.delete(`/vehiculesTypes/` + id, {
        headers: {
          Authorization: "Bearer " + localStorage.sp_admin_token,
        },
      });

      const res = await request.data;

      return res;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const updateVehiculesType = createAsyncThunk(
  "vehiculesType/updateVehiculesType",
  async (data, thunkApi) => {
    const { rejectWithValue } = thunkApi;

    try {
      const request = await axiosInstance.put(
        `/vehiculesTypes/` + data.id,
        {
          type: data.type,
        },
        {
          headers: {
            Authorization: "Bearer " + localStorage.sp_admin_token,
          },
        }
      );

      const res = await request.data;

      return res;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

const initialState = {
  types: [],
  loading: false,
  success: false,
  rejected: false,
};

const VehicleTypeSlice = createSlice({
  name: "vehiculesType",
  initialState,
  reducers: {
    addVehicleTypeToState: (state, action) => {
      state.types.push(action.payload);
    },
    deleteVehiculesTypeFromState: (state, action) => {
      state.types = state.types.filter((type) => type.id !== action.payload);
    },
    updateVehiculesTypeFromState: (state, action) => {

      state.types = state.types.map((type) =>
        type.id === action.payload.id ? action.payload : type
      );
    },
  },
  extraReducers: (builder) => {
    builder
      // add
      .addCase(addVehiculesType.pending, (state, action) => {
        state.loading = true;
        state.success = false;
      })
      .addCase(addVehiculesType.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload.success) {
          state.success = true;
        }
      })
      .addCase(addVehiculesType.rejected, (state, action) => {
        state.loading = false;
        state.success = false;
        state.rejected = true;
      })

      // update
      .addCase(updateVehiculesType.pending, (state, action) => {
        state.loading = true;
        state.success = false;
      })
      .addCase(updateVehiculesType.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload.success) {
          state.success = true;
          console.log(action.payload);
        }
      })
      .addCase(updateVehiculesType.rejected, (state, action) => {
        state.loading = false;
        state.success = false;
        state.rejected = true;
      })

      // delete
      .addCase(deleteVehiculesType.pending, (state, action) => {
        state.loading = true;
        state.success = false;
      })
      .addCase(deleteVehiculesType.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload.success) {
          state.success = true;
        }
      })
      .addCase(deleteVehiculesType.rejected, (state, action) => {
        state.loading = false;
        state.success = false;
        state.rejected = true;
      })

      // get
      .addCase(getVehiculesType.pending, (state, action) => {
        state.loading = true;
        state.success = false;
      })
      .addCase(getVehiculesType.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload) {
          state.success = true;
          state.types = action.payload;

        }
      })
      .addCase(getVehiculesType.rejected, (state, action) => {
        state.loading = false;
        state.success = false;
        state.rejected = true;
      });
  },
});

export const {
  addVehicleTypeToState,
  deleteVehiculesTypeFromState,
  updateVehiculesTypeFromState,
} = VehicleTypeSlice.actions;

export default VehicleTypeSlice.reducer;
