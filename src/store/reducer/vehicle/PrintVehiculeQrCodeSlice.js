import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../helpers/axios"



export const printVehiculeQrCode = createAsyncThunk('printVehiculeQrCode' , async (ids , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
    

    try {
        const request  = await axiosInstance.post(`/vehicules/qrcodes` ,
            {
             ids,
            }
        , {
            headers : {
                Authorization : 'Bearer ' + (localStorage.sp_admin_token || localStorage.token),
               
            }
        })
        
        const res = await request.data
        return res
    } catch (error) {
        return rejectWithValue(error)
    }
})


const initialState = {
    loading : false,
    pdf: "",
    success: false,
    rejected : false,
}

const PrintVehiculeQrCodeSlice = createSlice({
    name: 'vehicules',
    initialState ,
    
    extraReducers : (builder) => {
        builder
        //
        .addCase( printVehiculeQrCode.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( printVehiculeQrCode.fulfilled , (state , action) => {
            state.loading = false;
            if(action.payload.success){
                state.success = true;
                state.pdf = action.payload.pdf
         

            }
            console.log(action.payload);
        })
        .addCase( printVehiculeQrCode.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;
 
        })

    }

})


export default PrintVehiculeQrCodeSlice.reducer