import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosInstance from "../../../helpers/axios";

export const updateVehicule = createAsyncThunk(
  "vehicules/updateVehicule",
  async (data, thunkApi) => {

    const { rejectWithValue } = thunkApi;

    try {
      const request = await axiosInstance.post(
        `/vehicules/`+data.id,
        {
            ...data.data,
            "_method": "PUT"
        },
        {
          headers: {
            Authorization: "Bearer " + localStorage.sp_admin_token,
            "Content-Type" : "multipart/form-data",
            
          },
        }
      );

      const res = await request.data;

      return res;
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

const initialState = {
  vehicule: {},
  loading: false,
  success: false,
  rejected: false,
};

const UpdateVehicleSlice = createSlice({
  name: "vehicules",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      // update vehicule 
      .addCase(updateVehicule.pending, (state, action) => {
        state.loading = true;
        state.success = false;
      })
      .addCase(updateVehicule.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload.success) {
          state.success = true;
          state.vehicule = action.payload.vehicule;
        }
      })
      .addCase(updateVehicule.rejected, (state, action) => {
        state.loading = false;
        state.success = false;
        state.rejected = true;
        console.log(action.payload);
      });
  },
});

export default UpdateVehicleSlice.reducer;
