import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosInstance from "../../../helpers/axios";

export const addVehicules = createAsyncThunk(
  "vehicules/addVehicules",
  async (data, thunkApi) => {

    const { rejectWithValue } = thunkApi;

    try {
      const request = await axiosInstance.post(
        `/vehicules`,
        {
          immatriculation: data.immatriculation,
          nom: data.nom,
          pussance_fiscale: data.pussanceFiscale,
          type_vehicule_id: data.typeVehiculeId,
          type_carburant: data.typeCarburant,
          date_acquisition: data.dateAcquisition,
          date_debut_assurance: data.dateDebutAssurance,
          cout_assurance: data.coutAssurance,
          image: data.image,
        },
        {
          headers: {
            Authorization: "Bearer " + localStorage.sp_admin_token,
            "Content-Type" : "multipart/form-data"
          },
        }
      );

      const res = await request.data;

      return res;
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

const initialState = {
  vehicules: [],
  loading: false,
  success: false,
  rejected: false,
};

const AddVehicleSlice = createSlice({
  name: "vehicules",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      // get
      .addCase(addVehicules.pending, (state, action) => {
        state.loading = true;
        state.success = false;
      })
      .addCase(addVehicules.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload.success) {
          state.success = true;
          state.vehicules = action.payload;
          console.log(action.payload);
        }
      })
      .addCase(addVehicules.rejected, (state, action) => {
        state.loading = false;
        state.success = false;
        state.rejected = true;
      });
  },
});

export default AddVehicleSlice.reducer;
