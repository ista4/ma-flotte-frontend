import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../helpers/axios"


export const getFuel = createAsyncThunk('fuel/getFuel' , async (_ , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
  
    try {
        const request  = await axiosInstance.get(`/carburants`,{
            headers : {
                Authorization : 'Bearer ' + (localStorage.user_token ||
                    localStorage.sp_admin_token ||
                    localStorage.token)
            }
        })
        
        const res = await request.data
        return res
    } catch (error) {
        return rejectWithValue(error)
    }
})


const initialState = {
    loading : false,
    fuels : [],
    success: false,
    rejected : false,
}

const FuelSlice = createSlice({
    name: 'fuel',
    initialState ,
    reducers : {
        updateVehicleFromFuelState : (state , action) => {
             state.fuels.map((fuel) => {

                if(fuel.vehicule.id === action.payload.id){
                    fuel.vehicule = action.payload
                }
                return ''
            })
        }   ,
        addFuelToState : (state , action) => {
            state.fuels.push(action.payload)
        } ,
    },
    extraReducers : (builder) => {
        builder
        // Get Carburant
        .addCase( getFuel.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( getFuel.fulfilled , (state , action) => {
            state.loading = false;
            if(action.payload){
                state.success = true;
                state.fuels = action.payload
            }
        })
        .addCase( getFuel.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;

        })

    }

})

export const {updateVehicleFromFuelState, addFuelToState} = FuelSlice.actions
export default FuelSlice.reducer