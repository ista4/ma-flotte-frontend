import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../helpers/axios"


export const getFuelStatistiques = createAsyncThunk('fuel/getFuelStatistiques' , async (_ , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
  
    try {
        const request  = await axiosInstance.get(`/carburants/statistiques`,{
            headers : {
                Authorization : 'Bearer ' + (
                    localStorage.sp_admin_token ||
                    localStorage.token)
            }
        })
        
        const res = await request.data
        return res
    } catch (error) {
        return rejectWithValue(error)
    }
})


const initialState = {
    loading : false,
    statistiques : [],
    success: false,
    rejected : false,
}

const FuelStatistiquesSlice = createSlice({
    name: 'fuel',
    initialState ,

    extraReducers : (builder) => {
        builder
        // Get Carburant
        .addCase( getFuelStatistiques.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( getFuelStatistiques.fulfilled , (state , action) => {
            state.loading = false;
            if(action.payload){
                state.success = true;
                state.statistiques = action.payload
            }
        })
        .addCase( getFuelStatistiques.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;

        })

    }

})

export default FuelStatistiquesSlice.reducer