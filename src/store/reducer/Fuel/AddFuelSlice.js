import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosInstance from "../../../helpers/axios";

export const addFuel = createAsyncThunk(
  "fuel/addFuel",
  async (data, thunkApi) => {
    const { rejectWithValue } = thunkApi;

    try {
      const request = await axiosInstance.post(
        `/carburants`,
        {
          kilometrage: data.kilometrage,
          prix: data.prix,
          quantite: data.quantite,
          vehicule_id: data.vehicule,
          type: data.type,
          recu_image: data.image,
          nom: data.nom,
          lat: data.latLng.lat,
          lng: data.latLng.lng,
        },
        {
          headers: {
            Authorization:
              "Bearer " +
              (localStorage.user_token ||
                localStorage.sp_admin_token ||
                localStorage.token),
            'Content-Type': 'multipart/form-data'
          },
        }
      );

      const res = await request.data;
      return res;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

const initialState = {
  loading: false,
  fuel: {},
  success: false,
  rejected: false,
};

const AddFuelSlice = createSlice({
  name: "fuel",
  initialState,

  extraReducers: (builder) => {
    builder
      // Add Carburant
      .addCase(addFuel.pending, (state, action) => {
        state.loading = true;
        state.success = false;
      })
      .addCase(addFuel.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload) {
          state.success = true;
          state.fuel = action.payload.carburant;
        }
      })
      .addCase(addFuel.rejected, (state, action) => {
        state.loading = false;
        state.success = false;
        state.rejected = true;
      });
  },
});

export default AddFuelSlice.reducer;
