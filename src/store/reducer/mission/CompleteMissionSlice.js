import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosInstance from "../../../helpers/axios";

// Add Mission 
export const completeMission = createAsyncThunk(
  "mission/completeMission",
  async (missionID, thunkApi) => {
    const { rejectWithValue } = thunkApi;
    try {
      const request = await axiosInstance.put(`/missions/complete`, 
      {
        id: missionID,
      },
      {
        headers: {
          Authorization:
            "Bearer " + (localStorage.user_token),
        },
      });

      const res = await request.data;

      return res;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);


const initialState = {
  loading: false,
  success: false,
  rejected: false,
};

const CompleteMissionSlice = createSlice({
  name: "mission",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      // update Order Location  
      .addCase(completeMission.pending, (state, action) => {
        state.loading = true;
        state.success = false;
      })
      .addCase(completeMission.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload.success) {
          state.success = true;
        }
      })
      .addCase(completeMission.rejected, (state, action) => {
        state.loading = false;
        state.success = false;
        state.rejected = true;
      })
  
  },
});


export default CompleteMissionSlice.reducer;
