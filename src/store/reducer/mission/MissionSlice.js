import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosInstance from "../../../helpers/axios";

export const getMissions = createAsyncThunk(
  "mission/getMissions",
  async (_, thunkApi) => {
    const { rejectWithValue } = thunkApi;
    try {
      const request = await axiosInstance.get(`/missions`, {
        headers: {
          Authorization:
            "Bearer " + (localStorage.user_token ||
              localStorage.sp_admin_token ||
              localStorage.token),
        },
      });

      const res = await request.data;

      return res;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);




const initialState = {
  missions: [],
  loading: false,
  success: false,
  rejected: false,
};

const MissionSlice = createSlice({
  name: "mission",
  initialState,
  reducers: {
    //add mission to state
    addMission: (state, action) => {
        state.missions.push(action.payload);
        },
    //delete mission from state
    deleteMission: (state, action) => {
        state.missions = state.missions.filter((mission) => mission.id !== action.payload);
    },
  },
  extraReducers: (builder) => {
    builder
      // get all Missions 
      .addCase(getMissions.pending, (state, action) => {
        state.loading = true;
        state.success = false;
      })
      .addCase(getMissions.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload) {
          state.success = true;
          state.missions = action.payload;
        }
      })
      .addCase(getMissions.rejected, (state, action) => {
        state.loading = false;
        state.success = false;
        state.rejected = true;
      })
      // get by id 
  

  },
});


export default MissionSlice.reducer;
