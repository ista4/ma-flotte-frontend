import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosInstance from "../../../helpers/axios";
// data.vehicule && data.dateRetour && 
//       data.nombrePersonne && data.lieu && 
//       data.objet && data.lat && data.lng 

// Add Mission 
export const addMission = createAsyncThunk(
  "mission/addMission",
  async (data, thunkApi) => {
    const { rejectWithValue } = thunkApi;
    try {
      const request = await axiosInstance.post(`/missions`, 
      {
        'vehicule_id': data.vehicule,
        'date_retour': data.dateRetour,
        'lieu': data.lieu,
        'object' : data.objet,
        'nbr_personne': data.nombrePersonne,
        'lat' : data.latLng.lat,
        'lng' : data.latLng.lng,
      },
      {
        headers: {
          Authorization:
            "Bearer " + (localStorage.user_token),
        },
      });

      const res = await request.data;

      return res;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);




const initialState = {
  loading: false,
  success: false,
  rejected: false,
};

const AddMissionSlice = createSlice({
  name: "mission",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      // add Mission  
      .addCase(addMission.pending, (state, action) => {
        state.loading = true;
        state.success = false;
      })
      .addCase(addMission.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload.success) {
          state.success = true;
        }
      })
      .addCase(addMission.rejected, (state, action) => {
        state.loading = false;
        state.success = false;
        state.rejected = true;
      })
  
  },
});


export default AddMissionSlice.reducer;
