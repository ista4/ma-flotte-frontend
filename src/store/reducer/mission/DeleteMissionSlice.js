import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosInstance from "../../../helpers/axios";

// Add Mission 
export const deleteMission = createAsyncThunk(
  "mission/deleteMission",
  async (id, thunkApi) => {
    const { rejectWithValue } = thunkApi;
    try {
      const request = await axiosInstance.delete(`/missions/${id}`,
      {
        headers: {
          Authorization:
            "Bearer " + (localStorage.sp_admin_token),
        },
      });

      const res = await request.data;

      return res;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);


const initialState = {
  loading: false,
  success: false,
  rejected: false,
};

const DeleteMissionSlice = createSlice({
  name: "mission",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      // update Order Location  
      .addCase(deleteMission.pending, (state, action) => {
        state.loading = true;
        state.success = false;
      })
      .addCase(deleteMission.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload.success) {
          state.success = true;
        }
      })
      .addCase(deleteMission.rejected, (state, action) => {
        state.loading = false;
        state.success = false;
        state.rejected = true;
      })
  
  },
});


export default DeleteMissionSlice.reducer;
