import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosInstance from "../../../helpers/axios";

// Add Mission 
export const printMission = createAsyncThunk(
  "mission/printMission",
  async (id, thunkApi) => {
    const { rejectWithValue } = thunkApi;
    try {
      const request = await axiosInstance.post(`/missions/pdf`,
      {
        id,
      },
      {
        headers: {
          Authorization:
            "Bearer " + (localStorage.sp_admin_token || localStorage.token),
        },
      });

      const res = await request.data;

      return res;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);


const initialState = {
  loading: false,
  pdf: null,
  success: false,
  rejected: false,
};

const PrintMissionSlice = createSlice({
  name: "mission",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      // update Order Location  
      .addCase(printMission.pending, (state, action) => {
        state.loading = true;
        state.success = false;
      })
      .addCase(printMission.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload.success) {
          state.success = true;
          state.pdf = action.payload.pdf;
        }
      })
      .addCase(printMission.rejected, (state, action) => {
        state.loading = false;
        state.success = false;
        state.rejected = true;
      })
  
  },
});


export default PrintMissionSlice.reducer;
