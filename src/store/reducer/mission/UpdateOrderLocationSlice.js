import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosInstance from "../../../helpers/axios";

// Add Mission 
export const updateOrderLocation = createAsyncThunk(
  "mission/updateOrderLocation",
  async (data, thunkApi) => {
    const { rejectWithValue } = thunkApi;
    try {
      const request = await axiosInstance.put(`/missions/${data.id}}`, 
      {
        'lat' : data.lat,
        'lng' : data.lng,
      },
      {
        headers: {
          Authorization:
            "Bearer " + (localStorage.user_token),
        },
      });

      const res = await request.data;

      return res;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);


const initialState = {
  loading: false,
  success: false,
  rejected: false,
};

const UpdateOrderLocationSlice = createSlice({
  name: "mission",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      // update Order Location  
      .addCase(updateOrderLocation.pending, (state, action) => {
        state.loading = true;
        state.success = false;
      })
      .addCase(updateOrderLocation.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload.success) {
          state.success = true;
        }
      })
      .addCase(updateOrderLocation.rejected, (state, action) => {
        state.loading = false;
        state.success = false;
        state.rejected = true;
      })
  
  },
});


export default UpdateOrderLocationSlice.reducer;
