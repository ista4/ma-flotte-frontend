import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    open: false,
}

const MenuListSlice = createSlice({
    name: 'menuList',
    initialState ,
    reducers : {
        openMenuList : (state, action) => {
            state.open = action.payload;
        },
        closeMenuList : (state, action) => {
            state.open = false;
        },
    }

})

export const {openMenuList, closeMenuList} =  MenuListSlice.actions

export default MenuListSlice.reducer