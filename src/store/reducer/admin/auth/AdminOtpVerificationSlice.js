
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"
import { getAdmin } from "../AdminSlice"

export const adminOtp = createAsyncThunk('adminOtp' , async (otp , thunkApi) => {
    
    const {rejectWithValue, dispatch } = thunkApi
    
    
    const admin = await dispatch(getAdmin())

    try {
        const request  = await axiosInstance.put(`/admin/otpVerification`, {
            identifiant : admin.payload.cin,
            otp : otp
        },{
            headers : {
                Authorization : 'Bearer ' + localStorage.token
            }
        })
        
        const res = await request.data

        return res
    } catch (error) {
        return rejectWithValue(error)
    }
})


const initialState = {
    loading : false,
    success: false,
    rejected : false,
}

const AdminOtpVerificationSlice = createSlice({
    name: 'adminOTP',
    initialState ,
    extraReducers : (builder) => {
        builder
        // otp verification
        .addCase( adminOtp.pending , (state , action) => {
            state.loading = true;
            state.success = false;
            
        })
        .addCase( adminOtp.fulfilled , (state , action) => {
            state.loading = false;
           
            if(action.payload.success){
                state.success = true;
                state.rejected = false;  
            }
            

            console.log(action.payload);
        })
        .addCase( adminOtp.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;
            console.log(action.payload);
        })

    }

})


export default AdminOtpVerificationSlice.reducer