import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"


export const resetPassword = createAsyncThunk('resetPassword' , async (data , thunkApi) => {
    
    const {rejectWithValue } = thunkApi

    console.log(data);
    try {
        const request  = await axiosInstance.put(`/admin/resetPassword`, {
            email : localStorage.getItem('admin-email') ? localStorage.getItem('admin-email') : '',
            otp : data.otp,
            password : data.password,
            password_confirmation : data.passwordConfirmation,
        })

        const res = await request.data
        return res

    } catch (error) {
        return rejectWithValue(error.response.data.message)
    }
})


const initialState = {
    loading : false,
    success: false,
    rejected : false,
}

const AdminResetPasswordSlice = createSlice({
    name: 'adminResetPassword',
    initialState ,
    extraReducers : (builder) => {
        builder
        // otp verification
        .addCase( resetPassword.pending , (state , action) => {
            state.loading = true;
            state.success = false;
            
        })
        .addCase( resetPassword.fulfilled , (state , action) => {
            state.loading = false;
           
            if(action.payload.status){
                state.success = true;
                state.rejected = false;  
                localStorage.removeItem('admin-email');
            }
            

            console.log(action.payload);
        })
        .addCase( resetPassword.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;
            console.log(action.payload);
        })

    }

})


export default AdminResetPasswordSlice.reducer