import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"



export const forgetPassword = createAsyncThunk('forgetPassword' , async (email , thunkApi) => {
    
    const {rejectWithValue } = thunkApi

    try {
        const request  = await axiosInstance.post(`/admin/forgetPassword`, {email : email})
        const res = await request.data
        localStorage.setItem('admin-email', email)
        return res
    } catch (error) {
        return rejectWithValue(error.response.data.msg)
    }
})


const initialState = {
    loading : false,
    success: false,
    rejected : false,
}

const AdminForgetPasswordSlice = createSlice({
    name: 'adminForgetPassword',
    initialState ,
    extraReducers : (builder) => {
        builder
        // otp verification
        .addCase( forgetPassword.pending , (state , action) => {
            state.loading = true;
            state.success = false;
            
        })
        .addCase( forgetPassword.fulfilled , (state , action) => {
            state.loading = false;
           
            if(action.payload.success){
                state.success = true;
                state.rejected = false;  
            }
            

   
        })
        .addCase( forgetPassword.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;
    
        })

    }

})


export default AdminForgetPasswordSlice.reducer