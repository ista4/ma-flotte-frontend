
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"





export const adminLogin = createAsyncThunk('adminLogin' , async (data , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
    
    console.log(data)
    try {
        const request  = await axiosInstance.post(`/admin/login`, {
            identifiant : data.identifiant,
            password : data.password
        },{
            headers : {
                Authorization : 'Bearer ' + localStorage.token
            }
        })
        
        const res = await request.data

        return res
    } catch (error) {
        return rejectWithValue(error)
    }
})


const initialState = {
    token: '',
    loading : false,
    success: false,
    rejected : false,
}

const AdminLoginSlice = createSlice({
    name: 'adminLogin',
    initialState ,
    extraReducers : (builder) => {
        builder
        // login
        .addCase( adminLogin.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( adminLogin.fulfilled , (state , action) => {
            state.loading = false;
           
            if(action.payload.success){
                state.success = true;
                state.token = action.payload.token;
                state.rejected = false;
                localStorage.setItem('token', state.token);
            }
            

            console.log(action.payload);
        })
        .addCase( adminLogin.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;
        })

    }

})


export default AdminLoginSlice.reducer