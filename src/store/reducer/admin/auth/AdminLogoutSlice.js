
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"





export const adminLogout = createAsyncThunk('adminLogout' , async (data , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
    
    try {
        const request  = await axiosInstance.delete(`/admin/logout`,{
            headers : {
                Authorization : 'Bearer ' + localStorage.token
            }
        })
        
        const res = await request.data

        return res
    } catch (error) {
        return rejectWithValue(error.message)
    }
})


const initialState = {
    loading : false,
    success: false,
    rejected : false,
}

const AdminLogoutSlice = createSlice({
    name: 'adminLogout',
    initialState ,
    extraReducers : (builder) => {
        builder
        // login
        .addCase( adminLogout.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( adminLogout.fulfilled , (state , action) => {
            state.loading = false;
           
            if(action.payload.success){
                state.success = true;   
                localStorage.removeItem('token');
                state.rejected = false;
                state.loading = false;
            }
        })
        .addCase( adminLogout.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;

        })

    }

})


export default AdminLogoutSlice.reducer