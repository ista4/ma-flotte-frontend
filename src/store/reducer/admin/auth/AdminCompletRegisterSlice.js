
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"





export const adminCompletRegister = createAsyncThunk('adminCompletRegister' , async (data , thunkApi) => {
    
    const {rejectWithValue} = thunkApi

    
    try {
        const request  = await axiosInstance.post(`/admin/completRegister`, {
            email : data.email,
            tel : data.tel,
            sexe : data.sexe
        },{
            headers : {
                Authorization : 'Bearer ' + localStorage.token
                
            }
        })
        
        const res = await request.data

        return res
    } catch (error) {
        return rejectWithValue(error.response.data.message)
    }
})


const initialState = {
    loading : false,
    success: false,
    rejected : false,
}

const AdminCompletRegisterSlice = createSlice({
    name: 'adminCompletRegister',
    initialState ,
    extraReducers : (builder) => {
        builder
        // login
        .addCase( adminCompletRegister.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( adminCompletRegister.fulfilled , (state , action) => {
            state.loading = false;
            if(action.payload.success){
                state.success = true;
                state.rejected = false;
            }
            
        })
        .addCase( adminCompletRegister.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;
         
        })

    }

})


export default AdminCompletRegisterSlice.reducer