import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../helpers/axios"





export const getAdmin = createAsyncThunk('getAdmin' , async (_ , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
    
    try {
        const request  = await axiosInstance.get(`/admin`,{
            headers : {
                Authorization : 'Bearer ' + localStorage.token
            }
        })
        
        const res = await request.data

        return res
    } catch (error) {
        return rejectWithValue(error.message)
    }
})


const initialState = {
    admin: {},
    loading : false,
    success: false,
    rejected : false,
}

const AdminSlice = createSlice({
    name: 'admin',
    initialState ,
    extraReducers : (builder) => {
        builder
        // login
        .addCase( getAdmin.pending , (state , action) => {
            state.loading = true;
            state.success = false;
            
        })
        .addCase( getAdmin.fulfilled , (state , action) => {
            state.loading = false;
            state.admin = action.payload;
        })
        .addCase( getAdmin.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;

        })

    }

})


export default AdminSlice.reducer