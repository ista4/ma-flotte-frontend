import {createSlice } from "@reduxjs/toolkit"


const initialState = {
    open: false,
    componentName: '',
    title : '',
    childrenProps: {},
    isFullScreen  : false
}

const ResponsiveModalSlice = createSlice({
    name: 'responsiveModal',
    initialState ,
    reducers : {
        openResponsiveModal : (state, action) => {
            state.open = true;
            state.componentName = action.payload.componentName;
            state.title = action.payload.title;
            state.childrenProps = action.payload.childrenProps; 
            state.isFullScreen = action.payload.isFullScreen;
        },
        closeResponsiveModal : (state, action) => {
            state.open = false;
            state.componentName = '';
            state.title = '';  
            state.childrenProps = {};  
            state.isFullScreen = false;
        },
    }

})

export const {openResponsiveModal, closeResponsiveModal} =  ResponsiveModalSlice.actions

export default ResponsiveModalSlice.reducer