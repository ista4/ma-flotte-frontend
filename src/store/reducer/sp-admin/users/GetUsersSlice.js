import { createAsyncThunk, createSlice, current } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"


export const getUsers = createAsyncThunk('users/getUsers' , async (_ , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
    
    try {
        const request  = await axiosInstance.get(`/sp-admin/users`,{
            headers : {
                Authorization : 'Bearer ' + localStorage.sp_admin_token || localStorage.token 
            }
        })
        
        const res = await request.data

        return res
    } catch (error) {
        return rejectWithValue(error)
    }
})


const initialState = {
    users: [],
    loading : false,
    success: false,
    rejected : false,
}

const GetUsersSlice = createSlice({
    name: 'users',
    initialState ,
    reducers : {
        removeUsers(state, action){

            const users = state.users.map((user) => {
                if(!action.payload.includes(user.id) ){
                    return current(user)
                }
                return null
            })
            
            state.users = users.filter(e => e !== null)
            
        },
        addUserToState(state, action) {

            state.users.push(action.payload);
        },

        updateUsersState: (state, action)=>{

            const data = state.users.filter((user) => { 
                return user.id !== action.payload.id
            })


            state.users = [...data, action.payload]
    },
       
    },
    extraReducers : (builder) => {
        builder
        // login
        .addCase( getUsers.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( getUsers.fulfilled , (state , action) => {
            state.loading = false;
            if(action.payload.success){
                state.success = true;
                state.users = action.payload.users
            }
        })
        .addCase( getUsers.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;

        })

    }

})

export const {addUserToState, removeUsers , updateUsersState} = GetUsersSlice.actions
export default GetUsersSlice.reducer