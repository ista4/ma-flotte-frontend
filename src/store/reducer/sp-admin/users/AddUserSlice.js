import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"

export const addUser = createAsyncThunk('addUser' , async (data , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
    
    try {
        const request  = await axiosInstance.post(`/sp-admin/adduser`, {
            nom : data.nom,
            prenom : data.prenom,
            role : data.role,
            identifiant : data.identifiant  
        },{
            headers : {
                Authorization : 'Bearer ' + localStorage.sp_admin_token
            }
        })
        
        const res = await request.data

        return res
    } catch (error) {
        return rejectWithValue(error.message)
    }
})


const initialState = {
    loading : false,
    success: false,
    rejected : false,
}

const AddUserSlice = createSlice({
    name: 'users',
    initialState ,
    extraReducers : (builder) => {
        builder
        // login
        .addCase( addUser.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( addUser.fulfilled , (state , action) => {
            state.loading = false;
            if(action.payload.success){
                state.success = true;
            }
        })
        .addCase( addUser.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;

        })

    }

})

export default AddUserSlice.reducer