import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"


export const printUsers = createAsyncThunk('printUsers' , async (ids , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
    

    try {
        const request  = await axiosInstance.post(`/sp-admin/users/print` ,
            {
             ids,
            }
        , {
            headers : {
                Authorization : 'Bearer ' + localStorage.sp_admin_token,
            }
        })
        
        const res = await request.data
        return res
    } catch (error) {
        return rejectWithValue(error)
    }
})


const initialState = {
    loading : false,
    pdf: "",
    success: false,
    rejected : false,
}

const PrintUsersSlice = createSlice({
    name: 'users',
    initialState ,
    
    extraReducers : (builder) => {
        builder
        // PrintUsers
        .addCase( printUsers.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( printUsers.fulfilled , (state , action) => {
            state.loading = false;
            if(action.payload.success){
                state.success = true;
                state.pdf = action.payload.pdf
         

            }
            console.log(action.payload);
        })
        .addCase( printUsers.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;
 
        })

    }

})


export default PrintUsersSlice.reducer