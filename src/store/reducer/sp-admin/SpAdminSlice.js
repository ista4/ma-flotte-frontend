import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../helpers/axios"





export const getSpAdmin = createAsyncThunk('getSpAdmin' , async (_ , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
    
    try {
        const request  = await axiosInstance.get(`/sp-admin`,{
            headers : {
                Authorization : 'Bearer ' + localStorage.sp_admin_token
            }
        })
        
        const res = await request.data

        return res
    } catch (error) {
        return rejectWithValue(error.message)
    }
})


const initialState = {
    spAdmin: {},
    loading : false,
    success: false,
    rejected : false,
}

const SpAdminSlice = createSlice({
    name: 'SpAdmin',
    initialState ,
    extraReducers : (builder) => {
        builder
        // login
        .addCase( getSpAdmin.pending , (state , action) => {
            state.loading = true;
            state.success = false;
            
        })
        .addCase( getSpAdmin.fulfilled , (state , action) => {
            state.loading = false;
            state.spAdmin = action.payload;
        })
        .addCase( getSpAdmin.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;

        })

    }

})


export default SpAdminSlice.reducer