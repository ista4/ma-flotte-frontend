import { createAsyncThunk, createSlice, current } from "@reduxjs/toolkit"

import axiosInstance from "../../../../helpers/axios"


export const getAdmins = createAsyncThunk('getAdmins' , async (_ , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
    
    try {
        const request  = await axiosInstance.get(`/sp-admin/admins`,{
            headers : {
                Authorization : 'Bearer ' + localStorage.sp_admin_token
            }
        })
        
        const res = await request.data

        return res
    } catch (error) {
        return rejectWithValue(error)
    }
})




const initialState = {
    admins: [],
    loading : false,
    success: false,
    rejected : false,
}

const GetAdminsSlice = createSlice({
    name: 'admins',
    initialState ,
    reducers : {
        removeAdmins(state, action){

            const admins = state.admins.map((admin) => {
                if(!action.payload.includes(admin.id) ){
                    return current(admin)
                }
                return null
            })
            state.admins = admins.filter(e => e !== null)
        },
        addAdminToState: (state, action)=>{

          
            state.admins.push(action.payload)
        },
        updateAdminsState: (state, action)=>{
                const data = state.admins.filter((admin) => { 
                    return admin.id !== action.payload.id
                })


                state.admins = [...data, action.payload]
        },
    },
    extraReducers : (builder) => {
        builder
        // get
        .addCase( getAdmins.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( getAdmins.fulfilled , (state , action) => {
            state.loading = false;
            if(action.payload.success){
                state.success = true;
                state.admins = action.payload.admins
            }
        })
        .addCase( getAdmins.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;

        })

    }

})


export const {removeAdmins , addAdminToState, updateAdminsState} = GetAdminsSlice.actions
export default GetAdminsSlice.reducer