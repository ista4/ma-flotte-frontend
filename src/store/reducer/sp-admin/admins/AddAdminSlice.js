import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"
import { getAdmins } from "./GetAdminsSlice"

export const addAdmin = createAsyncThunk('addAdmin' , async (data , thunkApi) => {
    
    const {rejectWithValue, dispatch} = thunkApi



    
    try {
        const request  = await axiosInstance.post(`/sp-admin/addadmin`, {
            nom : data.nom,
            prenom : data.prenom,
            role : data.role,
            identifiant : data.identifiant  
        },{
            headers : {
                Authorization : 'Bearer ' + localStorage.sp_admin_token
            }
        })
        
        const res = await request.data
        return res
    } catch (error) {
        return rejectWithValue(error.response.data.message)
    }
})


const initialState = {
    loading : false,
    success: false,
    rejected : false,
}

const AddAdminSlice = createSlice({
    name: 'admins',
    initialState ,
    extraReducers : (builder) => {
        builder
        // login
        .addCase( addAdmin.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( addAdmin.fulfilled , (state , action) => {
            state.loading = false;
            if(action.payload.success){
                state.success = true;
            }
        })
        .addCase( addAdmin.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;

        })

    }

})

export default AddAdminSlice.reducer