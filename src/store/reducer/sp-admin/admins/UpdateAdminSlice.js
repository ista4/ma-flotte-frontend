import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"

export const updateAdmin = createAsyncThunk('updateAdmin' , async ({id, field, value , ...others} , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
    const data = {'_method': 'PUT', ...others}
    data[field] = value

    try {
        const request  = await axiosInstance.post(`/admin/update/${id}`,data,{
            headers : {
                Authorization : 'Bearer ' + (localStorage.sp_admin_token || localStorage.token),
                'Content-Type' : 'multipart/form-data'
            }
            
        })
        
        const res = await request.data

        return res
    } catch (error) {
        return rejectWithValue(error.response.data.message)
    }
})

const initialState = {
    loading : false,
    success: false,
    rejected : false,
}

const UpdateAdminSlice = createSlice({
    name: 'admins',
    initialState ,
    extraReducers : (builder) => {
        builder
        // login
        .addCase( updateAdmin.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( updateAdmin.fulfilled , (state , action) => {
            state.loading = false;
            if(action.payload.success){

                state.success = true;
     
            }
        })
        .addCase( updateAdmin.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;

        })
    }

})

export default UpdateAdminSlice.reducer