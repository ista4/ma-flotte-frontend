import { createAsyncThunk, createSlice , current} from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"


export const deleteAdmins = createAsyncThunk('deleteAdmins' , async (ids , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
    
    try {
        const request  = await axiosInstance.delete(`/sp-admin/admins`, {
            headers : {
                Authorization : 'Bearer ' + localStorage.sp_admin_token,
                'X-ADMINS' : JSON.stringify(ids), 
            }
        })
        
        const res = await request.data
        return res
    } catch (error) {
        return rejectWithValue(error.response.data.message)
    }
})


const initialState = {
    loading : false,
    success: false,
    rejected : false,
}

const DeleteAdminsSlice = createSlice({
    name: 'admins',
    initialState ,
    
    extraReducers : (builder) => {
        builder
        // deleteAdmins
        .addCase( deleteAdmins.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( deleteAdmins.fulfilled , (state , action) => {
            state.loading = false;
            if(action.payload.success){
                state.success = true;
                
            }
            console.log(action.payload);
        })
        .addCase( deleteAdmins.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;
 
        })

    }

})


export default DeleteAdminsSlice.reducer