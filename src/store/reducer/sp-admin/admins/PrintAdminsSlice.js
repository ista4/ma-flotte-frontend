import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"


export const printAdmins = createAsyncThunk('printAdmins' , async (ids , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
    

    try {
        const request  = await axiosInstance.post(`/sp-admin/admins/print` ,
            {
             ids,
            }
        , {
            headers : {
                Authorization : 'Bearer ' + localStorage.sp_admin_token,
               
            }
        })
        
        const res = await request.data
        return res
    } catch (error) {
        return rejectWithValue(error)
    }
})


const initialState = {
    loading : false,
    pdf: "",
    success: false,
    rejected : false,
}

const PrintAdminsSlice = createSlice({
    name: 'admins',
    initialState ,
    
    extraReducers : (builder) => {
        builder
        //
        .addCase( printAdmins.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( printAdmins.fulfilled , (state , action) => {
            state.loading = false;
            if(action.payload.success){
                state.success = true;
                state.pdf = action.payload.pdf
         

            }
            console.log(action.payload);
        })
        .addCase( printAdmins.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;
 
        })

    }

})


export default PrintAdminsSlice.reducer