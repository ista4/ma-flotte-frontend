import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"


export const spAdminLogout = createAsyncThunk('spAdminLogout' , async (data , thunkApi) => {
    const {rejectWithValue} = thunkApi
    try {
        const request  = await axiosInstance.delete(`/sp-admin/logout`,{
            headers : {
                Authorization : 'Bearer ' + localStorage.sp_admin_token
            }
        })
        const res = await request.data
        return res
    } catch (error) {
        return rejectWithValue(error.message)
    }
})


const initialState = {
    loading : false,
    success: false,
    rejected : false,
}

const SpAdminLogoutSlice = createSlice({
    name: 'spAdminLogout',
    initialState ,
    extraReducers : (builder) => {
        builder
        // login
        .addCase( spAdminLogout.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( spAdminLogout.fulfilled , (state , action) => {
            state.loading = false;
           
            if(action.payload.success){
                state.success = true;   
                localStorage.removeItem('sp_admin_token');
                state.rejected = false;
                state.loading = false;
            }
        })
        .addCase( spAdminLogout.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;

        })

    }

})


export default SpAdminLogoutSlice.reducer