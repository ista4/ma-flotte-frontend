
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axiosInstance from "../../../../helpers/axios"


export const spAdminLogin = createAsyncThunk('spAdminLogin' , async (data , thunkApi) => {
    
    const {rejectWithValue} = thunkApi
    try {
        const request  = await axiosInstance.post(`/sp-admin/login`, {
            email : data.identifiant,
            password : data.password
        },{
            headers : {
                Authorization : 'Bearer ' + localStorage.sp_admin_token
            }
        })
        const res = await request.data

        return res
    } catch (error) {
        return rejectWithValue(error.message)
    }
})


const initialState = {
    token: '',
    loading : false,
    success: false,
    rejected : false,
}

const SpAdminLoginSlice = createSlice({
    name: 'spAdminLogin',
    initialState ,
    extraReducers : (builder) => {
        builder
        // login
        .addCase( spAdminLogin.pending , (state , action) => {
            state.loading = true;
            state.success = false;
        })
        .addCase( spAdminLogin.fulfilled , (state , action) => {
            state.loading = false;
           
            if(action.payload.success){
                state.success = true;
                state.token = action.payload.token;
                state.rejected = false;
                localStorage.setItem('sp_admin_token', state.token);
            }
            

            console.log(action.payload);
        })
        .addCase( spAdminLogin.rejected , (state , action) => {
            state.loading = false;
            state.success = false;
            state.rejected = true;
        })

    }

})


export default SpAdminLoginSlice.reducer