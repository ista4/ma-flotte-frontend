import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import { Box } from '@mui/system';

export default function CardBox({nom, kilometrage, matricule, image, loading, qrCode}) {
  return (

    <Card sx={{ maxWidth: 340 , margin:'auto', boxShadow:'0 1px 6px #1c1c1c73'}}>
    
      <CardActionArea>
        <CardMedia
          component="img"
          height="200"
          image={image}
          alt="green iguana"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {nom}
          </Typography>
          <Box sx={{
            display: 'flex',
            justifyContent: 'space-between',
          }}>
          <Typography variant="body2" color="text.dark">
            Matricule : {matricule} <br/>
            kilometrage : {kilometrage} <br/>
          </Typography>

          <Typography variant="body2" color="text.dark">

            statu : active <br/>
          </Typography>
          </Box>
        </CardContent>
      </CardActionArea>
  
    </Card>
  );
}