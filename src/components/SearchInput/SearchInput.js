import * as React from "react";
import Paper from "@mui/material/Paper";
import InputBase from "@mui/material/InputBase";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import ClearIcon from "@mui/icons-material/Clear";

import MenuList from "./MenuList";
import { useDispatch, useSelector } from "react-redux";
import { MenuItem } from "@mui/material";
import { closeMenuList } from "../../store/reducer/MenuListSlice";
import { searchVehicleFromState } from "../../store/reducer/vehicle/VehicleSlice";

export default function SearchInput() {
  const [nom, setNom] = React.useState("");
  const [matricule, setMatricule] = React.useState("");

  const handelChange = (data) => {
    setNom(data.nom)
    setMatricule(data.matricule)

    if (data.nom) {

      dispatch(searchVehicleFromState({ nom: data.nom }));
    }else{
      dispatch(searchVehicleFromState({ nom: null, matricule:data.matricule }));
    };

    if (data.matricule) {

      dispatch(searchVehicleFromState({ matricule: data.matricule }));
    }else{
      dispatch(searchVehicleFromState({ matricule: null , nom : data.nom }));
    }
    


  };

  const dispatch = useDispatch();
  const { types } = useSelector((state) => state.VehicleType);

  const handleItemClick = (type) => {
    dispatch(closeMenuList());

    dispatch(searchVehicleFromState({ type: type }));
  };

  const handelClear = () => {
    dispatch(searchVehicleFromState({ type: null , nom : null , matricule : null}));
    setNom("");
    setMatricule("");
  };


  return (
    <Paper
      sx={{ p: "2px 4px", display: "flex", alignItems: "center", width: 500 }}
    >
      {/* <IconButton sx={{ p: "10px" }} aria-label="menu">
        <MenuIcon />
      </IconButton> */}

      <form style={{ display: "flex" }}>
        <InputBase
          sx={{ ml: 1, flex: 1 }}
          type="text"
          placeholder="matricule"
          title="par immatriculation"
          value={matricule ?? ''}
          onChange={(e) => handelChange({ matricule: e.target.value })}
          inputProps={{ "aria-label": "search car by  immatriculation" }}
        />
        <Divider sx={{ height: 28, m: 0.2 }} orientation="vertical" />
        <InputBase
          sx={{ ml: 1, flex: 1 }}
          type="text"
          title="par nom"
          value={nom ?? ''}
          onChange={(e) => handelChange({ nom: e.target.value })}
          placeholder="nom"
          inputProps={{ "aria-label": "search car by  nom" }}
        />

        <IconButton
          type="button"
          sx={{ p: "6px" }}
          onClick={handelClear}
          aria-label="search"
        >
          <ClearIcon />
        </IconButton>
        <Divider sx={{ height: 28, m: 0.5 }} orientation="vertical" />
      </form>

      <MenuList>
        {types.map((type) => (
          <MenuItem key={type.id} onClick={() => handleItemClick(type.id)}>
            {" "}
            {type.type}{" "}
          </MenuItem>
        ))}
      </MenuList>
    </Paper>
  );
}
