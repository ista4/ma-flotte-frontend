import React from 'react';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';

import MoreVertIcon from '@mui/icons-material/MoreVert';
import { Box } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { closeMenuList, openMenuList } from '../../store/reducer/MenuListSlice';


const ITEM_HEIGHT = 32;

export default function MenuList({children,loading}) {


  
  const [anchorEl, setAnchorEl] = React.useState(null);

  const dispatch = useDispatch();
  const {open} = useSelector(state => state.menuList);

  const handleClick = (event) => {
  
    setAnchorEl(event.currentTarget);
    dispatch(openMenuList(Boolean(event.currentTarget)));

  };
  const handleClose = () => {
    dispatch(closeMenuList())
    setAnchorEl(null);
  };





  return (
    <Box >
      <Button
        aria-label="more"
        id="long-button"
        sx={{color:'#1c1c1c',}}
        aria-controls={open ? 'long-menu' : undefined}
        aria-expanded={open ? 'true' : undefined}
        aria-haspopup="true"
        onClick={handleClick}
        startIcon={<MoreVertIcon />}
      >
         types  
      </Button>
      <Menu
        id="long-menu"
        MenuListProps={{
          'aria-labelledby': 'long-button',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        PaperProps={{
          style: {
            maxHeight: ITEM_HEIGHT * 4.5,
            width: '20ch',
          },
        }}
      >
        {loading ? '...': (children ?? <span>Aucun type</span>)}
      </Menu>
      
    </Box>
  );
}