import { LoadingButton } from '@mui/lab'
import {  TextField } from '@mui/material'
import { Box } from '@mui/system'
import { enqueueSnackbar } from 'notistack'
import React from 'react'
import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { closeResponsiveModal } from '../../store/reducer/ResponsiveModalSlice'
import { updateVehiculesType, updateVehiculesTypeFromState } from '../../store/reducer/vehicle/VehicleTypeSlice'

const UpdateVehicleType = ({id, type}) => {

  const [name, setName] = useState("")
  const {loading} = useSelector((state) => state.VehicleType)
  const dispatch = useDispatch()
  const handelSubmit = (e) => {
    e.preventDefault()
    
    if (name) {
      dispatch(updateVehiculesType({id, type: name}))
        .unwrap()
        .then((res) => {
          dispatch(updateVehiculesTypeFromState(res.type));
          enqueueSnackbar("Type de véhicule modifié avec succès", {variant: "success"})
          dispatch(closeResponsiveModal())
        }).catch((err) => {
          enqueueSnackbar(err.response.data.message, {variant: "error"})
        })
    }
  }
  return (
    <>
         <form onSubmit={handelSubmit}>
        <Box
        sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              gap: "16px"
            }}>
          <TextField
            type={"text"}
            variant="outlined"
            placeholder={type}
            onChange={(e) => setName(e.target.value)}
            value={name}
          />

          <LoadingButton
            type="submit"
            variant="outlined"
            color='warning'
            loading={loading}
            sx={{
              height: "56px",
            }}
          >
            Modifier
          </LoadingButton>
        </Box>
      </form>
    
    </>
  )
}

export default UpdateVehicleType