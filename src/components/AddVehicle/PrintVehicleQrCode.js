import { Box, Button } from "@mui/material";
import React from "react";
import SelectMultiple from "../SelectMultiple/SelectMultiple";
import { useDispatch, useSelector } from "react-redux";
import { LoadingButton } from "@mui/lab";

import PrintIcon from '@mui/icons-material/Print';
import { Clear } from "@mui/icons-material";
import { closeResponsiveModal } from "../../store/reducer/ResponsiveModalSlice";
import { enqueueSnackbar } from "notistack";
import { printVehiculeQrCode } from "../../store/reducer/vehicle/PrintVehiculeQrCodeSlice";
import downloadFile from "../../helpers/fileDownload";
import DownloadIcon from '@mui/icons-material/Download';
const PrintVehicleQrCode = () => {
  const { vehicules } = useSelector((state) => state.Vehicle);
  const {pdf, loading} = useSelector((state) => state.PrintVehiculeQrCode);
  const dispatch = useDispatch();
  let ids = [];


  const getData = (data) => {
    ids = data.map((item) => item.id);
  };

  const handelQrCodePrint = () => {
    if(!ids.length){
      enqueueSnackbar("Veuillez selectionner au moins un vehicule", { variant: "warning" });
      return
    };
    
    dispatch(printVehiculeQrCode(ids)).unwrap()
    .then((res) => {
      console.log(res);
      downloadFile(res.pdf , "Mafloote_Vehicule_QrCodes.pdf")

      enqueueSnackbar('utilisateurs imprimer avec succès' ,{ variant : 'success'})
    }).catch((err) => console.log(err))

  };

  const handelDownload = () => {

    dispatch(closeResponsiveModal())
  };

  return (
    <Box>
      <SelectMultiple
        label="vehicules"
        data={vehicules}
        getData={getData}
        viewField={["nom", "immatriculation"]}
      />

{pdf ? (
            <Button
              href={pdf}
              onClick={handelDownload} 
              fullWidth 
              disabled={loading}
              target="_blank"
              color='info'
              variant='contained'
              sx={{ mt: "16px" , mb:'16px',  gap:'16px'}}
              download='Mafloote_Vehicule_QrCodes.pdf'
              endIcon={<DownloadIcon/>}
              rel="noreferrer"
              
            >

              Telecharger 
            </Button>
          ) : (
            ""
          )}
      <Box
      sx={{
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "end",
        mt: "16px",
        gap: "16px"
      }}
      >
      <Button endIcon={<Clear/>} 
      onClick={() => dispatch(closeResponsiveModal())}
      disabled={loading}      
       variant="contained" > Anuller </Button>

      <LoadingButton sx={{ gap: "12px" }}
        variant="contained"
        endIcon={<PrintIcon/>}
        loading={loading}
        color="success"
        onClick={handelQrCodePrint}>
        imprimer
      </LoadingButton>
      </Box>
    </Box>
  );
};

export default PrintVehicleQrCode;
