import { LoadingButton } from '@mui/lab';
import { Box, Button, Typography } from '@mui/material'
import { enqueueSnackbar } from 'notistack';
import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { closeResponsiveModal } from '../../store/reducer/ResponsiveModalSlice';
import { deleteVehiculesType, deleteVehiculesTypeFromState } from '../../store/reducer/vehicle/VehicleTypeSlice';
import styles from './style.module.css';


const DeleteVehicleType = ({id, type}) => {

    const dispatch  = useDispatch()
    const {loading} = useSelector((state) => state.VehicleType)
    const handelDelete = () => {
        dispatch(deleteVehiculesType(id)).unwrap().then((res) => {

            dispatch(closeResponsiveModal())
            dispatch(deleteVehiculesTypeFromState(id))
            enqueueSnackbar("Type de véhicule supprimé avec succès", {variant: "success"})
        }).catch((err) => {
            enqueueSnackbar(err.response.data.message, {variant: "error"})
        })
    }
  return (
    <Box className={styles.content}>

        <Typography color='error'>
            
            Si vous supprimez ce type, 
            vous supprimez toutes les voitures qui lui appartiennent
        </Typography>

        <Box
        
        sx={{
            display: 'flex',
            justifyContent: 'end',
            alignItems: 'center',
            gap:'16px'
        }}
        >
            <Button
            variant='contained'
            color='success'
            disabled={loading}
            onClick={() => dispatch(closeResponsiveModal())}
            >
                Anuler
            </Button>

            <LoadingButton
            onClick={handelDelete}
            variant='contained'
            color='error'
            loading={loading}
            >
            supprimer
            </LoadingButton>
        </Box>
    </Box>

  )
}

export default DeleteVehicleType