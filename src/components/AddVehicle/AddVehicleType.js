import {
  CircularProgress,
  Divider,
  IconButton,
  TextField,
} from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import { Box } from "@mui/system";
import React, { useState } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import { useDispatch, useSelector } from "react-redux";
import { openResponsiveModal } from "../../store/reducer/ResponsiveModalSlice";
import {
  addVehicleTypeToState,
  addVehiculesType,
} from "../../store/reducer/vehicle/VehicleTypeSlice";
import { LoadingButton } from "@mui/lab";




export const AddVehicleType = () => {

  
  const dispatch = useDispatch();

  const { types, loading } = useSelector((state) => state.VehicleType);

  const [name, setName] = useState("");

  const handelDeleteAction = (id, type) => {
    dispatch(
      openResponsiveModal({
        componentName: "DeleteVehicleType",
        title: `supprimer ${type} ?`,
        childrenProps: { id, type },
      })
    );
  };

  const handelUpdateAction = (id, type) => {
    dispatch(
      openResponsiveModal({
        componentName: "UpdateVehicleType",
        title: "Modifier Type",
        childrenProps: { id, type },
      })
    );
  };

  const handelSubmit = (e) => {
    e.preventDefault();

    if (name) {
      dispatch(addVehiculesType(name))
        .unwrap()
        .then((res) => {
          console.log(res);
          dispatch(addVehicleTypeToState(res.type));
          setName("");
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  return (
    <Box>
      <form onSubmit={handelSubmit} method="post">
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            gap: "16px",
          }}
        >
          <TextField
            type={"text"}
            variant="outlined"
            placeholder={"Ajouter nouveau type"}
            
            onChange={(e) => setName(e.target.value)}
            value={name}
          />

          <LoadingButton
            type="submit"
            variant="outlined"
            sx={{
              height: "56px",
            }}
            loading={loading}
          >
            Ajouter
          </LoadingButton>
        </Box>
      </form>
      <Divider sx={{ mt: "16px" }} />

      <TableContainer component={Paper} sx={{ mt: "16px" }}
      
      >
        <Table
          sx={{ width: "100%", overflow: "hidden" }}
          aria-label="simple tabl"
          
        >
          <TableHead
            sx={{
              background: "var(--light)",
            }}
            
          >
            <TableRow>
              <TableCell>Types</TableCell>
              <TableCell>N° vehicles </TableCell>
              <TableCell>Actions </TableCell>
            </TableRow>
            
          </TableHead>

          <TableBody>

            {types.map((type) => (
              <TableRow
                key={type?.id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {type?.type}
                </TableCell>
                <TableCell align="right">{type.vehicules?.length}</TableCell>
                <TableCell align="right">
                  <IconButton
                    color="error"
                    onClick={() => handelDeleteAction(type.id, type.type)}
                  >
                    <DeleteForeverIcon />
                  </IconButton>

                  <IconButton
                    color="warning"
                    onClick={() => handelUpdateAction(type?.id, type?.type)}
                  >
                    <EditIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}

            {
              loading ? <TableRow  >
                  <TableCell colSpan={3} >
                    <CircularProgress size={"12px"} />
                  </TableCell>
            </TableRow> :''
            }
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};
