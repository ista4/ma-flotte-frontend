import { PhotoCamera } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab'
import { Autocomplete, Box, FormControl, FormLabel, IconButton, TextField } from '@mui/material'
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';

import Select from '@mui/material/Select';
import { enqueueSnackbar } from 'notistack';
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { closeResponsiveModal } from '../../store/reducer/ResponsiveModalSlice';
import { addVehicules } from '../../store/reducer/vehicle/AddVehicleSlice';
import { addVehicleToState } from '../../store/reducer/vehicle/VehicleSlice';

import styles from './style.module.css';
const AddVehicle = () => {

  const dispatch = useDispatch();
  const {types} = useSelector(state => state.VehicleType)
  const {loading} = useSelector(state => state.AddVehicle)

  const [immatriculation,setImmatriculation] = useState('')
  const [nom,setNom] = useState('')
  const [pussanceFiscale,setPussanceFiscale] = useState('')
  const [typeVehiculeId,setTypeVehiculeId] = useState('')
  const [typeCarburant,setTypeCarburant] = useState('')
  const [dateAcquisition,setDateAcquisition] = useState('')
  const [dateDebutAssurance,setDateDebutAssurance] = useState('')
  const [coutAssurance,setCoutAssurance] = useState('')
  const [image,setImage] = useState()

  const [msgError,setMsgError] = useState({
    nom : false,
    pussanceFiscale : false,
    typeVehiculeId : false,
    dateAcquisition : false,
    coutAssurance : false,
    dateDebutAssurance : false,
    typeCarburant : false,
    immatriculation : false,
    image : false,
    error : false,
    msg:""
  })

  const handelSubmit = (e)=> {
    e.preventDefault();

    const data = {
      nom , immatriculation, pussanceFiscale , image, typeVehiculeId,
      dateAcquisition, coutAssurance, typeCarburant, dateDebutAssurance
    }


    if(!data.nom){
      setMsgError((prev) =>( {...prev,nom:true}))
     }else{
      setMsgError((prev) =>( {...prev,nom:false}))
     }
 
     if(!data.immatriculation){
      setMsgError((prev) =>( {...prev,immatriculation:true}))
     }else{
      setMsgError((prev) =>( {...prev,immatriculation:false}))
     }
     if(!data.pussanceFiscale){
       setMsgError((prev) =>( {...prev,pussanceFiscale:true}))
      }else{
       setMsgError((prev) =>( {...prev,pussanceFiscale:false}))
      }
     if(!data.typeVehiculeId){
       setMsgError((prev) =>( {...prev,typeVehiculeId:true}))
      }else{
       setMsgError((prev) =>( {...prev,typeVehiculeId:false}))
      }
     if(!data.dateAcquisition){
       setMsgError((prev) =>( {...prev,dateAcquisition:true}))
      }else{
       setMsgError((prev) =>( {...prev,dateAcquisition:false}))
      }
     if(!data.coutAssurance){
       setMsgError((prev) =>( {...prev,coutAssurance:true}))
      }else{
       setMsgError((prev) =>( {...prev,coutAssurance:false}))
      }
     if(!data.typeCarburant){
       setMsgError((prev) =>( {...prev,typeCarburant:true}))
      }else{
       setMsgError((prev) =>( {...prev,typeCarburant:false}))
      }
     if(!data.dateDebutAssurance){
       setMsgError((prev) =>( {...prev,dateDebutAssurance:true}))
      }else{
       setMsgError((prev) =>( {...prev,dateDebutAssurance:false}))
      }


      if(
        data.nom && data.immatriculation && data.pussanceFiscale && data.typeCarburant

        && data.typeVehiculeId && data.coutAssurance && data.dateAcquisition && data.dateDebutAssurance
      ){

        console.log(data);
       dispatch(addVehicules(data)).unwrap()
       .then((res)=>{ 
        console.log(res)
        dispatch(addVehicleToState(res.vehicule))
        dispatch(closeResponsiveModal())
        enqueueSnackbar('Vehicule ajouté avec succès', { variant: 'success' })
        })
       .catch((err)=>{
        enqueueSnackbar(err, { variant: 'error' })  
        console.log(err)
       });
      }
  }

  return (
    <Box className={styles.form_content}>
      <form method="POST" onSubmit={handelSubmit}>
        <Box className={styles.group_one}>
          <FormControl className={styles.image_box}>
            <IconButton
              color="success"
              aria-label="upload picture"
              component="label"
            >
              <input
                hidden
                accept="image/*"
                type="file"
                onChange={(e) => setImage(e.target.files[0])}
              />

              {image ? (
                <img
                  src={image ? URL.createObjectURL(image) : ""}
                  alt={"img"}
                />
              ) : (
                ""
              )}

              <PhotoCamera />
            </IconButton>
          </FormControl>

          <FormControl className={styles.formcontrol_one}>
            <TextField
              id="nom"
              error={msgError.nom}
              label="nom"
              onChange={(e) => setNom(e.target.value)}
              value={nom}
              autoComplete="on"
              variant="outlined"
              sx={{ width: "100%" }}
            />

            <TextField
              id="immatriculation"
              error={msgError.immatriculation}
              label="matricule"
              autoComplete="on"
              variant="outlined"
              onChange={(e) => setImmatriculation(e.target.value)}
              value={immatriculation}
              sx={{ width: "100%" }}
            />
          </FormControl>
        </Box>

        <Box className={styles.group_two}>
          
        <FormControl className={styles.formcontrol}>
        
        <InputLabel id="typeVehiculeId">Type Vehicule</InputLabel>
              <Select
          labelId="typeVehiculeId"
          id="typeVehiculeId"
          value={typeVehiculeId}
          label="Type Vehicule"
          error={msgError.typeVehiculeId}
          onChange={(e) =>{
            setTypeVehiculeId(e.target.value)
          }}
        >
          {types.map(e => <MenuItem key={e.id} value={e.id}>{e.type}</MenuItem>)}
        </Select>
        <FormControl>
          
        <InputLabel id="typeCarburant">Type Carburant</InputLabel>
                      <Select
                  labelId="typeCarburant"
                  id="typeCarburant"
                  value={typeCarburant}
                  label="Type Carburant"
                  error={msgError.typeCarburant}
                  onChange={(e) =>{
                    setTypeCarburant(e.target.value)
                  }}
                >
                  <MenuItem value={'diesel'}>Diesel</MenuItem>
                  <MenuItem value={'essence'}>Essence</MenuItem>
                </Select>
        </FormControl>

       
        </FormControl>

        <FormControl className={styles.formcontrol}>
          <TextField
            id="pussanceFiscale"
            error={msgError.pussanceFiscale}
            label="Pussance Fiscale"
            autoComplete="on"
            value={pussanceFiscale}
            onChange={(e) => setPussanceFiscale(e.target.value)}
            variant="outlined"
            sx={{ width: "100%" }}
          />

          <TextField
            id="coutAssurance"
            error={msgError.coutAssurance}
            label="Cout Assurance"
            autoComplete="on"
            value={coutAssurance}
            onChange={(e) => setCoutAssurance(e.target.value)}
            variant="outlined"
            sx={{ width: "100%" }}
          />
        </FormControl>
        </Box>

        <FormControl className={styles.formcontrol}>
          <FormLabel htmlFor="dateAcquisition">Date Acquisition</FormLabel>
          <TextField
            id="dateAcquisition"
            type="date"
            error={msgError.dateAcquisition}
            autoComplete="on"
            value={dateAcquisition}
            onChange={(e) => setDateAcquisition(e.target.value)}
            variant="outlined"
            sx={{ width: "100%" }}
          />
          <FormLabel htmlFor="dateDebutAssurance">
            Date Debut Assurance
          </FormLabel>
          <TextField
            id="dateDebutAssurance"
            type="date"
            error={msgError.dateDebutAssurance}
            autoComplete="on"
            value={dateDebutAssurance}
            onChange={(e) => setDateDebutAssurance(e.target.value)}
            variant="outlined"
            sx={{ width: "100%" }}
          />
        </FormControl>

        <LoadingButton
          type="submit"
          color='success'
          loading={loading}
          variant="contained"
          sx={{ width: "100%", gap: "16px", mt:'16px' }}
        >
          Ajouter
        </LoadingButton>
      </form>
    </Box>
  );
}



export default AddVehicle