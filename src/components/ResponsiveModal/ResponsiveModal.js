import React from "react";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";
import { useDispatch, useSelector } from "react-redux";
import { closeResponsiveModal } from "../../store/reducer/ResponsiveModalSlice";
import { IconButton, Slide } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import AddVehicle from "../AddVehicle/AddVehicle";
import { AddVehicleType } from "../AddVehicle/AddVehicleType";
import UpdateVehicleType from "../AddVehicle/UpdateVehicleType";
import DeleteVehicleType from "../AddVehicle/DeleteVehicleType";
import UserCart from "../UserCart/UserCart";
import GasStation from "../GasStation/GasStation";
import AddFuel from "../AddFuel/AddFuel";
import AddFuelManuel from "../AddFuel/AddFuelManuel";
import AddFuelByQrCode from "../AddFuel/AddFuelByQrCode";
import PrintVehicleQrCode from "../AddVehicle/PrintVehicleQrCode";
import AddScreen from "../../pages/user/dashboard/AddScreen";
import Mission from "../Mission/Mission";
import StartMission from "../Mission/StartMission";
import UpdateProfile from "../UpdateProfile/UpdateProfile";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const ResponsiveModal = () => {
  const { open, componentName, title, childrenProps, isFullScreen } = useSelector(
    (state) => state.respoiveModal
  );
  const dispatch = useDispatch();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("md"));
  const handleClose = () => {
    dispatch(closeResponsiveModal());
  };

  let rederedComponent;
  const components = {
    AddVehicle,
    AddVehicleType,
    UpdateVehicleType,
    DeleteVehicleType,
    UserCart,
    GasStation,
    AddFuel,
    AddFuelManuel,
    AddFuelByQrCode,
    PrintVehicleQrCode,
    AddScreen,
    Mission,
    StartMission,
    UpdateProfile
  };

  if (componentName) {
    const Component = components[componentName];
    rederedComponent = <Component {...childrenProps} />;
  }
  return (
    <Dialog
      fullScreen={isFullScreen ? isFullScreen : fullScreen}
      TransitionComponent={Transition}
      open={open}
      onClose={handleClose}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogTitle id="responsive-dialog-title">{title}</DialogTitle>

      <IconButton
        aria-label="close modal"
        color="success"
        sx={{ position: "absolute", right: "0", top: "0" }}
        onClick={handleClose}
      >
        <CloseIcon />
      </IconButton>

      <DialogContent
        sx={{
          marginBottom: "16px",
        }}
      >
        {rederedComponent}
      </DialogContent>
    </Dialog>
  );
};

export default ResponsiveModal;
