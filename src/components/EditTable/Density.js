import { GridToolbarContainer, GridToolbarDensitySelector } from '@mui/x-data-grid';
import React from 'react'

const Density = () => {
    return (
        <GridToolbarContainer >
          <GridToolbarDensitySelector sx={{color:'var(--primary)'}} />
        </GridToolbarContainer>
      );
}

export default Density