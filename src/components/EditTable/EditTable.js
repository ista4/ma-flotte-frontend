import * as React from 'react';
import { DataGrid, GridLoadingOverlay} from '@mui/x-data-grid';
import Density from './Density';
import { useDispatch, useSelector } from 'react-redux';
import { updateAdmin } from '../../store/reducer/sp-admin/admins/UpdateAdminSlice';
import { enqueueSnackbar } from 'notistack';
import {  LinearProgress } from '@mui/material';
import { updateUser } from '../../store/reducer/sp-admin/users/UpdateUserSlice';

export default function EditTable(
  {rows , columns ,updated, storeName , role , pageSize , pageSizeOptions , loading, fontSize }
  ) {
  
  const [selectedCellParams, setSelectedCellParams] = React.useState(null);
  const [cellModesModel, setCellModesModel] = React.useState({});
  const dispatch = useDispatch()

  const {loading:isLoading} = useSelector(state => state[storeName])

  // handle cell update
  const processRowUpdate = async (newRow , oldRow) => {

    const odlField = oldRow[selectedCellParams.field]
    const newField = newRow[selectedCellParams.field]
    if (odlField === newField) {
      return newRow;
    }

    const row = {
      id: newRow.id,
      field: selectedCellParams.field,
      value : odlField !== newField ? newField : '',
    }

    let valide = newRow

    if(role === 'SpAdmin' && updated === 'admin'){
      console.log(isLoading);
      await dispatch(updateAdmin(row )).unwrap()
      .then((res) => {
        console.log(res);
        enqueueSnackbar(row.field +' mis à jour avec succès', {variant :'success'});
        valide = newRow;
      })
      .catch((err) => {
        console.log(err);
         enqueueSnackbar(err, {variant :'error'});
        valide = oldRow
      });
    }

    if(role === 'SpAdmin' && updated === 'user'){

      await dispatch(updateUser(row )).unwrap()
      .then((res) => {
        console.log(res);
        enqueueSnackbar(row.field +' mis à jour avec succès', {variant :'success'});
        valide = newRow;
      })
      .catch((err) => {
        console.log(err);
         enqueueSnackbar(err, {variant :'error'});
        valide = oldRow
      });
    }


    //handle send data to api
    return valide
  };
  const handleCellFocus = React.useCallback((event) => {
    const value = event.target.value
    const row = event.currentTarget.parentElement;
    const id = row.dataset.id;
    const field = event.currentTarget.dataset.field;
    setSelectedCellParams({ id, field, value, role });

  }, [role]);

  const cellMode = React.useMemo(() => {
    if (!selectedCellParams) {
      return 'view';
    }
    const { id, field } = selectedCellParams;
    return cellModesModel[id]?.[field]?.mode || 'view';
  }, [cellModesModel, selectedCellParams]);

  const onRowEditStop = (params , event) =>{
      return params.field;
  }

  const handleCellKeyDown = React.useCallback(
    (params, event) => {

      if (cellMode === 'edit') {
        // Prevents calling event.preventDefault() if Tab is pressed on a cell in edit mode
        event.defaultMuiPrevented = true;
      }
    },
    [cellMode],
  );
  const getRowSpacing = React.useCallback((params) => {
    return {
      top: params.isFirstVisible ? 0 : 5,
      bottom: params.isLastVisible ? 0 : 5,
    };
  }, []);
  
  return (
    <div style={{ height: 460, width: '100%', marginTop:'16px' , background:'#fff', color:'black' }}>
    
      <DataGrid
        editMode='row'
        processRowUpdate={processRowUpdate}
        rows={rows}
     
        initialState={{
          pagination: { paginationModel: { pageSize: pageSize } },
          sorting: {
            sortModel: [{ field: 'created_at', sort: 'desc' }],
          },
        }}
        pageSizeOptions={pageSizeOptions}
        sx={{
          fontWeight: '600',
          fontSize: fontSize ? fontSize : '14px',
        }}
        getRowSpacing={getRowSpacing}
        
        columns={columns}
        onCellKeyDown={handleCellKeyDown}
        onRowEditStop={onRowEditStop}
        cellModesModel={cellModesModel}
        onCellModesModelChange={(model) => setCellModesModel(model)}
        loading={loading || isLoading}
        slots={{
          toolbar: Density,
          loadingOverlay: isLoading ?  LinearProgress :GridLoadingOverlay,
        }}
        slotProps={{
          toolbar: {
            cellMode,
            selectedCellParams,
            setSelectedCellParams,
            cellModesModel,
            setCellModesModel,
          },
          cell: {
            onFocus: handleCellFocus,
          },
        }}
        
      />

    </div>
  );
}



