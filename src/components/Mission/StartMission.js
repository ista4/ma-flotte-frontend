import { Box, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { MapContainer, TileLayer, useMap } from "react-leaflet";

import L from "leaflet";

import "leaflet-routing-machine"; // Don't forget to import the CSS
import "leaflet-routing-machine/dist/leaflet-routing-machine.js";
import "leaflet-routing-machine/dist/leaflet-routing-machine.css";

import "leaflet-control-geocoder/dist/Control.Geocoder.css";
import "leaflet-control-geocoder/dist/Control.Geocoder.js";
import { LoadingButton } from "@mui/lab";

import CancelIcon from '@mui/icons-material/Cancel';
import { completeMission } from "../../store/reducer/mission/CompleteMissionSlice";
import { useDispatch, useSelector } from "react-redux";
import { enqueueSnackbar } from "notistack";
import { closeResponsiveModal } from "../../store/reducer/ResponsiveModalSlice";
import { getUser } from "../../store/reducer/user/UserSclice";

let markerOne;

let carIcon = L.icon({
  iconUrl: "/car-marker.svg",
  iconSize: [38, 38],
  iconAnchor: [12, 25],
  popupAnchor: [0, -25],

});


const RoutingMachine = ({position}) => {

  const [routes, setRoutes] = useState([]); 

  const userLocation = JSON.parse(sessionStorage.getItem("order")).latLng; 

  const map = useMap();

  function createButton(label, container) {
    var btn = L.DomUtil.create("button", "", container);
    btn.setAttribute("type", "button");
    btn.innerHTML = label;
    return btn;
  }

  useEffect(() => {

    map.setView([userLocation.lat, userLocation.lng], 13);

   if(routes){
    markerOne = L.marker([userLocation.lat, userLocation.lng], { icon: defaultMarker }).addTo(map);
    markerOne.bindPopup("Départ").openPopup();
    markerOne.setZIndexOffset(1000);
   }

    let control;
  
      control = L.Routing.control({
        waypoints: [
          L.latLng(userLocation.lat, userLocation.lng),
        ],
        
        lineOptions: {
          styles: [{ color: "blue", opacity: 1, weight: 3 }],
        },
        language: "fr",
        location: "ma",
        countrycodes : "ma",
        //nominatim({ geocodingQueryParams: { countrycodes: "ma" } }),
        geocoder: L.Control.Geocoder.photon("https://photon.komoot.de/" , {countrycodes: "ma" }),
        routeWhileDragging: true,
        showAlternatives: true,
        fitSelectedRoutes: true,
  
        altLineOptions: {
          styles: [
            { color: "black", opacity: 0.15, weight: 9 },
            { color: "white", opacity: 0.8, weight: 6 },
            { color: "blue", opacity: 0.5, weight: 2 },
          ],
        },
      })
      .on("routesfound", function (e) {
          var routes = e.routes;
          // var summary = routes[0].summary;
          setRoutes(routes[0]);
      })
      .addTo(map);

  
    map.on("click", function (e) {
      const container = L.DomUtil.create("div"),
        startBtn = createButton("Commencer à cet endroit", container),
        destBtn = createButton("aller à cet endroit", container);

      L.popup().setContent(container).setLatLng(e.latlng).openOn(map);

      L.DomEvent.on(startBtn, "click", function () {
        control.spliceWaypoints(0, 1, e.latlng);
        map.closePopup();
      });

      L.DomEvent.on(destBtn, "click", function () {
        control.spliceWaypoints(control.getWaypoints().length - 1, 1, e.latlng);
        map.closePopup();
      });
    });

  }, [map, userLocation.lat, userLocation.lng]);

  return null;
};

const StartMission = ({missionID}) => {

  const [position, setPosition] = useState({
    heading: null,
    altitude: null,
    latitude: null,
    longitude: null,
    accuracy: null,
    altitudeAccuracy: null,
    speed: null,
  });

  const dispatch = useDispatch();
  const {loading} = useSelector((state) => state.CompleteMission);
  // stop mission
  const handelMissionStop = () => {
   dispatch(completeMission(missionID)).unwrap()
   .then((res) => {
     enqueueSnackbar("Mission Terminer avec succés", { variant: "success" })
     dispatch(closeResponsiveModal())
   })
   .catch((err) => {
     console.log(err);
      enqueueSnackbar("Erreur lors de la terminaison de la mission", { variant: "error" })
   });
  };
  
  window.onbeforeunload = function (event) {
    
    const message = "Voulez vous vraiment quitter la page";
    if (typeof event == "undefined") {
      event = window.event;
    }
    if (event) {
      event.returnValue = message;
    }

    // On Accepete 
    window.onunload = function () {
      dispatch(completeMission(missionID)).unwrap()
      .then((res) => {
        console.log(res);
        enqueueSnackbar("Mission Terminer avec succés", { variant: "success" })
        dispatch(closeResponsiveModal())
      })
      .catch((err) => {
        console.log(err);
          enqueueSnackbar("Erreur lors de la terminaison de la mission", { variant: "error" })
      });
    };
    return message;
  };

  useEffect(() => {

    const onSuccess = (potition) => {
      
      setPosition({
        heading: potition.coords.heading,
        altitude: potition.coords.altitude,
        latitude: potition.coords.latitude,
        longitude: potition.coords.longitude,
        accuracy: potition.coords.accuracy,
        altitudeAccuracy: potition.coords.altitudeAccuracy,
        speed: potition.coords.speed,
      });

      if(markerOne){
        markerOne.setLatLng([potition.coords.latitude, potition.coords.longitude]);
        markerOne.setIcon(carIcon);
        markerOne.setZIndexOffset(1000);
        markerOne.bindPopup("Ma location" ).openPopup();
      }
    };

    const onError = (err) => {
      console.log(err);
    };

    const options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0,
    };

    const watchId = navigator.geolocation.watchPosition(
      onSuccess,
      onError,
      options
    );

    return () => {
      navigator.geolocation.clearWatch(watchId);
      console.log("stop");
      dispatch(getUser())
      window.onbeforeunload = null;
    };
  }, [dispatch]);

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",

        height: "100%",
        width: "100%",
      }}
    >
     <Typography variant="body2" color="text.secondary">
          Votre Mission est en cours ,
          Pour Terminer la mission cliquer sur le bouton terminer la mission 
       </Typography>


      <MapContainer id="map"
      style={{ height: "600px", width: "100%" }}
       zoom={13} scrollWheelZoom={true}   >
        <TileLayer
          attribution="Ma Flotte"
          url="https://tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <RoutingMachine position={position} />
      </MapContainer>

      <LoadingButton
          fullWidth
          variant="contained"
          color="error"
          size="large"
          loading={loading}
          onClick={handelMissionStop}
          endIcon={<CancelIcon />}
          sx={{ mt: 1, mb: 2 }}
        >
          Terminer La Mission
        </LoadingButton>
    </Box>
  );
};



// change the default marker icon
const defaultMarker = L.icon({
  iconUrl: "/marker.svg",
  iconSize: [36, 36],
  iconAnchor: [12, 25],
  popupAnchor: [0, -25],
});

L.Marker.prototype.options.icon = defaultMarker;
export default StartMission;
