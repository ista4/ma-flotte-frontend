import {
  Box,
  CircularProgress,
  FormControl,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import styles from "./style.module.css";

import { useDispatch, useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import SpeedIcon from "@mui/icons-material/Speed";
import InfoIcon from "@mui/icons-material/Info";
import StartIcon from '@mui/icons-material/Start';
import PlaceIcon from "@mui/icons-material/Place";

import { LoadingButton } from "@mui/lab";
import { Html5QrcodeScanner } from "html5-qrcode";
import { getVehiculeById } from "../../store/reducer/vehicle/VehicleSlice";
import { enqueueSnackbar } from "notistack";
import { closeResponsiveModal, openResponsiveModal } from "../../store/reducer/ResponsiveModalSlice";
import { GroupAdd, Restore } from "@mui/icons-material";
import { addMission } from "../../store/reducer/mission/AddMissionSlice";

const Mission = () => {
  const [vehicule, setVehicule] = useState({});
  const [latLng, setLatLng] = useState({ lat: null, lng: null});
  const [objet, setObjet] = useState("");
  const [lieu, setLieu] = useState("");
  const [dateRetour, setDateRetour] = useState("");
  const [nombrePersonne, setNombrePersonne] = useState(0);

  const dispatch = useDispatch();
  const { loading } = useSelector((state) => state.Vehicle);
  const { loading: loadingMission } = useSelector((state) => state.AddMission);

  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
      setLatLng({
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      });
    });

    function onScanSuccess(decodedText, decodedResult) {
      // handle the scanned code as you like, for example:
      const data = JSON.parse(decodedText);

      enqueueSnackbar("Scanné avec succès ", { variant: "success" });
      dispatch(getVehiculeById(data.immatriculation))
        .unwrap()
        .then((result) => {
          setVehicule(result.vehicule);
        })
        .catch((error) => {
          enqueueSnackbar("Vehicule non trouver", { variant: "error" });
          dispatch(closeResponsiveModal());
        });

      html5QrcodeScanner.clear();
    }

    function onScanFailure(error) {
      // handle scan failure, usually better to ignore and keep scanning.
      // for example:
      console.warn(`Code scan error = ${error}`);
    }

    let html5QrcodeScanner = new Html5QrcodeScanner(
      "reader",
      { fps: 12, qrbox: { width: 250, height: 250 } },
      /* verbose= */ false
    );

    html5QrcodeScanner.render(onScanSuccess, onScanFailure);

    return () => {
      html5QrcodeScanner.clear();
      console.log(html5QrcodeScanner.getState());
    };
  }, [dispatch]);

  const [msgError, setMsgError] = useState({
    vehicule: false,
    dateRetour: false,
    nombrePersonne: false,
    lieu: false,
    objet: false,
    error: false,
    msg: "",
  });

  const handelSubmit = (e) => {
    e.preventDefault();
    const data = {
      vehicule: vehicule.id,
      latLng,
      objet,
      lieu,
      dateRetour,
      nombrePersonne,
    };

    if (!data.vehicule_id) {
      setMsgError((prev) => ({ ...prev, vehicule: true }));
    } else {
      setMsgError((prev) => ({ ...prev, vehicule: false }));
    }
    if(!data.dateRetour){
      setMsgError((prev) => ({ ...prev, dateRetour: true }));
    }else{
      setMsgError((prev) => ({ ...prev, dateRetour: false }));
    }
    if(!data.nombrePersonne){
      setMsgError((prev) => ({ ...prev, nombrePersonne: true }));
    }else{
      setMsgError((prev) => ({ ...prev, nombrePersonne: false }));
    }
    if(!data.lieu){
      setMsgError((prev) => ({ ...prev, lieu: true }));
    }else{
      setMsgError((prev) => ({ ...prev, lieu: false }));
    }
    if(!data.objet){
      setMsgError((prev) => ({ ...prev, objet: true }));
    }else{
      setMsgError((prev) => ({ ...prev, objet: false }));
    }

    if (data.vehicule && data.dateRetour && 
      data.nombrePersonne && data.lieu && 
      data.objet) {

     if(data.latLng.lat && data.latLng.lng){
      sessionStorage.setItem("order", JSON.stringify(data));
      // send data to server
      dispatch(addMission(data)).unwrap()
      .then((result) => { 
        console.log(result);
        enqueueSnackbar("Mission ajouté avec succès", { variant: "success" });
        dispatch(openResponsiveModal({componentName : 'StartMission', 
        title : "Mission Commencée" , isFullScreen : true,  childrenProps : {missionID : result.id,}}))
        }).catch((error) => {
          console.log(error);
          enqueueSnackbar("Erreur lors de l'ajout de la mission", { variant: "error" });
        });
     }else {
      enqueueSnackbar("Veuillez activer votre GPS", { variant: "error" });
     }
      
    }
  };
  return (
    <Box className={styles.fuel_container}>
      <form onSubmit={handelSubmit}>
        {loading ? (
          <Box
            className={styles.vehicule_box}
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              padding: "12px",
            }}
          >
            <Box className={styles.vehicule_box_info}>
              <CircularProgress color="success" />
            </Box>
          </Box>
        ) : vehicule.nom ? (
          <Box className={styles.vehicule_box}>
            <Box className={styles.vehicule_box_info}>
              <Typography variant="h6" component="h6">
                {vehicule.nom}
              </Typography>
              <Typography variant="h6" component="h6">
                {vehicule.immatriculation}
              </Typography>
            </Box>

            <Typography
              variant="h6"
              component="h6"
              sx={{
                display: "flex",
                alignItems: "center",
                gap: "8px",
              }}
            >
              <SpeedIcon /> {vehicule.kilometrage}
            </Typography>
          </Box>
        ) : null}
        <div
          id="reader"
          width="600px"
          style={{ display: vehicule.nom ? "none" : "block" }}
        ></div>

        <FormControl fullWidth className={styles.form_control}>
          <TextField
            id="objet"
            label="Objet"
            maxRows={3}
            error={msgError.objet}
            value={objet}
            onChange={(e) => setObjet(e.target.value)}
            multiline={true}
            variant="outlined"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <InfoIcon />
                </InputAdornment>
              ),
              inputProps: {
                maxLength: 152,
                },
            }}
          />
        </FormControl>

        <FormControl fullWidth className={styles.form_control}>
          <TextField
            id="lieu"
            label="Lieu"
            value={lieu}
            onChange={(e) => setLieu(e.target.value)}
            type="text"
            variant="outlined"
            error={msgError.lieu}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <PlaceIcon />
                </InputAdornment>
              ),
            }}
          />
        </FormControl>
        <Box
          className={styles.fuel_data}
          sx={{
            width: "100%",
            display: "flex",
            gap: "16px",
          }}
        ></Box>

        <Box
          className={styles.fuel_data}
          sx={{
            width: "100%",
            display: "flex",
            gap: "16px",
          }}
        >
          <FormControl 
            sx={{width: "51%"}}
           className={styles.form_control}>
            <TextField
              id="Nombre Personnes"
              type="number"
              value={nombrePersonne}
              
              onChange={(e) => setNombrePersonne(e.target.value)}
              label="N° Personnes"
              variant="outlined"
              error={msgError.nombrePersonne}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <GroupAdd />
                  </InputAdornment>
                ),
                inputProps: { min: 0 , max: 50},
              }}
            />
          </FormControl>

          <FormControl fullWidth className={styles.form_control}>
            <TextField
              id="dateRetour"
              label="Date Retour"
              error={msgError.dateRetour}
              variant="outlined"
              type="datetime-local"
              value={dateRetour}
              onChange={(e) => setDateRetour(e.target.value)}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Restore />
                  </InputAdornment>
                ),
              
              }}
            />
          </FormControl>
        </Box>

        <LoadingButton
          fullWidth
          type="submit"
          variant="contained"
          color="success"
          endIcon={<StartIcon />}
          loading={loadingMission}
          size="large"  
        >
          Commencer La Mission
        </LoadingButton>
      </form>
    </Box>
  );
};

export default Mission;
