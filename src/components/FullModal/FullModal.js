import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Slide from "@mui/material/Slide";
import { useDispatch, useSelector } from "react-redux";
import SaveIcon from "@mui/icons-material/Save";
import {
  closeFullModal,
  openFullModal,
} from "../../store/reducer/FullModalSlice";
import { Box } from "@mui/system";
import AssignmentIcon from "@mui/icons-material/Assignment";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/effect-flip";

import styles from "./style.module.css";
// import required modules
import { EffectFlip } from "swiper";
import { forwardRef, useCallback, useEffect } from "react";
import {
  Divider,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import { useState } from "react";
import { PhotoCamera } from "@mui/icons-material";
import { updateVehicule } from "../../store/reducer/vehicle/UpdateVehicleSlice";

import { enqueueSnackbar } from "notistack";
import {  updateVehicleFromState } from "../../store/reducer/vehicle/VehicleSlice";
import { getVehiculesType } from "../../store/reducer/vehicle/VehicleTypeSlice";
import { updateVehicleFromFuelState } from "../../store/reducer/Fuel/FuelSlice";
import { LoadingButton } from "@mui/lab";

// transition
const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});


export default function FullModal() {
  const { open, vehicule, role } = useSelector((state) => state.fullModal);

  const { types } = useSelector((state) => state.VehicleType);
  const { loading } = useSelector((state) => state.UpdateVehicle);

  const dispatch = useDispatch();

  const getTypesIfChanged = useCallback(() => {
    if (open && types.length === 0) {
      dispatch(getVehiculesType());
    }
  }, [dispatch, open, types]);

  useEffect(() => {
    setNom(vehicule.nom);
    setImmatriculation(vehicule.immatriculation);
    setPussanceFiscale(vehicule.pussance_fiscale);
    setTypeVehiculeId(vehicule.type?.id);
    setTypeCarburant(vehicule.type_carburant);
    setDateAcquisition(vehicule.date_acquisition);
    setDateDebutAssurance(vehicule.date_debut_assurance);
    setCoutAssurance(vehicule.cout_assurance);
    getTypesIfChanged();
  }, [vehicule, getTypesIfChanged]);

  const [immatriculation, setImmatriculation] = useState(
    vehicule.immatriculation
  );
  const [nom, setNom] = useState("");
  const [pussanceFiscale, setPussanceFiscale] = useState(
    vehicule.pussance_fiscale
  );
  const [typeVehiculeId, setTypeVehiculeId] = useState(vehicule.type?.id);
  const [typeCarburant, setTypeCarburant] = useState(vehicule.type_carburant);
  const [dateAcquisition, setDateAcquisition] = useState(
    vehicule.date_acquisition
  );
  const [dateDebutAssurance, setDateDebutAssurance] = useState(
    vehicule.date_debut_assurance
  );
  const [coutAssurance, setCoutAssurance] = useState(vehicule.cout_assurance);

  const [image, setImage] = useState(null);

  const handleClose = () => {
    dispatch(closeFullModal());
  };

  const handelSubmit = (e) => {
    e.preventDefault();

    let data = {};

    if (nom !== vehicule.nom) {
      data = { ...data, nom };
    }
    if (immatriculation !== vehicule.immatriculation) {
      data = { ...data, immatriculation };
    }
    if (pussanceFiscale !== vehicule.pussance_fiscale) {
      data = { ...data, pussance_fiscale: pussanceFiscale };
    }
    if (typeVehiculeId !== vehicule.type?.id) {
      data = { ...data, type_vehicule_id: typeVehiculeId };
    }
    if (typeCarburant !== vehicule.type_carburant) {
      data = { ...data, type_carburant: typeCarburant };
    }
    if (dateAcquisition !== vehicule.date_acquisition) {
      data = { ...data, date_acquisition: dateAcquisition };
    }
    if (dateDebutAssurance !== vehicule.date_debut_assurance) {
      data = { ...data, date_debut_assurance: dateDebutAssurance };
    }
    if (coutAssurance !== vehicule.cout_assurance) {
      data = { ...data, cout_assurance: coutAssurance };
    }

    if (image) {
      data = { ...data, image };
    }
    if (Object.keys(data).length) {
      dispatch(updateVehicule({ id: vehicule.id, data }))
        .unwrap()
        .then((res) => {
          console.log(res);
          dispatch(openFullModal({
            vehicule: res.vehicule,
            role: 'SpAdmin',
          }))
      
          setImage(null);
          dispatch(updateVehicleFromState(res.vehicule));
          dispatch(updateVehicleFromFuelState(res.vehicule));
          // dispatch(updateVehiculeFullMoadal(res.vehicule));
          
          enqueueSnackbar("Vehicule modifié avec succès", {
            variant: "success",
          });
        })
        .catch((err) => {
          enqueueSnackbar(err.response.data.message, { variant: "error" });
        });
    }
  };

  return (
    <Dialog
      fullScreen
      open={open}
      onClose={handleClose}
      TransitionComponent={Transition}
    >
      <AppBar sx={{ position: "relative", background: "var(--primary)" }}>
        <Toolbar>
          <IconButton
            edge="start"
            color="inherit"
            onClick={handleClose}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>

          <Box
            sx={{
              display: "flex",
              gap: "6px",
              width: "100%",
              justifyContent: "end",
            }}
          >
            <Button
              autoFocus
              type="submit"
              disabled={loading}
              variant="contained"
              endIcon={<AssignmentIcon />}
              onClick={handelSubmit}
            >
              Rapport
            </Button>

            {
              role === "SpAdmin" ? (
                <LoadingButton
              autoFocus
              type="submit"
              variant="contained"
              loading={loading}
              color="success"
              endIcon={<SaveIcon />}
              onClick={handelSubmit}
            >
              enregistrer
            </LoadingButton>
              ) : null
            }
          </Box>
        </Toolbar>
      </AppBar>

      <Box className={styles.content}>
        <Box className={styles.vehicule_info}>
          <Swiper
            effect={"flip"}
            grabCursor={true}
            loop={true}
            modules={[EffectFlip]}
            slidesPerView={1}
            className={styles.mySwiper}
          >
            <SwiperSlide className={styles.swiper_slide}>
              <img
                src={image ? URL.createObjectURL(image) : vehicule.image}
                alt={vehicule.nom}
              />
              <input
                type="file"
                className={styles.image_input}
                accept="image/*"
                onChange={(e) => setImage(e.target.files[0])}
              />

              <PhotoCamera className={styles.image_icon} color="success" />
            </SwiperSlide>

            <SwiperSlide className={styles.swiper_slide}>
              <img
                src={vehicule.qr_code}
                style={{ width: "200px" }}
                alt={vehicule.nom}
              />
            </SwiperSlide>
          </Swiper>

          <Box className={styles.vehicule_data}>
            <form onSubmit={handelSubmit}>
              <FormControl className={styles.form_control} required>
                <TextField
                  id="nom"
                  value={nom ?? ""}
                  label="nom"
                  onChange={(e) => setNom(e.target.value)}
                  variant="filled"
                />

                <TextField
                  id="immatriculation"
                  value={immatriculation ?? ""}
                  label="Matricule"
                  onChange={(e) => setImmatriculation(e.target.value)}
                  variant="filled"
                />
              </FormControl>

              <FormControl className={styles.form_control}>
                <TextField
                  id="cout assurance"
                  value={coutAssurance ?? ""}
                  label="coutAssurance"
                  onChange={(e) => setCoutAssurance(e.target.value)}
                  variant="filled"
                />

                <TextField
                  id="pussanceFiscale"
                  value={pussanceFiscale ?? ""}
                  label="pussance fiscale"
                  onChange={(e) => setPussanceFiscale(e.target.value)}
                  variant="filled"
                />
              </FormControl>

              <FormControl className={styles.form_control}>
                <FormControl className={styles.select}>
                  <InputLabel id="typeCarburant">Type Carburant</InputLabel>
                  <Select
                    labelId="typeCarburant"
                    id="typeCarburant"
                    value={typeCarburant ?? ""}
                    label="Type Carburant"
                    variant="filled"
                    fullWidth
                    onChange={(e) => {
                      setTypeCarburant(e.target.value);
                    }}
                  >
                    <MenuItem value={"diesel"}>Diesel</MenuItem>
                    <MenuItem value={"essence"}>Essence</MenuItem>
                  </Select>
                </FormControl>

                <FormControl className={styles.select}>
                  <InputLabel id="typeVehiculeId">Type typeVehicule</InputLabel>
                  <Select
                    labelId="typeVehiculeId"
                    id="typeVehiculeId"
                    fullWidth
                    className={styles.select}
                    value={typeVehiculeId ?? ""}
                    label="Type Carburant"
                    variant="filled"
                    onChange={(e) => {
                      setTypeVehiculeId(e.target.value);
                    }}
                  >
                    {types.map((typeVehicule) => (
                      <MenuItem key={typeVehicule?.id} value={typeVehicule?.id}>
                        {" "}
                        {typeVehicule?.type}{" "}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </FormControl>

              <FormControl className={styles.form_control}>
                <TextField
                  id="dateAcquisition"
                  value={dateAcquisition ?? ""}
                  label="date acquisition"
                  sx={{ width: "226px" }}
                  type="date"
                  onChange={(e) => setDateAcquisition(e.target.value)}
                  variant="filled"
                />

                <TextField
                  id="dateDebutAssurance"
                  value={dateDebutAssurance ?? ""}
                  label="date debut assurance"
                  type="date"
                  sx={{ width: "226px" }}
                  onChange={(e) => setDateDebutAssurance(e.target.value)}
                  variant="filled"
                />
              </FormControl>
            </form>
          </Box>
        </Box>

        <Divider />
      </Box>
    </Dialog>
  );
}
