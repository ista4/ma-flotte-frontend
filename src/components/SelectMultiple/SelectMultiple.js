import * as React from "react";
import Chip from "@mui/material/Chip";
import Autocomplete from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";

export default function SelectMultiple({ data, viewField, label, getData }) {
  const [value, setValue] = React.useState([]);

  return (

      <Autocomplete
        sx={{ width: "100%", minWidth: "300px" }}
        multiple
        id="tags-filled"
        isOptionEqualToValue={(option, value) =>
            value === undefined || value === "" || option.id === value.id
          }
     
        options={data}
        value={value}

        getOptionLabel={(item) =>
          (item[viewField[0]] ?  item[viewField[0]] :" ") + " " + 
          (item[viewField[1]] ?  item[viewField[1]] :" ") + " " + 
          (item[viewField[2]] ? " | " + item[viewField[2]] :" ")
        }
        
        onChange={(e, val) => {
          setValue(val);
          getData(val);
        }}
        renderTags={(val, getTagProps) => {
          return val.map((option, index) => (
            <Chip
              variant="outlined"
              sx={{ color: "#1c1c1c" }}
              label={option[viewField[0]] + " " + 
              (option[viewField[1]] ?  option[viewField[1]] :" ") }
              {...getTagProps({ index })}
            />
          ));
        }}
        renderInput={(params) => (
          <TextField {...params} variant="filled" label={label} />
        )}
      />

  );
}
