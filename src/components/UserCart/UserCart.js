import { Avatar, Box, Typography } from '@mui/material'
import React from 'react'
import styles from './style.module.css';
const UserCart = ({image, nom , prenom , cin , email, tel , role}) => {


  return (
    <Box 
    className= {styles.profile_container}
    >
      <Box className={styles.profile_info}>
        
          <Avatar alt={nom} src={image} sx={{ width: 82, height: 82 }} />

        <Box className={styles.profile_name}>
          <Typography variant="h6"  >
            {nom}  {prenom} 
          </Typography>
        </Box>
        
      </Box>

      <Box className={styles.profile_details}>
       
        <Box className={styles.profile_details_item}>
       
            <Typography>CIN </Typography> 
            <Typography>{cin}</Typography>
        
        </Box>

        <Box className={styles.profile_details_item}>

              <Typography>Role </Typography>
              <Typography>{role}</Typography>
        </Box>

        <Box className={styles.profile_details_item}>

             <Typography>Email </Typography>
              <Typography>{email}</Typography>

        </Box>


        <Box className={styles.profile_details_item}>
          <Typography >Téléphone </Typography>
          <Typography>{tel}</Typography>
        </Box>
      </Box>

    
    </Box>
  )
}

export default UserCart