import {
  Autocomplete,
  Box,
  Chip,
  Divider,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import styles from "./style.module.css";
import { useDispatch, useSelector } from "react-redux";
import { getVehicules } from "../../store/reducer/vehicle/VehicleSlice";
import SpeedIcon from "@mui/icons-material/Speed";
import LocalGasStationIcon from "@mui/icons-material/LocalGasStation";
import PointOfSaleIcon from "@mui/icons-material/PointOfSale";
import { PhotoCamera } from "@mui/icons-material";

import StationMap from "./StationMap";
import { LoadingButton } from "@mui/lab";
import { addFuel } from "../../store/reducer/Fuel/AddFuelSlice";
import { addFuelToState, getFuel } from "../../store/reducer/Fuel/FuelSlice";
import { enqueueSnackbar } from "notistack";
import { closeResponsiveModal } from "../../store/reducer/ResponsiveModalSlice";
import { getUser } from "../../store/reducer/user/UserSclice";
import { getFuelStatistiques } from "../../store/reducer/Fuel/FuelStatistiquesSlice";

const AddFuelManuel = () => {
  const {vehicules } = useSelector((state) => state.Vehicle);
  const {loading} = useSelector((state) => state.AddFuel);
  const [kilometrage, setKilometrage] = useState("");
  const [prix, setPrix] = useState("");
  const [quantite, setQuantite] = useState("");
  const [vehicule, setVehicule] = useState("");
  const [type, setType] = useState("");
  const [image, setImage] = useState();
  const [nom, setNom] = useState("");
  const [latLng, setLatLng] = useState({
    lat: null,
    lng: null,
  });

  const [msgError, setMsgError] = useState({
    nom: false,
    kilometrage: false,
    prix: false,
    quantite: false,
    vehicule: false,
    type: false,
    image: false,
    error: false,
    msg: "",
  });

  const dispatch = useDispatch();
  useEffect(() => {
    if (!vehicules.length) dispatch(getVehicules());

    return () => {
      if(localStorage.user_token){
        dispatch(getUser())
      }
    };
  }, [dispatch, vehicules]);

  const handelSubmit = (e) => {
    e.preventDefault();

    const data = {
      kilometrage,
      prix,
      quantite,
      vehicule,
      type,
      image,
      nom,
      latLng,
    };

    if(!data.nom){
      setMsgError((prev) =>( {...prev,nom:true}))
    }else{
      setMsgError((prev) =>( {...prev,nom:false}))
    }
    if(!data.kilometrage){
      setMsgError((prev) =>( {...prev,kilometrage:true}))
    }else{
      setMsgError((prev) =>( {...prev,kilometrage:false}))
    }
    if(!data.prix){
      setMsgError((prev) =>( {...prev,prix:true}))
    }else{
      setMsgError((prev) =>( {...prev,prix:false}))
    }
    if(!data.quantite){
      setMsgError((prev) =>( {...prev,quantite:true}))
    }else{
      setMsgError((prev) =>( {...prev,quantite:false}))
    }
    if(!data.vehicule){
      setMsgError((prev) =>( {...prev,vehicule:true}))
    }else{
      setMsgError((prev) =>( {...prev,vehicule:false}))
    }
    if(!data.type){
      setMsgError((prev) =>( {...prev,type:true}))
    }else{
      setMsgError((prev) =>( {...prev,type:false}))
    }
    if(!data.image){
      setMsgError((prev) =>( {...prev,image:true}))
    }else{
      setMsgError((prev) =>( {...prev,image:false}))
    }
    if(!data.latLng.lat){
      setMsgError((prev) =>( {...prev,error:true,msg:"Veuillez choisir une station"}))
    }else{
      setMsgError((prev) =>( {...prev,error:false,msg:""}))
    }


    if(data.nom && data.kilometrage && data.prix && data.quantite && data.vehicule && data.type && data.image && data.latLng.lat){

      dispatch(addFuel(data)).unwrap().then((res) => {
        enqueueSnackbar("Carburant ajouté avec succès", { variant: "success" });
        dispatch(addFuelToState(res.carburant))
        dispatch(getFuelStatistiques())
        dispatch(closeResponsiveModal())
      }
      ).catch((err) => {
        console.log(err);
        enqueueSnackbar(err.response.data.message, { variant: "error" });
      });
    }

  };
  
  return (
    <Box className={styles.fuel_container}>
      <form onSubmit={handelSubmit}>
        <FormControl fullWidth className={styles.form_control}>
          <Autocomplete
            id="vehicule_id"
            options={vehicules.map((v) => v)}
            getOptionLabel={(item) =>
              item.nom +
              " - " +
              item.immatriculation +
              " | " +
              item.kilometrage +
              " Km"
            }
            onChange={(e, value) => {
              setVehicule(value.id);
            }}
            renderInput={(params) => {
              return (
                <TextField
                  {...params}
                  label="Vehicule"
                  error={msgError.vehicule}
                  placeholder="Vehicule"
                />
              );
            }}
          />
        </FormControl>

        <FormControl fullWidth className={styles.form_control}>
          <TextField
            id="Kilometrage"
            label="Kilometrage"
            variant="outlined"
            error={msgError.kilometrage}
            value={kilometrage}
            onChange={(e) => {
              setKilometrage(e.target.value);
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SpeedIcon />
                </InputAdornment>
              ),
            }}
          />
        </FormControl>

        <Box
          className={styles.fuel_data}
          sx={{
            width: "100%",
            display: "flex",
            gap: "16px",
          }}
        >
          <FormControl fullWidth className={styles.form_control}>
            <TextField
              id="quantite"
              value={quantite ?? ""}
              error={msgError.quantite}
              type="number"
              onChange={(e) => {
                setQuantite(e.target.value);
              }}
              label="Quantite - (L)"
              variant="outlined"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <LocalGasStationIcon />
                  </InputAdornment>
                ),
              }}
            />
          </FormControl>

          <FormControl fullWidth className={styles.form_control}>
            <InputLabel id="typeCarburant">Type</InputLabel>
            <Select
              labelId="typeCarburant"
              error={msgError.type}
              id="typeCarburant"
              value={type}
              label="Type"
              onChange={(e) => {
                setType(e.target.value);
              }}
            >
              <MenuItem value={"diesel"}>Diesel</MenuItem>
              <MenuItem value={"essence"}>Essence</MenuItem>
            </Select>
          </FormControl>
        </Box>

        <Box
          className={styles.fuel_data}
          sx={{
            width: "100%",
            display: "flex",
            gap: "16px",
          }}
        >
          <FormControl fullWidth className={styles.form_control}>
            <TextField
              id="Prix"
              label="Prix / L"
              variant="outlined"
              error={msgError.prix}
              type="number"
              value={prix ?? ""}
              onChange={(e) => {
                setPrix(e.target.value);
              }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <PointOfSaleIcon />
                  </InputAdornment>
                ),
              }}
            />
          </FormControl>

          <FormControl fullWidth className={styles.form_control}>
            <TextField
              id="cout"
              label="Coût"
              value={
                prix && quantite ? Number(prix * quantite).toFixed(3) : "0"
              }
              variant="outlined"
              readOnly={true}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <PointOfSaleIcon />
                  </InputAdornment>
                ),
              }}
            />
          </FormControl>
        </Box>

        <Box sx={{ width: "100%", mb: "16px" }}>
          <Divider>
            <Chip label="Station Service" />
          </Divider>
        </Box>

        <FormControl fullWidth className={styles.form_control}>
          <TextField
            id="nom"
            label="Nom de station"
            variant="outlined"
            error={msgError.nom}
            value={nom}
            onChange={(e) => setNom(e.target.value)}
          />
        </FormControl>

        <Box
          sx={{
            width: "100%",
            display: "flex",
            gap: "45px",
            alignItems: "center",
          }}
        >
          <FormControl className={styles.form_control}>
            <Box className={styles.image_box_station}>
              <IconButton
                color="success"
                aria-label="upload recu image"
                component="label"
              >
                <input
                  hidden
                  accept="image/*"
                  title="Recu image"
                  type="file"
                  onChange={(e) => setImage(e.target.files[0])}
                />

                {image ? (
                  <img
                    src={image ? URL.createObjectURL(image) : ""}
                    alt={"Recu de carburant"}
                  />
                ) : (
                  ""
                )}

                <PhotoCamera />
              </IconButton>
            </Box>
          </FormControl>

          <Typography variant="p" color="text.secondary" style={{ color :(  !image ? '#b73030' : "") }} >
            Enter image de recu de carburant(Image)
          </Typography>
        </Box>

        <Box className={styles.station_gps}>
          <Typography variant="h6" style={{ color :(  !latLng ? '#b73030' : "") }} >
            Sélectionner la station de service
          </Typography>

          <StationMap setLatLng={setLatLng} />
        </Box>
        <LoadingButton
          fullWidth
          type="submit"
          variant="contained"
          color="success"
          loading={loading}
        >
          Ajouter
        </LoadingButton>
      </form>
    </Box>
  );
};

export default AddFuelManuel;
