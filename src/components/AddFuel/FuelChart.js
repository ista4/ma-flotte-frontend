import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  BarChart,
  Bar,
  XAxis,

  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer
} from "recharts";
import { getFuelStatistiques } from "../../store/reducer/Fuel/FuelStatistiquesSlice";
import { Box } from "@mui/material";



const FuelChart = ({loading}) => {

  const dispatch = useDispatch();
  const {statistiques} = useSelector(state => state.FuelStatistiques);

  useEffect(() => {
    dispatch(getFuelStatistiques())
  }, [dispatch])

  


  return (
    <Box
      sx={{
        width: "100%",
        height: "100%",


      }}
    >

      {
        loading ? ('') :
        (
          <ResponsiveContainer width="100%" height="100%">
    <BarChart
      
      style={{
        width : "100%"
      }}
      width = {600}
      height={500}
      data={statistiques}
      margin={{
        top: 20,
        right: 30,
        left: 20,
        bottom: 5
      }}
    >
      <CartesianGrid strokeDasharray="4 4" />
      <XAxis dataKey="name" />

      <Tooltip />
      <Legend />
      
      <Bar dataKey="coutTotal"  background={{ fill: '#eee' }} fill="#8884d8" />
      <Bar dataKey="quantiteTotal"  background={{ fill: '#eee' }} fill="#82ca9d" />
      <Bar dataKey="remplissage"  background={{ fill: '#eee' }} fill="#ffc658" />

    </BarChart>
    </ResponsiveContainer>

        )
      }
  
    </Box>
  );
}

export default FuelChart