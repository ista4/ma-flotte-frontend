import React, { useEffect, useState } from "react";
import {
  MapContainer,
  Marker,
  Popup,
  TileLayer,
  useMapEvents,

} from "react-leaflet";
import L from "leaflet";




const StationMap = ({setLatLng}) => {

  const [position, setPosition] = useState(  {lat: 34.41200812435203, lng: -2.894505858421326});

  
  useEffect(() => {
    if (navigator.geolocation) {

      navigator.geolocation.getCurrentPosition((position) => {
        setPosition({
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        });
        
        setLatLng({
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        })

      });
    } else {
      alert("Geolocation is not supported by this browser.");
    }

  }, [setLatLng]);
  

  const ChengeLoc = () => {

    const map = useMapEvents({
      click(e) {

        setPosition(e.latlng)
        setLatLng(e.latlng)
        map.flyTo(e.latlng, map.getZoom())
      }
    })

    return position === null ? null : (
      <Marker position={position} >
        <Popup>You are here</Popup>
      </Marker>
    )
  } 

  return (
    <MapContainer
      id="myMap"
      center={{ lat: position.lat, lng: position.lng }}
      zoom={13}
      scrollWheelZoom={true}
    >
      <TileLayer
        attribution='Ma Flotte'
        url="https://tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <ChengeLoc/>
    </MapContainer>
  );
};


// change the default marker icon 
const defaultMarker = L.icon({
  iconUrl: "/marker.svg",
  iconSize: [36, 36],
  iconAnchor: [12, 25],
  popupAnchor: [0, -25],
});

Marker.defaultProps = {
  icon: defaultMarker,
};
export default StationMap;
