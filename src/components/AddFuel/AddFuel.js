import { Box } from "@mui/material";
import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import { QrCodeScanner } from "@mui/icons-material";
import KeyboardIcon from '@mui/icons-material/Keyboard';
import styles from "./style.module.css";
import { useDispatch } from "react-redux";
import { openResponsiveModal } from "../../store/reducer/ResponsiveModalSlice";
const AddFuel = () => {

    const dispatch = useDispatch()
  return (
    <Box
      sx={{
       maxWidth: "600px",
       height: "100%",
        display: "flex",
        flexDirection: "column",
        gap: "20px",
        alignItems: "center",
        justifyContent: "center",
        margin: "auto",
      }}
    >
      <Card className={styles.card} onClick={() => dispatch(openResponsiveModal({componentName:"AddFuelByQrCode"}))}>
        <CardActionArea>
          <CardContent>
            <Typography
              gutterBottom
              variant="h5"
              className={styles.card_title}
              fontWeight="500"
              component="div"
            >
              Par QrCode <QrCodeScanner fontSize="large" />
            </Typography>
            <Typography variant="body2" color="text.secondary">
            scanner le QrCode de vehicule pour ajouter le carburant
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>


      <Card className={styles.card} onClick={() => dispatch(openResponsiveModal({componentName:"AddFuelManuel", title : 'Ajouter carburant'}))}>
        <CardActionArea>
          <CardContent>
            <Typography
              gutterBottom
              variant="h5"
              className={styles.card_title}
              fontWeight="500"
              component="div"
            >
              Manuel <KeyboardIcon fontSize="large" />
            </Typography>
            <Typography variant="body2" color="text.secondary">
            entrer les données de vehicule  manuelle pour ajouter le carburant
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Box>
  );
};

export default AddFuel;
