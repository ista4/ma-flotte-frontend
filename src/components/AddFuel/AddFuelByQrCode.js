import {
  Box,
  Chip,
  CircularProgress,
  Divider,
  FormControl,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import styles from "./style.module.css";

import { useDispatch, useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import SpeedIcon from "@mui/icons-material/Speed";
import LocalGasStationIcon from "@mui/icons-material/LocalGasStation";
import PointOfSaleIcon from "@mui/icons-material/PointOfSale";
import { PhotoCamera } from "@mui/icons-material";

import { LoadingButton } from "@mui/lab";
import {Html5QrcodeScanner} from "html5-qrcode"
import { getVehiculeById } from "../../store/reducer/vehicle/VehicleSlice";
import { enqueueSnackbar } from "notistack";
import { closeResponsiveModal } from "../../store/reducer/ResponsiveModalSlice";
import { addFuel } from "../../store/reducer/Fuel/AddFuelSlice";
import { addFuelToState, getFuel } from "../../store/reducer/Fuel/FuelSlice";
import { getUser } from "../../store/reducer/user/UserSclice";
import { getFuelStatistiques } from "../../store/reducer/Fuel/FuelStatistiquesSlice";

const AddFuelByQrCode = () => {

  const [kilometrage, setKilometrage] = useState("");
  const [prix, setPrix] = useState("");
  const [quantite, setQuantite] = useState("");
  const [image, setImage] = useState();
  const [nom, setNom] = useState("");
  const [vehicule, setVehicule] = useState({});
  const [latLng, setLatLng] = useState({
    lat: null,
    lng: null,
  });

  const dispatch = useDispatch();
  const { loading } = useSelector((state) => state.Vehicle);
  const {loading: fuelLoading} = useSelector((state) => state.AddFuel);


  useEffect(() => {

    navigator.geolocation.getCurrentPosition((position) => {
      setLatLng({
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      });
    });

    function onScanSuccess(decodedText, decodedResult) {
      // handle the scanned code as you like, for example:
      const data = JSON.parse(decodedText);

      enqueueSnackbar('Scanné avec succès ', { variant: "success" })
      dispatch(getVehiculeById(data.immatriculation)).unwrap()
      .then((result) => {
          setVehicule(result.vehicule);
      }).catch((error) => {
        enqueueSnackbar('Vehicule non trouver', { variant: "error" })
        dispatch(closeResponsiveModal())
      })

      html5QrcodeScanner.clear()
    }
    
    function onScanFailure(error) {
      // handle scan failure, usually better to ignore and keep scanning.
      // for example:
      console.warn(`Code scan error = ${error}`);
    }
    
    let html5QrcodeScanner = new Html5QrcodeScanner(
      "reader",
      { fps: 12, qrbox: {width: 250, height: 250} },
      /* verbose= */ false);

    html5QrcodeScanner.render(onScanSuccess, onScanFailure);


    return () => {
      html5QrcodeScanner.clear();
      console.log( html5QrcodeScanner.getState());

      if(localStorage.user_token){
        dispatch(getUser())
      }

    }

  }, [dispatch]);




  const [msgError, setMsgError] = useState({
    nom: false,
    kilometrage: false,
    prix: false,
    quantite: false,
    vehicule: false,
    type: false,
    image: false,
    error: false,
    msg: "",
  });

  

  const handelSubmit = (e) => {
    e.preventDefault();
    const data = {nom, kilometrage, prix, quantite, vehicule : vehicule.id, image, type:vehicule.type_carburant, latLng};

    if(!data.nom){
      setMsgError((prev) =>( {...prev,nom:true}))
    }else{
      setMsgError((prev) =>( {...prev,nom:false}))
    }
    if(!data.kilometrage){
      setMsgError((prev) =>( {...prev,kilometrage:true}))
    }else{
      setMsgError((prev) =>( {...prev,kilometrage:false}))
    }
    if(!data.prix){
      setMsgError((prev) =>( {...prev,prix:true}))
    }else{
      setMsgError((prev) =>( {...prev,prix:false}))
    }
    if(!data.quantite){
      setMsgError((prev) =>( {...prev,quantite:true}))
    }else{
      setMsgError((prev) =>( {...prev,quantite:false}))
    }
    if(!data.vehicule_id){
      setMsgError((prev) =>( {...prev,vehicule:true}))
    }else{
      setMsgError((prev) =>( {...prev,vehicule:false}))
    }
    if(!data.type){
      setMsgError((prev) =>( {...prev,type:true}))
    }else{
      setMsgError((prev) =>( {...prev,type:false}))
    }
    if(!data.image){
      setMsgError((prev) =>( {...prev,image:true}))
    }else{
      setMsgError((prev) =>( {...prev,image:false}))
    }

    if(data.nom && data.kilometrage 
      && data.prix && data.quantite 
      && data.vehicule &&
      data.type && data.image)
    {

      setMsgError((prev) =>( {...prev,error:false}))
      dispatch(addFuel(data)).unwrap().then((result) => {
        enqueueSnackbar('Ajouté avec succès ', { variant: "success" })
        dispatch(closeResponsiveModal())
        dispatch(addFuelToState(result.carburant))
        dispatch(getFuelStatistiques())
        dispatch(closeResponsiveModal())
      }).catch((error) => {
        enqueueSnackbar('Erreur lors de l\'ajout ', { variant: "error" })
        console.log(error);
      })
    }
  };
  return (
    <Box className={styles.fuel_container}>
    <form onSubmit={handelSubmit} >
    
    {
      loading ? (
        
        <Box className={styles.vehicule_box} sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          padding: "12px",
        }}>
          
          <Box className={styles.vehicule_box_info} >
           <CircularProgress  color="success"/>
          </Box>
        </Box>

      ) : ( vehicule.nom ? (
        <Box className={styles.vehicule_box}>
          
          <Box className={styles.vehicule_box_info} >
          <Typography variant="h6" component="h6">
            {vehicule.nom}
          </Typography>
          <Typography variant="h6" component="h6">
            {vehicule.immatriculation}
          </Typography>

          </Box>

          <Typography variant="h6" component="h6" 
          sx={{
            display: "flex",
            alignItems: "center",
            gap: "8px",

          }}
          >
          <SpeedIcon/>  {vehicule.kilometrage}
          </Typography>
        </Box>
      ) : null
       )
    }
    <div id="reader" width="600px" style={{ display : (vehicule.nom ? "none" : 'block')}}></div>

      <FormControl fullWidth className={styles.form_control}>
        <TextField
          id="Kilometrage"
          label="Kilometrage"
          type="number"
          variant="outlined"
          error={msgError.kilometrage}
          value={kilometrage}
          onChange={(e) => {
            setKilometrage(e.target.value);
          }}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SpeedIcon />
              </InputAdornment>
            ),
          }}
        />
      </FormControl>

      <Box
        className={styles.fuel_data}
        sx={{
          width: "100%",
          display: "flex",
          gap: "16px",
        }}
      >
        <FormControl fullWidth className={styles.form_control}>
          <TextField
            id="quantite"
            value={quantite ?? ""}
            type="number"
            error={msgError.quantite}
            onChange={(e) => {
              setQuantite(e.target.value);
            }}
            label="Quantite - (L)"
            variant="outlined"

            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <LocalGasStationIcon />
                </InputAdornment>
              ),
            }}
          />

        </FormControl>

      </Box>

      <Box
        className={styles.fuel_data}
        sx={{
          width: "100%",
          display: "flex",
          gap: "16px",
        }}
      >
        <FormControl fullWidth className={styles.form_control}>
          <TextField
            id="Prix"
            type="number"
            label="Prix / L"
            variant="outlined"
            error={msgError.prix}
            value={prix ?? ""}
            onChange={(e) => {
              setPrix(e.target.value);
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <PointOfSaleIcon />
                </InputAdornment>
              ),
            }}
          />
        </FormControl>

        <FormControl fullWidth className={styles.form_control}>
          <TextField
            id="cout"
            label="Coût"
            value={
              prix && quantite ? Number(prix * quantite).toFixed(3) : "0"
            }
            variant="outlined"
            readOnly={true}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <PointOfSaleIcon />
                </InputAdornment>
              ),
            }}
          />
        </FormControl>
      </Box>

      <Box sx={{ width: "100%", mb: "16px" }}>
        <Divider>
          <Chip label="Station Service" />
        </Divider>
      </Box>

      <FormControl fullWidth className={styles.form_control}>
        <TextField
          id="nom"
          label="Nom de station"
          variant="outlined"
          error={msgError.nom}
          value={nom}
          onChange={(e) => setNom(e.target.value)}
        />
      </FormControl>

      <Box
        sx={{
          width: "100%",
          display: "flex",
          gap: "45px",
          alignItems: "center",
        }}
      >
        <FormControl className={styles.form_control}>
          <Box className={styles.image_box_station}>
            <IconButton
              color="success"
              aria-label="upload recu image"
              component="label"
            >
              <input
                hidden
                accept="image/*"
                title="Recu image"
                type="file"
                onChange={(e) => setImage(e.target.files[0])}
              />

              {image ? (
                <img
                  src={image ? URL.createObjectURL(image) : ""}
                  alt={"Recu de carburant"}
                />
              ) : (
                ""
              )}

              <PhotoCamera />
            </IconButton>
          </Box>
        </FormControl>

        <Typography variant="p" color="text.secondary" style={{ color :(  !image ? '#b73030' : "") }} >
          Entrez l'image du reçu de carburant ! 
        </Typography>
      </Box>

      
      <LoadingButton
        fullWidth
        type="submit"
        loading={fuelLoading}
        variant="contained"
        color="success"
      >
        Ajouter
      </LoadingButton>
    </form>
  </Box>
  )
}

export default AddFuelByQrCode