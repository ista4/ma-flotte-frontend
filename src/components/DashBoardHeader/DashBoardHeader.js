import { Box, Divider } from '@mui/material'
import React from 'react'
import styles from './styles.module.css'

const DashBoardHeader = ({children}) => {

  return (
    <>
    <Box className={styles.header} component='header'>
        {children}
        
    </Box>
    <Divider/>
    </>
  )
}

export default DashBoardHeader