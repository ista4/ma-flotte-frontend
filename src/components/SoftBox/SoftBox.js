import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import styles from "./styles.module.css";
import { CardActionArea } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import lang from '../../helpers/appLang';



export default function SoftBox({title, body , icon , link}) {
    const navigate = useNavigate()


    const handelClick = (link) => {
        if (link) {
          navigate(lang+link)
        }
    }


  return (
    <Card className={styles.box} onClick={() => handelClick(link)}>
    <CardActionArea sx={{cursor:'auto'}}>
        <CardContent sx={{paddingBottom:'0'}}>
            <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
            {/* Text */}
            </Typography>
            <Typography variant="h5" component="div" className={styles.title}>
                {title}
            </Typography>
            <Typography className={styles.icon} >
                  <span> {icon} </span>   <span title={body} className={styles.innerText}>{body}</span>
            </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}