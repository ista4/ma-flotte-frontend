import { LoadingButton } from '@mui/lab'
import { Autocomplete, Box, FormControl, FormLabel, TextField } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { updateUser } from '../../store/reducer/sp-admin/users/UpdateUserSlice'
import { getUser } from '../../store/reducer/user/UserSclice'
import { enqueueSnackbar } from 'notistack'
import { updateAdmin } from '../../store/reducer/sp-admin/admins/UpdateAdminSlice'
import { getAdmin } from '../../store/reducer/admin/AdminSlice'
import { closeResponsiveModal } from '../../store/reducer/ResponsiveModalSlice'

const UpdateProfile = ({role}) => {

    const [field, setField] = useState('')
    const [value, setValue] = useState('')
    const [user, setUser] =useState({})
    const [loading, setLoading] = useState(false)

    const {user : userState} = useSelector(state => state.user)
    const {admin: adminState} = useSelector(state => state.admin)
    const {spAdmin : spAdminState} = useSelector(state => state.SpAdmin)


    const dispatch = useDispatch()


    useEffect(() => {
        if(role === 'user'){
            setUser(userState)
        }else if(role === 'admin'){
            setUser(adminState)
        }else if(role === 'SpAdmin'){
            setUser(spAdminState)
        }
    }, [userState, adminState, spAdminState, role])


    const handelUpdate = (e) => {
        e.preventDefault()
        if(value){
            setLoading(true)
            if(role === 'user'){
                dispatch(updateUser({id : user.id, field, value})).unwrap()
                .then(res => {
                    dispatch(getUser())
                    setValue('')
                    setField('')
                    setLoading(false)
                    enqueueSnackbar('profile est modifié avec succés', {variant : 'success'})
                    dispatch(closeResponsiveModal())
                }).catch(err => {
                    console.log(err);
                    enqueueSnackbar('une erreur est survenu', {variant : 'error'})
                    setLoading(false)
                })
            }else if(role === 'admin'){
                dispatch(updateAdmin({id : user.id, field, value})).unwrap()
                .then(res => {
                    dispatch(getAdmin())
                    setValue('')
                    setField('')
                    setLoading(false)
                    enqueueSnackbar('profile est modifié avec succés', {variant : 'success'})
                    dispatch(closeResponsiveModal())
                }).catch(err => {
                    console.log(err);
                    enqueueSnackbar('une erreur est survenu', {variant : 'error'})
                    setLoading(false)
                })
            }

        }
    }

  return (
    <Box>
        <Autocomplete
                    sx={{ width: "100%" , minWidth:'280px', mb:'16px'}}
                    options={['nom', 'prenom' , 'role', 'tel' , 'email', 'sexe' ]}
                    id="fields"
                    fullWidth
                    value={field }
                    onChange={(e, val) => {
                        setField(val); 
                    }}
                    getOptionLabel={(item) =>  item}
               
                    renderInput={(params) => <TextField {...params} label="selectioner le champe" />}
                />

        <form
        onSubmit={handelUpdate}
        >
        {
            field ? 
            (
                
                <FormControl
                fullWidth
                sx={{ width: "100%", mb:'16px' }}
                 >
                    <FormLabel
                    sx={{ mb:'5px',ml:'2px'}}
                    >entrer la nouvelle valeur</FormLabel>

                <TextField 
                onChange={(e) => setValue(e.target.value)}
                value={value}
                id="outlined-basic"
                placeholder={user[field]}
                variant="outlined"
                />
                </FormControl>
            ):
            null
        }
        <LoadingButton 
        fullWidth
        variant="contained"
        color='success'
        loading={loading}
        type='submit'
        >
            Modifier
        </LoadingButton>
        </form>

        

    </Box>
  )
}

export default UpdateProfile